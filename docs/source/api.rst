API
===

.. autosummary::
   :toctree: _autosummary
   :template: custom-module-template.rst
   :recursive:

   capitu.dap
   capitu.datastore
   capitu.scripts
