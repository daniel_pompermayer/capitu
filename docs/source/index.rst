Welcome to ``capitu``'s documentation!
======================================

..   For fully understanding the modeling process and the available features, check out the :doc:`roadmap` section.

.. include:: ../../README.rst
   :start-after: .. start-introduction
   :end-before: .. end-introduction

.. note:: This project is under [a not so much] active development.


Contents
--------

.. toctree::
   :hidden:

   self

.. toctree::
   :maxdepth: 1

   getting_started
   why_capitu
   publications
   api
..      roadmap

.. include:: ../../README.rst
   :start-after: .. end-introduction
   :end-before: .. end-initial includes

.. include:: ../../README.rst
   :start-after: .. end-why_capitu
