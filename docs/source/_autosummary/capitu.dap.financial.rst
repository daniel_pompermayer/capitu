capitu.dap.financial
====================

.. automodule:: capitu.dap.financial

   
   
   

   
   
   

   
   
   

   
   
   



.. rubric:: Modules

.. autosummary::
   :toctree:
   :template: custom-module-template.rst
   :recursive:

   capitu.dap.financial.energy_cost
   capitu.dap.financial.get_fees
   capitu.dap.financial.reactive_cost

