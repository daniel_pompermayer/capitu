capitu.dap.premises.clusters
============================

.. automodule:: capitu.dap.premises.clusters

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
      :toctree:
   
      get_season_clusters
      mark_season_clusters
   
   

   
   
   

   
   
   



