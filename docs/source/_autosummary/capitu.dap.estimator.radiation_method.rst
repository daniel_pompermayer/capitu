capitu.dap.estimator.radiation\_method
======================================

.. automodule:: capitu.dap.estimator.radiation_method

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
      :toctree:
   
      compute_surface_temperature
      estimate_generation
   
   

   
   
   

   
   
   



