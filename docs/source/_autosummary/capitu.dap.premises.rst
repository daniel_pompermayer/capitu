capitu.dap.premises
===================

.. automodule:: capitu.dap.premises

   
   
   

   
   
   

   
   
   

   
   
   



.. rubric:: Modules

.. autosummary::
   :toctree:
   :template: custom-module-template.rst
   :recursive:

   capitu.dap.premises.clusters
   capitu.dap.premises.generation_time
   capitu.dap.premises.holidays
   capitu.dap.premises.market
   capitu.dap.premises.panels

