capitu.dap.stats
================

.. automodule:: capitu.dap.stats

   
   
   

   
   
   

   
   
   

   
   
   



.. rubric:: Modules

.. autosummary::
   :toctree:
   :template: custom-module-template.rst
   :recursive:

   capitu.dap.stats.aic
   capitu.dap.stats.best_fit
   capitu.dap.stats.cutoff_table
   capitu.dap.stats.dist_fit
   capitu.dap.stats.find_rvs
   capitu.dap.stats.group_ecdf
   capitu.dap.stats.group_fit
   capitu.dap.stats.kstest
   capitu.dap.stats.modeling
   capitu.dap.stats.simulation
   capitu.dap.stats.single_fit

