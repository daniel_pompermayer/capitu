capitu.dap.stats.modeling
=========================

.. automodule:: capitu.dap.stats.modeling

   
   
   

   
   
   

   
   
   

   
   
   



.. rubric:: Modules

.. autosummary::
   :toctree:
   :template: custom-module-template.rst
   :recursive:

   capitu.dap.stats.modeling.consumption
   capitu.dap.stats.modeling.data_processing
   capitu.dap.stats.modeling.generation

