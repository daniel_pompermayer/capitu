capitu.dap.stats.simulation
===========================

.. automodule:: capitu.dap.stats.simulation

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
      :toctree:
   
      simulation
   
   

   
   
   

   
   
   



.. rubric:: Modules

.. autosummary::
   :toctree:
   :template: custom-module-template.rst
   :recursive:

   capitu.dap.stats.simulation.monte_carlo
   capitu.dap.stats.simulation.sample_size

