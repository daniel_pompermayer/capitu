capitu.dap.power\_factor.get\_pf\_violations
============================================

.. automodule:: capitu.dap.power_factor.get_pf_violations

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
      :toctree:
   
      get_pf_violations
   
   

   
   
   

   
   
   



