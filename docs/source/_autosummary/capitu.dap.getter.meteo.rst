capitu.dap.getter.meteo
=======================

.. automodule:: capitu.dap.getter.meteo

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
      :toctree:
   
      get
      get_inmet_data
      get_stored_data
   
   

   
   
   

   
   
   



