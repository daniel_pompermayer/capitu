﻿capitu.dap
==========

.. automodule:: capitu.dap

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
      :toctree:
   
      set_language
   
   

   
   
   

   
   
   



.. rubric:: Modules

.. autosummary::
   :toctree:
   :template: custom-module-template.rst
   :recursive:

   capitu.dap.balance
   capitu.dap.estimator
   capitu.dap.filter
   capitu.dap.financial
   capitu.dap.getter
   capitu.dap.optimization
   capitu.dap.parser
   capitu.dap.plotter
   capitu.dap.power_factor
   capitu.dap.premises
   capitu.dap.stats

