capitu.dap.stats.modeling.generation
====================================

.. automodule:: capitu.dap.stats.modeling.generation

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
      :toctree:
   
      generation
      prepare_generation_for_modeling
   
   

   
   
   

   
   
   



