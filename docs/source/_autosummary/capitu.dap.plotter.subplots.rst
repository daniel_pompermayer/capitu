capitu.dap.plotter.subplots
===========================

.. automodule:: capitu.dap.plotter.subplots

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
      :toctree:
   
      close_cell_subplot
      prepare_cell_subplot
      prepare_column_subplot
   
   

   
   
   

   
   
   



