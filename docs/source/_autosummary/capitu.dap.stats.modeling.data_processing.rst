capitu.dap.stats.modeling.data\_processing
==========================================

.. automodule:: capitu.dap.stats.modeling.data_processing

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
      :toctree:
   
      filtering
      pre_processing
   
   

   
   
   

   
   
   



