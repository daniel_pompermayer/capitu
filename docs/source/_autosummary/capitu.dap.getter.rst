capitu.dap.getter
=================

.. automodule:: capitu.dap.getter

   
   
   

   
   
   

   
   
   

   
   
   



.. rubric:: Modules

.. autosummary::
   :toctree:
   :template: custom-module-template.rst
   :recursive:

   capitu.dap.getter.balance
   capitu.dap.getter.consumption
   capitu.dap.getter.generation
   capitu.dap.getter.inverters
   capitu.dap.getter.meteo

