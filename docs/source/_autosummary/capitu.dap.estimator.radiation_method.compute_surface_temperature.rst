capitu.dap.estimator.radiation\_method.compute\_surface\_temperature
====================================================================

.. currentmodule:: capitu.dap.estimator.radiation_method

.. autofunction:: compute_surface_temperature