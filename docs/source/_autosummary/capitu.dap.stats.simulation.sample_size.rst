capitu.dap.stats.simulation.sample\_size
========================================

.. automodule:: capitu.dap.stats.simulation.sample_size

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
      :toctree:
   
      sample_size_determination
   
   

   
   
   

   
   
   



