capitu.dap.plotter
==================

.. automodule:: capitu.dap.plotter

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
      :toctree:
   
      close_plot
      prepare_plot
   
   

   
   
   

   
   
   



.. rubric:: Modules

.. autosummary::
   :toctree:
   :template: custom-module-template.rst
   :recursive:

   capitu.dap.plotter.boxplot
   capitu.dap.plotter.colors
   capitu.dap.plotter.ecdfplot
   capitu.dap.plotter.labels
   capitu.dap.plotter.lineplot
   capitu.dap.plotter.pdfplot
   capitu.dap.plotter.percentile_plot
   capitu.dap.plotter.subplots
   capitu.dap.plotter.variation_plot

