capitu.dap.stats.modeling.consumption
=====================================

.. automodule:: capitu.dap.stats.modeling.consumption

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
      :toctree:
   
      consumption
      prepare_consumption_for_modeling
   
   

   
   
   

   
   
   



