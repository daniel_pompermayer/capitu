capitu.dap.power\_factor
========================

.. automodule:: capitu.dap.power_factor

   
   
   

   
   
   

   
   
   

   
   
   



.. rubric:: Modules

.. autosummary::
   :toctree:
   :template: custom-module-template.rst
   :recursive:

   capitu.dap.power_factor.compute
   capitu.dap.power_factor.get_pf_violations
   capitu.dap.power_factor.less_than_ideal

