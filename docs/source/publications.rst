===============================
Publications Using ``capitu``
===============================

Introduction
------------

Take a look at these publications where ``capitu`` has been used to handle the data:

1. Pompermayer, Daniel C., et al. "Extra Reactive Power Analysis on a Distribution Grid with High Integration of PV Generation." In *2019 IEEE 15th Brazilian Power Electronics Conference and 5th IEEE Southern Power Electronics Conference (COBEP/SPEC)*, 2019: 1-6. DOI: `10.1109/COBEP/SPEC44138.2019.9065632`_.

2. Pompermayer, Daniel C., et al. "Reactive Power Control on a Consumer Unit with High Integration of PV Generation." In *Anais do Simpósio Brasileiro de Sistemas Elétricos*, 2020, Vol. 1, No. 1. DOI: `10.48011/sbse.v1i1.2263`_.

3. Pompermayer, Daniel Campos and Fardin, Jussara Farias. "Probabilistic Analysis of Low Power Factor in a Facility with High Integration of PV Generation." In *2023 IEEE 8th Southern Power Electronics Conference and 17th Brazilian Power Electronics Conference (SPEC/COBEP)*, 2023: 1-7. DOI: `10.1109/SPEC56436.2023.10408330`_.


Running the Calculations
------------------------

The script used to perform the calculations for the SPEC 2019 publication can be found at :py:mod:`capitu.scripts.spec2019`.


How to Cite
-----------

To cite these publications, you can use the following BibTeX format:

.. code-block:: bibtex

   @INPROCEEDINGS{pompermayer2019,
     author={Pompermayer, Daniel C. and Marin, Caroline and Mendes, Mariana A. and Queiroz, Luann G. O. and Tonini, Luiz G. R. and Vargas, Murillo C. and Batista, Oureste E.},
     booktitle={2019 IEEE 15th Brazilian Power Electronics Conference and 5th IEEE Southern Power Electronics Conference (COBEP/SPEC)}, 
     title={Extra Reactive Power Analysis on a Distribution Grid with High Integration of PV Generation}, 
     year={2019},
     volume={},
     number={},
     pages={1-6},
     keywords={Reactive power;Estimation;Solar radiation;Inverters;Productivity;Photovoltaic systems;Generators;Electric power systems;energy management;energy quality;photovoltaic systems;power factor},
     doi={10.1109/COBEP/SPEC44138.2019.9065632}}

   @INPROCEEDINGS{pompermayer2020,
     author={Pompermayer, Daniel C. and Mendes, Mariana A. and Dimanski, Matheus and Rueda-Medina, Augusto C.},
     booktitle={Anais do Simpósio Brasileiro de Sistemas Elétricos}, 
     title={Reactive Power Control on a Consumer Unit with High Integration of PV Generation}, 
     year={2020},
     volume={1},
     number={1},
     keywords={Electric power systems, Energy management, Energy quality, Photovoltaic systems, Power factor},
     doi={10.48011/sbse.v1i1.2263}}

   @INPROCEEDINGS{pompermayer2023,
     author={Pompermayer, Daniel Campos and Fardin, Jussara Farias},
     booktitle={2023 IEEE 8th Southern Power Electronics Conference and 17th Brazilian Power Electronics Conference (SPEC/COBEP)}, 
     title={Probabilistic Analysis of Low Power Factor in a Facility with High Integration of PV Generation}, 
     year={2023},
     volume={},
     number={},
     pages={1-7},
     keywords={Reactive power;Probabilistic logic;Inverters;Hazards;Logistics;Distribution functions;Capacity planning;Electric power systems;energy management;energy quality;photovoltaic systems;power factor},
     doi={10.1109/SPEC56436.2023.10408330}}

.. _10.1109/COBEP/SPEC44138.2019.9065632: https://doi.org/10.1109/COBEP/SPEC44138.2019.9065632
.. _10.48011/sbse.v1i1.2263: https://doi.org/10.48011/sbse.v1i1.2263
.. _10.1109/SPEC56436.2023.10408330: https://doi.org/10.1109/SPEC56436.2023.10408330

