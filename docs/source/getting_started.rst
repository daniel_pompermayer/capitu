Getting Started
===============

.. include:: ../../README.rst
   :start-after: .. start-getting_started
   :end-before: .. end-getting_started
