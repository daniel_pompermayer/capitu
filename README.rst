``capitu``
==========

.. start-introduction
Introduction
------------

``capitu`` is a Python module programmed to capture, present, and process electricity generation and consumption data from the Federal University of Espírito Santo.

.. end-introduction

Source and Documentation
------------------------

You can access ``capitu``'s documentation by clicking `here <https://daniel_pompermayer.gitlab.io/capitu/>`__ and the source code by clicking `here <https://gitlab.com/daniel_pompermayer/capitu>`__.

Built with
----------

 * Emacs
 * Python
 * Sphinx

Development Status
------------------

``capitu`` still under construction. Until now it's able to:

* Access the PV inverters cloud plataform and acquire data.
* Retrieve meteorological data from a file and utilize a list of inverters to estimate the generated energy.
* Obtain consumption data from a file, combine it with the estimated generation, and calculate a balance.
* Estimate the impact of generation on the facility's power factor.
* Generate boxplots for consumption, solar radiation, generation, and power factor.

Needed Improvements
-------------------

This code is being developed as a proof of concept. It may require some standardization or optimization. You are welcome to contribute.
.. end-initial includes

Getting Started
---------------
.. start-getting_started
   
Well, you've probably noticed there are two main folders in the root of this repo:

::

    capitu
    ├── docs
    └── capitu

``docs`` is where we generate the `documentation <https://daniel_pompermayer.gitlab.io/capitu>`_ using a doc generator called `Sphinx <https://www.sphinx-doc.org/>`_. ``capitu`` is where all the actual source code lives.

Now, if you click into ``capitu``, you'll see a few different folders:
::

    capitu
    ├── dap
    ├── daq
    ├── datatore
    └── scripts

``dap`` is where we do all the processing. ``daq`` is where we handle all the data acquisition stuff. Right now, it includes a submodule called ``inverters`` which grabs data from the photovoltaic generators at Ufes by accessing their cloud platform. ``datastore`` is just a nice place to keep important data inside the app. And ``scripts`` is where all the calculations happen.
.. end-getting_started

Why Capitu?
-----------
.. start-why_capitu

`Capitu <https://en.wikipedia.org/wiki/Capitu>`_ is the nickname of Maria Capitolina Santiago, the main character of Machado de Assis' 1899 book "Dom Casmurro." `Joaquim Maria Machado de Assis <https://en.wikipedia.org/wiki/Machado_de_Assis>`_ (June 21, 1839 - September 29, 1908), often known as Machado de Assis, was a pioneering Brazilian black novelist, poet, playwright, and short story writer. He is widely regarded as the greatest writer of Brazilian literature.

In Portuguese, the word "Capitu" sounds almost like "capture," which aligns with the exact purpose of this module: capturing and processing metering data.

Capitu is one of the greatest female characters in Brazilian literature. This powerful and enigmatic woman has been the subject of numerous academic studies, books, television programs, and more. The character undoubtedly deserves a much better homage than what I can offer here.

Her intense way of living and her faithful friendship with Escobar caused her husband Betinho, the Dom Casmurro, to be affected by the distrust of having been betrayed. Machado de Assis' work is inconclusive about the supposed affair of Capitu, and for this reason, perhaps this has become the most important question in Brazilian literature: Has Capitu betrayed Bentinho?

Metering data capture, presentation, and processing software are used worldwide, among other purposes, to combat fraud in the distribution of electricity. Operators scrutinize the acquired data searching for the mystery solution: are these data proof of energy theft? Capitu evokes the mystery of betrayal as the telemetric data do as well.

    "Dá-me uma comparação exata e poética para dizer o que foram aqueles olhos de Capitu. Não me acode imagem capaz de dizer, sem quebra da dignidade do estilo, o que eles foram e me fizeram. Olhos de ressaca? Vá, de ressaca" (Machado de Assis).
.. end-why_capitu

Authors
-------

 * Daniel Campos Pompermayer

License
-------

Copyright (C) 2018  Daniel Campos Pompermayer

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
