#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Report published at the 2019 IEEE 15th Brazilian Power Electronics
Conference and 5th IEEE Southern Power Electronics Conference (COBEP/SPEC).
"""

import argparse
from pathlib import Path
from ...dap.balance.get_one_pf import get_one_pf as get_one_pf_balance
from ...dap.power_factor.get_pf_violations import get_pf_violations
from ...dap.plotter.percentile_plot import percentile_plot


def main(start_date, end_date, meteo_file, api, station, consumption_file,
         consumption_agg, extra_capacitive_energy, inverters_file, data_tz,
         target_tz, fee_date, image_export_path, table_export_path, show,
         img_format):
    """
    Main routine of the paper published at the 2019 IEEE 15th Brazilian Power
    Electronics Conference and 5th IEEE Southern Power Electronics Conference
    (COBEP/SPEC).

    .. warning::
        Get data using api might not work because of the INMET API policy.

    :param start_date: date of the begin of the interval of data.
    :type start_date: str, format: %Y-%m-%d
    :param end_date: date of the end of the interval of data.
    :type end_date: str, format: %Y-%m-%d
    :param meteo_file: String having the path and the name of the file having
        the meteorological data. If filepath is not None, api will not be used.
    :type meteo_file: str
    :param api: URL of the INMET's API.
    :type api: str, default None
    :param station: name of the INMET automatic meteorological station from
        which the data will be got.
    :type station: str, default None
    :param consumption_file: Path to the consumption file.
    :type consumption_file: str
    :param consumption_agg: If None, no aggregation is performed. If "hour",
        the consumption file is aggregated by hour. If "month", the consumption
        file is aggregated by month.
    :type consumption_agg: str, default: None
    :param extra_capacitive_energy: Additional capacitive reactive energy.
    :type extra_capacitive_energy: float, default: 0
    :param inverters_file: Path to the inverters file.
    :type inverters_file: str
    :param data_tz: name of the pytz timezone where the meteorological station
        is located.
    :type data_tz: str, default "Etc/UTC".
    :param target_tz: Name of the target pytz timezone.
    :type target_tz: str, default "America/Sao_Paulo".
    :param fee_date: Date for which the market fees are to be retrieved. If
        None, the index of the consumption DataFrame is used.
    :type fee_date: str or None, default: None
    :param image_export_path: Address to the directory where the plots will be
        saved. If None, the plot will not be saved.
    :type image_export_path: str or None, default: None
    :param show: If True, a boxplot of the meteorological data and of the
        consumption data  will be presented on the screen.
    :type show: bool, default: False
    :param img_format: Specifies the file format for saving the plot.
    :type img_format: str
    """

    # Compute the economy, power factor and bills when the inverters are setted
    # to unitary power factor.
    balance, one_pf_balance = get_one_pf_balance(
        start_date, end_date, consumption_file, inverters_file,
        meteo_file, extra_kvar_c=extra_capacitive_energy,
        extra_kvar_i=0, api=api, station=station, campus="G",
        inverters_date=None, void_sn="ignore", compute_temperature=False,
        uplimit="nominal", coherent=False, consumption_agg=consumption_agg,
        fee_date=fee_date, data_tz=data_tz, target_tz=target_tz,
        localize_tz=True, show=show, image_export_path=image_export_path,
        format=img_format)

    # Plot the power factor and save a table comparing how many times the power
    # factor was smaller than the regulatory value with no generation and with
    # unitary power factor generation.
    _ = get_pf_violations(balance, one_pf_balance,
                          table_export_path=table_export_path,
                          image_export_path=image_export_path, show=show,
                          format=img_format)

    # I wanna know how much the extra reactives intervals will affect my
    # economical intervals.
    economical = one_pf_balance.raw_economy > 0
    weight = 100 * (
        one_pf_balance.extra_reactive_bill / one_pf_balance.raw_economy)
    loud = weight > 0

    # Plot a cumulative histogram.
    # I've chosen after to change the colors to a seaborn colorblind palette.
    percentile_plot(weight[economical & loud], cutoff=92,
                    y="percentual_extra_reactive_charges",
                    path=image_export_path, show=show, format=img_format)


if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    # Receive arguments related to the meteorological data.
    _ = parser.add_argument(
        "--meteo_file", type=str,
        default=Path(__file__).parents[2] / "datastore/meteo.csv",
        help="Path to the file having the meteorological data")

    _ = parser.add_argument("--api", type=str, default=None,
                            help="Address to INMET api")

    _ = parser.add_argument("--station", type=str, default=None,
                            help="Name of INMET station")

    # Receive arguments related to the consumption data.
    _ = parser.add_argument(
        "--consumption_file", type=str,
        default=Path(__file__).parents[2] / "datastore/consumption.csv",
        help="Path to the file having the consumption data")

    _ = parser.add_argument(
        "--consumption_agg", type=str, default="hour",
        help="Type of aggregation of he consumption data")

    _ = parser.add_argument(
        "--extra_capacitive_energy", type=float, default=300,
        help=("Extra capacitive reactive energy that must be added when "
              + " computing the energy balance."))

    # Receive arguments related to the inverters data.
    _ = parser.add_argument(
        "--inverters_file", type=str,
        default=Path(__file__).parents[2] / ("datastore/"
                                             + "inverters_initial_plan.csv"),
        help="Path to the file having the inverters data")

    # Receive as argument the path to the directory where the results must be
    # saved.
    _ = parser.add_argument(
        "--export_path", type=str, default=str(Path(__file__).parent),
        help="Path to the directory where the results must be saved")

    # Receive as argument whether I want the plots to be shown.
    _ = parser.add_argument(
        "--show", action="store_true",
        help="Bool saying whether you want the plots to be shown.")

    # Receive as argument the format of the image files.
    _ = parser.add_argument(
        "--img_format", type=str,
        default="pgf",
        help="Specifies the file format for saving the plot.")

    # Get the arguments.
    args = parser.parse_args()
    meteo_file = args.meteo_file
    api = args.api
    station = args.station
    consumption_file = args.consumption_file
    consumption_agg = args.consumption_agg
    extra_capacitive_energy = args.extra_capacitive_energy
    inverters_file = args.inverters_file
    export_path = args.export_path
    show = args.show
    img_format = args.img_format

    # Time interval of the analisys.
    start_date = "2015-02-22"
    end_date = "2017-03-24"
    fee_date = "2019-07-06"

    # Data timezone and target timezone. I'm using São Paulo timezone which
    # is the same as Vitória, where the plant is located.
    data_tz = "Etc/UTC"
    target_tz = "America/Sao_Paulo"

    # The path to the exports.
    image_export_path = export_path + "/figures"
    table_export_path = export_path + "/tables"

    # Check whether we have a meteo file and Path it.
    if meteo_file is not None:
        meteo_file = Path(meteo_file)

    main(start_date, end_date, meteo_file, api, station, consumption_file,
         consumption_agg, extra_capacitive_energy, inverters_file, data_tz,
         target_tz, fee_date, image_export_path, table_export_path, show,
         img_format)
