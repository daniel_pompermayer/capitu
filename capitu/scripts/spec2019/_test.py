#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Test of the spec2019 script.
"""

import filecmp
import subprocess
from PIL import Image, ImageChops
from pathlib import Path


def test_script(tmp_path):

    # Create some directories to store the generated files.
    images_dir = tmp_path / "figures"
    images_dir.mkdir(parents=True, exist_ok=True)

    tables_dir = tmp_path / "tables"
    tables_dir.mkdir(parents=True, exist_ok=True)

    # Run the spec2019 script.
    subprocess.run(["python3", "-m", "capitu.scripts.spec2019",
                    "--meteo_file", "./capitu/capitu/datastore/meteo.csv",
                    "--consumption_file",
                    "./capitu/capitu/datastore/consumption.csv",
                    "--consumption_agg", "hour",
                    "--extra_capacitive_energy", "300",
                    "--inverters_file",
                    "./capitu/capitu/datastore/inverters_initial_plan.csv",
                    "--export_path", str(tmp_path),
                    "--img_format", "png"])

    # Verify if the files have been properly created.
    ref_figures_dir = Path("./capitu/capitu/scripts/spec2019/figures")
    for figure in ref_figures_dir.glob("*.png"):
        template = Image.open(figure)
        target = Image.open(images_dir / figure.name)
        diff = ImageChops.difference(target, template)
        assert diff.getbbox() is None

    ref_tables_dir = Path("./capitu/capitu/scripts/spec2019/tables")
    for table in ref_tables_dir.glob("*.tex"):
        tbl = tables_dir / table.name
        assert filecmp.cmp(tbl, table)
