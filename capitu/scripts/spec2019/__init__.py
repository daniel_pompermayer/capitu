#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Report published at the 2019 IEEE 15th Brazilian Power Electronics
Conference and 5th IEEE Southern Power Electronics Conference (COBEP/SPEC).

:command:
    python3 -m capitu.src.scripts.spec2019
    --meteo_file "./capitu/capitu/datastore/meteo.csv"
    --consumption_file "./capitu/capitu/datastore/consumption.csv"
    --consumption_agg "hour"
    --extra_capacitive_energy 300
    --inverters_file "./capitu/capitu/datastore/inverters_initial_plan.csv"
    --export_path "./"
    --img_format "png"
"""
name = "spec2019"
__author__ = "Daniel Campos Pompermayer [gitlab.com/daniel_pompermayer]"
