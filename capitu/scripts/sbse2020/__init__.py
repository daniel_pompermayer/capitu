#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Report published at the Simpósio Brasileiro de Sistemas Elétricos of 2020
(SBSE2020).
"""
name = "sbse2020"
__author__ = "Daniel Campos Pompermayer [gitlab.com/daniel_pompermayer]"
