import argparse
import logging
import numpy as np
import pandas as pd
from pathlib import Path
from ....src import dap
from ...dap.getter.inverters import get as get_inverters
from ...dap.financial.energy_cost import bill as energy_bill
from ...dap.financial.reactive_cost import bill as reactive_bill
from ...dap.balance.get_one_pf import get_one_pf as get_one_pf_balance
from ...dap.power_factor.get_pf_violations import get_pf_violations
from ...dap.optimization.min_q_cost import min_q_cost
# from ...dap.optimization.opt_q_cost import opt_q_cost
from ...dap.plotter.labels import table_labels
# from ...dap.power_factor.less_than_ideal import less_than_ideal
from ...dap.premises.market import brazilian_ideal_pf
# from ...dap.plotter.percentile_plot import percentile_plot


def main(start_date, end_date, meteo_file, api, station, consumption_file,
         consumption_agg, int_time, extra_kvar_c, extra_kvar_i, inverters_file,
         data_tz, target_tz, fee_date, pre_process, image_path, table_path,
         julia_runtime, show, compute_temperature, fig_width, fig_height,
         pf_width, pf_height, fontsize):

    logging.info("Starting process.")

    # Compute the economy, power factor and bills when the inverters are setted
    # to unitary power factor.
    balance, one_pf_balance = get_one_pf_balance(
        start_date, end_date, consumption_file, inverters_file, meteo_file,
        extra_kvar_c, extra_kvar_i, api, station, campus="G",
        inverters_date="2024-06-22", void_sn="drop",
        compute_temperature=compute_temperature, uplimit="nominal",
        coherent=False, consumption_agg=consumption_agg, fee_date=fee_date,
        data_tz=data_tz, target_tz=target_tz, localize_tz=True,
        generation_time_only=True, hour_offset=True,
        image_export_path=image_path, show=show, ax_height=fig_height,
        ax_width=fig_width, fontsize=fontsize)

    # Get the discontinuous region.
    discontinuous = np.logical_or(one_pf_balance.p_ext_grid_kw >= 0,
                                  one_pf_balance.pf >= brazilian_ideal_pf)

    # Get the inverters max capacity.
    inverters = get_inverters(inverters_file, campus="G",
                              inverters_date="2024-06-22", void_sn="drop")
    max_capacity = float(inverters.nominal_power_kw.sum())

    # Compute the generated powers and the demanded powers trying to minimize
    # the extra rective power cost.
    min_q_cost_set = min_q_cost(
        balance[~discontinuous], julia_runtime, max_capacity, fee_date,
        fee_tz="America/Sao_Paulo", min_inv_pf=0.8, path=table_path,
        filename="min_q_cost", show=show)
    min_q_cost_gen_balance = one_pf_balance.copy()
    min_q_cost_gen_balance.loc[
        ~discontinuous, min_q_cost_set.columns] = min_q_cost_set

    # Get a table comparing how many times the power factor was smaller than
    # the regulatory value with no generation and with unitary power factor
    # generation.
    _ = get_pf_violations(
        balance, one_pf_balance, min_q_cost_gen_balance,
        table_export_path=table_path, show=show,
        image_export_path=image_path, ax_width=pf_width, ax_height=pf_height,
        fontsize=fontsize, old_pf_label="no_gen", new_pf_label="one_pf",
        min_q_cost_pf_label="opt_gen")

    # Get the best of all the worlds.
    # opt_gen_balance = opt_q_cost(
    #     min_q_cost_gen_balance, one_pf_balance, path=table_path,
    #     filename="opt_q_cost", show=show, image_path=image_path,
    #     ax_width=pf_width, ax_height=pf_height, fontsize=fontsize)

    # Get how many times the power factor was below the regulatory value with
    # no generation and with full generation.
    # pf_violations.loc[:, "opt_gen"] = less_than_ideal(
    #     min_q_cost_gen_balance, brazilian_ideal_pf)
    # pf_violations.loc[:, "min_q_cost_gen"] = less_than_ideal(
    #     min_q_cost_gen_balance, brazilian_ideal_pf)
    # pf_violations.loc[:, "opt_gen"] = less_than_ideal(
    #     opt_gen_balance, brazilian_ideal_pf)

    # Save a table comparing new and old power factors.
    # if table_path is not None:
    #     formated_output = pf_violations.map(
    #         lambda x: "{:.2f}\\%".format(float(x.strip("%"))))
    #     formated_output = formated_output.reset_index().rename(
    #         {"index": "hour"}, axis=1).rename(columns=table_labels)
    #     formated_output.to_latex(
    #         Path(table_path)/"pf_violations.tex", index=False,
    #         column_format="c" * formated_output.shape[1],
    #         float_format="{:.2}\\%".format)

    # Economy table.
    balance["energy_bill"] = energy_bill(
        balance, active="load_kwh", fee_date=fee_date, signal="+",
        fee_tz="America/Sao_Paulo")
    balance["extra_reactive_bill"] = reactive_bill(
        balance, active="load_kwh", reactive="load_kvarh", pf="pf",
        fee_date=fee_date, signal="+", target_tz="America/Sao_Paulo",
        fee_tz="America/Sao_Paulo", hour="index")

    savings = []
    # for method, df in zip(["one_pf", "min_q_cost_gen", "opt_gen"],
    #                       [one_pf_balance, min_q_cost_gen_balance,
    #                        opt_gen_balance]):
    for method, df in zip(["one_pf", "opt_gen"],
                          [one_pf_balance, min_q_cost_gen_balance]):
        df = pd.DataFrame(
            {"value": [df.energy_bill.sum(),
                       df.extra_reactive_bill.sum()],
             "no_gen": [balance.energy_bill.sum(),
                        balance.extra_reactive_bill.sum()]},
            index=pd.MultiIndex.from_product(
                [[method], ["energy", "reactive"]], names=["method", "type"]))
        df.loc[(method, "total"), :] = df.sum(axis=0)
        df["savings"] = df.no_gen - df.value
        savings.append(df)
    savings = pd.concat(savings, axis=0)

    if table_path is not None:
        formated_output = savings.map(lambda x: "{:.2f}".format(float(x)))
        formated_output = formated_output.rename(columns=table_labels)
        formated_output = formated_output.rename(table_labels)
        formated_output.index = formated_output.index.rename(table_labels)
        formated_output.to_latex(
            Path(table_path)/"economy.tex",
            column_format=formated_output.reset_index().shape[1] * "c",
            float_format="{:.2}".format)

    # percentile_plot([weight(one_pf_balance), weight(opt_gen_balance)],
    #                 cutoff=92, path=image_path, show=show,
    #                 legend=["Full Generation", "Optimum Generation"])


def weight(balance):
    """Compute how much the extra reactive bill weights on the raw economy."""
    weight = balance.extra_reactive_bill * 100 / balance.raw_economy
    economical = (balance.raw_economy > 0) & (balance.economy > 0)
    loud = weight > 0
    return weight[economical & loud]


if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    # Receive arguments related to the meteorological data.
    _ = parser.add_argument(
        "--meteo_file", type=str,
        default=Path(__file__).parents[2] / "datastore/meteo.csv",
        help="Path to the file having the meteorological data")

    _ = parser.add_argument("--api", type=str, default=None,
                            help="Address to INMET api")

    _ = parser.add_argument("--station", type=str, default=None,
                            help="Name of INMET station")

    # Receive arguments related to the consumption data.
    _ = parser.add_argument(
        "--consumption_file", type=str,
        default=Path(__file__).parents[2] / "datastore/consumption.csv",
        help="Path to the file having the consumption data")

    _ = parser.add_argument(
        "--consumption_agg", type=str, default="hour",
        help="Type of aggregation of he consumption data")

    _ = parser.add_argument(
        "--fee_date", type=str, default="2019-07-06",
        help="Date of the billing period. If None, the current date is used.")

    _ = parser.add_argument(
        "--int_time", type=float, default=0.25,
        help=("Integration time between registers. If None, it will be got"
              + " from data."))

    _ = parser.add_argument(
        "--extra_kvar_c", type=float, default=300,
        help=("Extra capacitive reactive energy that must be added when "
              + " computing the energy balance."))

    _ = parser.add_argument(
        "--extra_kvar_i", type=float, default=0,
        help=("Extra inductive reactive energy that must be added when "
              + " computing the energy balance."))

    # Receive arguments related to the inverters data.
    _ = parser.add_argument(
        "--inverters_file", type=str,
        default=Path(__file__).parents[2] / ("datastore/"
                                             + "inverters_initial_plan.csv"),
        help="Path to the file having the inverters data")

    # Receive as argument the path to the directory where the results must be
    # saved.
    _ = parser.add_argument(
        "--image_path", type=str, default=str(
            Path(__file__).parent / "figures"),
        help="Path to the directory where the images must be saved")
    _ = parser.add_argument(
        "--table_path", type=str, default=str(
            Path(__file__).parent / "tables"),
        help="Path to the directory where the tables must be saved")

    # Receive as argument the path to the julia runtime.
    _ = parser.add_argument(
        "--julia_runtime", type=str,
        help="Path to the directory where julia is installed.")

    # Receive arguments related to the log file.
    _ = parser.add_argument(
        "--log_file", type=str,
        default=Path(__file__).parent / "sbse2020.log",
        help="Path to the log file.")

    # Receive as argument whether I want the plots to be shown.
    _ = parser.add_argument(
        "--show", action="store_true",
        help="Bool saying whether you want the plots to be shown.")

    # Receive as argument whether I want to compute the operation temperature
    # of the modules.
    _ = parser.add_argument(
        "--compute_temperature", action="store_true",
        help=("Bool saying whether you want the operational temperature of "
              "the modules to be computed."))

    # Receive as argument whether I want to pre-process the data.
    _ = parser.add_argument(
        "--pre_process", action="store_true",
        help=("Bool saying whether you want to pre-processes data so it will "
              + "keep only registers in generation time and will mark the "
              + "hour and the season."))

    # Receive arguments to format the figures and tables.
    _ = parser.add_argument(
        "--language", type=str,
        default="english",
        help="Language of figures and tables.")

    _ = parser.add_argument(
        "--fig_height", type=float,
        default=12,
        help="Height of the figure that will be plotted.")

    _ = parser.add_argument(
        "--fig_width", type=float,
        default=17,
        help="Width of the figure that will be plotted.")

    _ = parser.add_argument(
        "--pf_height", type=float,
        default=12,
        help="Height of the power factor boxplot.")

    _ = parser.add_argument(
        "--pf_width", type=float,
        default=17,
        help="Width of the power factor boxplot.")

    _ = parser.add_argument(
        "--fontsize", type=int,
        default=10,
        help="Fontsize of the texts of the plot.")

    # Get the arguments.
    args = parser.parse_args()
    meteo_file = args.meteo_file
    api = args.api
    station = args.station
    consumption_file = args.consumption_file
    consumption_agg = args.consumption_agg
    fee_date = args.fee_date
    int_time = args.int_time
    extra_kvar_c = args.extra_kvar_c
    extra_kvar_i = args.extra_kvar_i
    inverters_file = args.inverters_file
    image_path = args.image_path
    table_path = args.table_path
    log_file = parser.parse_args().log_file
    julia_runtime = parser.parse_args().julia_runtime
    show = args.show
    compute_temperature = args.compute_temperature
    pre_process = args.pre_process
    language = args.language
    fig_height = args.fig_height
    fig_width = args.fig_width
    pf_height = args.pf_height
    pf_width = args.pf_width
    fontsize = args.fontsize

    # Time interval of the analisys.
    start_date = "2015-02-22"
    end_date = "2017-03-24"

    # Data timezone and target timezone. I'm using São Paulo timezone which
    # is the same as Vitória, where the plant is located.
    data_tz = "Etc/UTC"
    target_tz = "America/Sao_Paulo"

    # Check whether we have a meteo file and Path it.
    if meteo_file is not None:
        meteo_file = Path(meteo_file)

    # Configuring the logger.
    FORMAT = ("[%(asctime)s] %(filename)s:%(lineno)s - %(funcName)s"
              + " - %(levelname)s: %(message)s")
    logging.basicConfig(format=FORMAT, filename=log_file, encoding='utf-8',
                        datefmt="%Y-%m-%d %H:%M:%S", level=logging.INFO)

    # Configuring the language of the plots.
    dap.set_language(language)

    # Check whether we have a None fee date and void it.
    if fee_date == "None":
        fee_date = None

    main(start_date, end_date, meteo_file, api, station, consumption_file,
         consumption_agg, int_time, extra_kvar_c, extra_kvar_i, inverters_file,
         data_tz, target_tz, fee_date, pre_process, image_path, table_path,
         julia_runtime, show, compute_temperature, fig_width, fig_height,
         pf_width, pf_height, fontsize)
