#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Report published at the 2023 IEEE 17th Brazilian Power Electronics
Conference and 8th IEEE Southern Power Electronics Conference (COBEP/SPEC).

:command:
    python3 -m capitu.src.scripts.spec2023
    --inverters_file "./capi../capitu/datastore/inverters_v1.2.0.csv"
    --consumption_file "./capi../capitu/datastore/consumption.csv"
    --extra_capacitive_energy 300
    --generation_file "path_to_the_generation_file"
    --pdfs_path "./pdfs"
    --image_path "./figures"
    --table_path "./tables"
"""
name = "spec2023"
__author__ = "Daniel Campos Pompermayer [gitlab.com/daniel_pompermayer]"
