#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Report published at the 2023 IEEE 17th Brazilian Power Electronics
Conference and 8th IEEE Southern Power Electronics Conference (COBEP/SPEC).
"""

import pytz
import logging
import argparse
import pandas as pd
from pathlib import Path
from datetime import datetime as dt
from ....src import dap
from ...dap.stats.modeling.consumption import consumption as model_consumption
from ...dap.stats.modeling.generation import generation as model_generation
from ...dap.stats.simulation import simulation as mc_simulation
from ...dap.getter.inverters import get as get_inverters
from ...dap.plotter.labels import table_labels
from ...dap.premises.clusters import get_season_clusters


def main(cons_start_date, cons_end_date, cons_pdfs_file, consumption_file,
         gen_start_date, gen_end_date, gen_pdfs_file, generation_file,
         inverters_file, extra_capacitive_energy, bounds, data_tz, target_tz,
         fee_date, show, ignore_simulation, pdfs_path, image_path, table_path,
         fig_height, fig_width, fontsize, density_peak, pdfs_label,
         best_label):

    """
    Main routine of the paper published at the 2023 IEEE 17th Brazilian Power
    Electronics Conference and 8th IEEE Southern Power Electronics Conference
    (COBEP/SPEC).

    .. warning::
        This routine is not actually yielding the exact same results as those
        published. The reason for this is that when I first coded the
        Kolmogorov-Smirnov Test, I thought I'd need a Monte Carlo Simulation
        to fit the D statistic distribution. But now I realize it's not
        necessary. Then, just because of that change, instead of modeling the
        reactive consumption as a Proportional Reversed Hazard Logistic
        Distribution, it has been modeled as a Double Weibull Distribution. But
        it doesn't qualitatively change the results.

    :param start_date: date of the begin of the interval of data.
    :type start_date: str, format: %Y-%m-%d
    :param end_date: date of the end of the interval of data.
    :type end_date: str, format: %Y-%m-%d
    :param cons_pdfs_file: String having the path and the name of the file
        having the consumption pdfs data.
    :type cons_pdfs_file: str or None
    :param consumption_file: String having the path and the name of the file
        having the consumption data. If cons_pdfs_file is not None, this
        parameter will be ignored.
    :type consumption_file: str
    :param gen_pdfs_file: String having the path and the name of the file
        having the generation pdfs data.
    :type gen_pdfs_file: str or None
    :param generation_file: String having the path and the name of the file
        having the generation data. If gen_pdfs_file is not None, this
        parameter will be ignored.
    :type generation_file: str
    :param inverters_file: Path to the inverters file.
    :type inverters_file: str
    :param extra_capacitive_energy: Additional capacitive reactive energy.
    :type extra_capacitive_energy: float]
    :param bounds: If True, constrain the found density parameters to the
        inferred bounds.
    :type bounds: bool, default: False
    :param data_tz: name of the pytz timezone where the meteorological station
        is located.
    :type data_tz: str
    :param target_tz: Name of the target pytz timezone.
    :type target_tz: str
    :param fee_date: Date for which the market fees are to be retrieved. If
        None, the index of the consumption DataFrame is used.
    :type fee_date: str or None
    :param show: If True, a boxplot of the meteorological data and of the
        consumption data  will be presented on the screen.
    :type show: bool
    :param ignore_simulation: If True, the simulation will not run.
    :type ignore_simulation: bool
    :param pdfs_path: Address to the directory where the pdf tables will be
        saved. If None, the pdfs will not be saved.
    :type pdfs_path: str or None
    :param image_path: Address to the directory where the plots will be
        saved. If None, the plot will not be saved.
    :type image_path: str or None
    :param table_path: Address to the directory where the tables will be
        saved. If None, the plot will not be saved.
    :type table_path: str or None
    """

    logging.info("Starting process.")

    # Get, adjust and plot consumption data.
    if cons_pdfs_file is None:
        consumption_pdfs, _, _ = model_consumption(
            cons_start_date, cons_end_date, consumption_file, target_tz,
            localize_tz=True, extra_kvar_i=0,
            extra_kvar_c=extra_capacitive_energy, bounds=bounds,
            generation_time_only=True, show=show,
            pdfs_path=pdfs_path, tables_path=table_path,
            pdfs_label=pdfs_label, best_label=best_label,
            image_path=image_path, fig_height=fig_height, fig_width=fig_width,
            fontsize=fontsize, density_peak=density_peak)
    else:
        consumption_pdfs = pd.read_csv(cons_pdfs_file, sep=";", decimal=".",
                                       index_col=0)

    logging.info("Consumption data has been modeled.")

    # Load the inverters data and check ho many inverters we have.
    inverters = get_inverters(inverters_file, campus="G", void_sn="drop")
    num_of_inverters = len(inverters[
        ~(inverters.expiration_date <= dt.now(pytz.timezone(target_tz)))])

    logging.info("Got the number of inverters.")

    # Get, adjust and plot generation data.
    if gen_pdfs_file is None:
        generation_pdfs, _, _ = model_generation(
            gen_start_date, gen_end_date, generation_file, data_tz, target_tz,
            localize_tz=True, bounds=bounds, generation_time_only=True,
            show=show, pdfs_path=pdfs_path, pdfs_label=pdfs_label,
            tables_path=table_path, best_label=best_label,
            image_path=image_path, fig_height=fig_height, fig_width=fig_width,
            fontsize=fontsize, density_peak=density_peak)
    else:
        generation_pdfs = pd.read_csv(gen_pdfs_file, sep=";", decimal=".",
                                      index_col=0)

    logging.info("Generation data has been modeled.")

    # Get the clusters from 2023.
    clusters = get_season_clusters(2023, path=table_path)

    if ignore_simulation:
        logging.info("Simulation ignored. Finished.")
        return

    logging.info("Got the clusters.")
    (pf_ecdfs, pf_table, q_cost_ecdfs, q_cost_table,
     avg_energy_savings, mc_realizations) = mc_simulation(
         generation_pdfs, consumption_pdfs, clusters, num_of_inverters,
         target_tz, fee_date, sample_size=20000, energy_savings=True,
         plotting=True, show=show, image_path=image_path,
         table_path=table_path, seed=42)

    logging.info("Got pf and q_cost tables.")

    formated_output = pf_table.map(
        lambda x: "{:.2f}\\%".format(x * 100)).reset_index().rename(
            columns=table_labels)
    formated_output.to_latex(
        Path(table_path)/"pf_table.tex",
        index=False, column_format="ccc",
        float_format="{:.2}\\%".format)

    q_cost_table = q_cost_table.reset_index().rename(columns=table_labels)
    q_cost_table.to_latex(
        Path(table_path)/"q_cost_table.tex",
        index=False, column_format="ccc")

    avg_energy_savings = avg_energy_savings.reset_index().rename(
        columns=table_labels)
    avg_energy_savings.to_latex(
        Path(table_path)/"avg_energy_savings.tex",
        index=False, column_format="cc")

    logging.info("Finished.")


if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    # Receive arguments related to the inverters data.
    _ = parser.add_argument(
        "--inverters_file", type=str,
        default=Path(__file__).parents[2] / ("datastore/"
                                             + "inverters_v1.2.0.csv"),
        help="Path to the file having the inverters data")

    # Receive arguments related to the consumption data.
    _ = parser.add_argument(
        "--cons_pdfs_file", type=str,
        default=None,
        help="Path to the file having the consumption pdfs data")

    _ = parser.add_argument(
        "--consumption_file", type=str,
        default=Path(__file__).parents[2] / "datastore/consumption.csv",
        help="Path to the file having the consumption data")

    _ = parser.add_argument(
        "--extra_capacitive_energy", type=float, default=300,
        help=("Extra capacitive reactive energy that must be added when "
              + " computing the energy balance."))

    # Receive arguments related to the generation data.
    _ = parser.add_argument(
        "--gen_pdfs_file", type=str,
        default=None,
        help="Path to the file having the generation pdfs data")

    _ = parser.add_argument(
        "--generation_file", type=str,
        default=None,
        help="Path to the file having the generation data")

    # Receive arguments related to the bounds of the distributions.
    _ = parser.add_argument(
        "--bounds", action="store_true",
        help=("If True, constrain the found density parameters to inferred "
              + "bounds."))

    # Receive arguments related to the log file.
    _ = parser.add_argument(
        "--log_file", type=str,
        default=Path(__file__).parent / "spec2023.log",
        help="Path to the log file.")

    # Receive as argument whether I want the plots to be shown.
    _ = parser.add_argument(
        "--show", action="store_true",
        help="Bool saying whether you want the plots to be shown.")

    # Receive as argument whether I want the plots to be shown.
    _ = parser.add_argument(
        "--ignore_simulation", action="store_true",
        help="Bool saying whether you don't want to run the simulation.")

    # Receive arguments related to the path to save figures and tables.
    _ = parser.add_argument(
        "--pdfs_path", type=str,
        default=Path(__file__).parent / "pdfs",
        help="Path to save the pdfs.")

    _ = parser.add_argument(
        "--image_path", type=str,
        default=Path(__file__).parent / "figures",
        help="Path to save the figures.")

    _ = parser.add_argument(
        "--table_path", type=str,
        default=Path(__file__).parent / "tables",
        help="Path to save the tables.")

    # Receive arguments to format the figures and tables.
    _ = parser.add_argument(
        "--language", type=str,
        default="english",
        help="Language of figures and tables.")

    _ = parser.add_argument(
        "--pdfs_label", type=str,
        default="Single Fit",
        help="Label for the density plot represented by the `pdfs` parameter.")

    _ = parser.add_argument(
        "--best_label", type=str,
        default="Best Fit",
        help=("Label for the density plot represented by the `best_pdfs` "
              + "parameter."))

    _ = parser.add_argument(
        "--fig_height", type=float,
        default=12,
        help="Height of the figure that will be plotted.")

    _ = parser.add_argument(
        "--fig_width", type=float,
        default=17,
        help="Width of the figure that will be plotted.")

    _ = parser.add_argument(
        "--fontsize", type=int,
        default=10,
        help="Fontsize of the texts of the plot.")

    _ = parser.add_argument(
        "--density_peak", type=float,
        default=1.1,
        help=("Maximum ratio between the biggest histogram bar and "
              + "the density plot size. If the density plot size is bigger "
              + "than the biggest histogram bar, it will be cropped in "
              + "density_peak times the size of biggest bar."))

    # Get the arguments.
    args = parser.parse_args()
    inverters_file = args.inverters_file
    cons_pdfs_file = args.cons_pdfs_file
    consumption_file = args.consumption_file
    extra_capacitive_energy = args.extra_capacitive_energy
    gen_pdfs_file = args.gen_pdfs_file
    generation_file = args.generation_file
    bounds = args.bounds
    log_file = args.log_file
    show = args.show
    ignore_simulation = args.ignore_simulation
    pdfs_path = args.pdfs_path
    image_path = args.image_path
    table_path = args.table_path
    language = args.language
    pdfs_label = args.pdfs_label
    best_label = args.best_label
    fig_height = args.fig_height
    fig_width = args.fig_width
    fontsize = args.fontsize
    density_peak = args.density_peak

    # Time interval of the analisys.
    cons_start_date = "2015-02-22"
    cons_end_date = "2017-03-24"
    gen_start_date = "2021-01-01"
    gen_end_date = "2022-12-30"
    fee_date = "2023-07-17"

    # Data timezone and target timezone. I'm using São Paulo timezone which
    # is the same as Vitória, where the plant is located.
    data_tz = None
    target_tz = "America/Sao_Paulo"

    # Configuring the logger.
    FORMAT = ("[%(asctime)s] %(filename)s:%(lineno)s - %(funcName)s"
              + " - %(levelname)s: %(message)s")
    logging.basicConfig(format=FORMAT, filename=log_file, encoding='utf-8',
                        datefmt="%Y-%m-%d %H:%M:%S", level=logging.INFO)

    # Configuring the language of the plots.
    dap.set_language(language)

    main(cons_start_date, cons_end_date, cons_pdfs_file, consumption_file,
         gen_start_date, gen_end_date, gen_pdfs_file, generation_file,
         inverters_file, extra_capacitive_energy, bounds, data_tz, target_tz,
         fee_date, show, ignore_simulation, pdfs_path, image_path, table_path,
         fig_height, fig_width, fontsize, density_peak, pdfs_label, best_label)
