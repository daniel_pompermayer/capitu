#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Test of the spec2023 script.
"""

import filecmp
import subprocess
from pathlib import Path


def test_script(tmp_path):

    # Create some directories to store the generated files.
    images_dir = tmp_path / "figures"
    images_dir.mkdir(parents=True, exist_ok=True)

    tables_dir = tmp_path / "tables"
    tables_dir.mkdir(parents=True, exist_ok=True)

    # Run the spec2019 script.
    subprocess.run(["python3", "-m", "capitu.src.scripts.spec2023",
                    "--inverters_file",
                    "./capi../capitu/datastore/inverters_v1.2.0.csv",
                    "--cons_pdfs_file",
                    "./capitu/src/scripts/spec2023/pdfs/"
                    + "single_consumption_pdfs.csv",
                    "--gen_pdfs_file",
                    "./capitu/src/scripts/spec2023/pdfs/"
                    + "single_generation_pdfs.csv",
                    "--image_path", str(images_dir),
                    "--table_path", str(tables_dir)])

    # Verify if the files have been properly created.
    ref_figures_dir = Path("./capitu/src/scripts/spec2023/figures")
    for figure in ["hist_pf.pgf", "hist_q_cost.pgf"]:
        fig = images_dir / figure
        assert filecmp.cmp(fig, ref_figures_dir / figure)

    ref_tables_dir = Path("./capitu/src/scripts/spec2023/tables")
    for table in ref_tables_dir.glob("*.*"):
        tbl = tables_dir / table.name
        assert filecmp.cmp(tbl, table)
