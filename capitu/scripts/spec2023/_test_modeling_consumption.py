#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Test of the part of the spec2023 script that models the consumption. I'm not
testing the generation because I lack the means to put the generation register
file online.
"""

import filecmp
import subprocess
from pathlib import Path


def test_script(tmp_path):

    # Create some directories to store the generated files.
    images_dir = tmp_path / "figures"
    images_dir.mkdir(parents=True, exist_ok=True)

    pdfs_dir = tmp_path / "pdfs"
    pdfs_dir.mkdir(parents=True, exist_ok=True)

    # Run the spec2019 script.
    subprocess.run(["python3", "-m", "capitu.src.scripts.spec2023",
                    "--consumption_file",
                    "./capi../capitu/datastore/consumption.csv",
                    "--extra_capacitive_energy", "300",
                    "--gen_pdfs_file",
                    "./capitu/src/scripts/spec2023/pdfs/"
                    + "single_generation_pdfs.csv",
                    "--ignore_simulation",
                    "--pdfs_path", str(pdfs_dir),
                    "--image_path", str(images_dir)])

    # Verify if the files have been properly created.
    ref_figures_dir = Path("./capitu/src/scripts/spec2023/figures")
    for figure in ref_figures_dir.glob("*_load_*.pgf"):
        fig = images_dir / figure.name
        assert filecmp.cmp(fig, figure)

    ref_pdfs_dir = Path("./capitu/src/scripts/spec2023/pdfs")
    for pdf_table in ref_pdfs_dir.glob("*_consumption_pdfs.csv"):
        pdf = pdfs_dir / pdf_table.name
        assert filecmp.cmp(pdf, pdf_table)
