#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Scripts used in the calculations of publications that employed the application.
"""
name = "scripts"
__author__ = "Daniel Campos Pompermayer [gitlab.com/daniel_pompermayer]"
