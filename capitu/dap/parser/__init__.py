#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
The parsers module provides functions that convert string formated dates to
datetime.

    :variables:
        consumption_date (str): Format of the date in the consumption data
            ("%d/%m/%Y %H:%M").
        gen_data_columns (dict): Dictionary translating the generation data
            columns to more convenient names.
        gen_float_columns (list): List of columns containing float values in
            the generation data.
        gen_columns_agg (dict): Dictionary specifying the aggregation methods
            for each column in the generation data.

    :example:
        >>> from capitu.dap.parser import (
        ...     consumption_date, gen_data_columns, gen_float_columns,
        ...     gen_columns_agg)
        >>> consumption_date
        '%d/%m/%Y %H:%M'
        >>> gen_data_columns["Time"]
        'date'
        >>> gen_float_columns[0:3]
        ['v_mppt_1_v', 'v_mppt_2_v', 'v_mppt_3_v']
        >>> gen_columns_agg["date"]
        'last'
"""

import pandas as pd
import datetime as dt


name = "parser"
__author__ = "Daniel Campos Pompermayer [gitlab.com/daniel_pompermayer]"


# Format of the date in the consumption data.
consumption_date = "%d/%m/%Y %H:%M"

# Translate the generation data columns to more convenient names.
gen_data_columns = {"Time": "date", "Working Mode": "mode",
                    "V MPPT 1(V)": "v_mppt_1_v", "V MPPT 2(V)": "v_mppt_2_v",
                    "V MPPT 3(V)": "v_mppt_3_v", "V MPPT 4(V)": "v_mppt_4_v",
                    "I MPPT 1(A)": "i_mppt_1_a", "I MPPT 2(A)": "i_mppt_2_a",
                    "I MPPT 3(A)": "i_mppt_3_a", "I MPPT 4(A)": "i_mppt_4_a",
                    "Ua(V)": "u_1_v", "Ub(V)": "u_2_v", "Uc(V)": "u_3_v",
                    "I AC 1(A)": "i_a_a", "I AC 2(A)": "i_b_a",
                    "I AC 3(A)": "i_c_a", "F AC 1(Hz)": "f_a_hz",
                    "F AC 2(Hz)": "f_b_hz", "F AC 3(Hz)": "f_c_hz",
                    "Power(W)": "p_w", "Temperature(℃)": "temperature_celsius",
                    "Generation Today(kWh)": "generation_today_kwh",
                    "Total Generation(kWh)": "total_generation_kwh",
                    "H Total(h)": "total_time_h", "Istr1(A)": "i_str1_a",
                    "Istr2(A)": "i_str2_a", "Istr3(A)": "i_str3_a",
                    "Istr4(A)": "i_str4_a", "Istr5(A)": "i_str5_a",
                    "Istr6(A)": "i_str6_a", "Istr7(A)": "i_str7_a",
                    "Istr8(A)": "i_str8_a", "Istr9(A)": "i_str9_a",
                    "Istr10(A)": "i_str10_a", "Istr11(A)": "i_str11_a",
                    "Istr12(A)": "i_str12_a", "RSSI(%)": "rssi_pc", "PF": "pf",
                    "ReactivePower(kVar)": "q_kvar",
                    "LeakageCurrent(mA)": "leakage_i_ma",
                    "ISOLimit(kΩ)": "isolimit_kohm"}

# These are the columns containing float values.
gen_float_columns = ["v_mppt_1_v", "v_mppt_2_v", "v_mppt_3_v", "v_mppt_4_v",
                     "i_mppt_1_a", "i_mppt_2_a", "i_mppt_3_a", "i_mppt_4_a",
                     "u_1_v", "u_2_v", "u_3_v", "i_a_a", "i_b_a", "i_c_a",
                     "f_a_hz", "f_b_hz", "f_c_hz", "p_w",
                     "temperature_celsius", "generation_today_kwh",
                     "total_generation_kwh", "total_time_h", "i_str1_a",
                     "i_str2_a", "i_str3_a", "i_str4_a", "i_str5_a",
                     "i_str6_a", "i_str7_a", "i_str8_a", "i_str9_a",
                     "i_str10_a", "i_str11_a", "i_str12_a", "rssi_pc", "pf",
                     "q_kvar", "leakage_i_ma", "isolimit_kohm"]

# These are the aggregation methods for each column.
gen_columns_agg = {"date": "last", "mode": "last", "serial_number": "first",
                   "v_mppt_1_v": "mean", "v_mppt_2_v": "mean",
                   "v_mppt_3_v": "mean", "v_mppt_4_v": "mean",
                   "i_mppt_1_a": "mean", "i_mppt_2_a": "mean",
                   "i_mppt_3_a": "mean", "i_mppt_4_a": "mean",
                   "u_1_v": "mean", "u_2_v": "mean", "u_3_v": "mean",
                   "i_a_a": "mean", "i_b_a": "mean", "i_c_a": "mean",
                   "f_a_hz": "mean", "f_b_hz": "mean", "f_c_hz": "mean",
                   "p_w": "mean", "temperature_celsius": "mean",
                   "generation_today_kwh": "last",
                   "total_generation_kwh": "last", "total_time_h": "last",
                   "i_str1_a": "mean", "i_str2_a": "mean", "i_str3_a": "mean",
                   "i_str4_a": "mean", "i_str5_a": "mean", "i_str6_a": "mean",
                   "i_str7_a": "mean", "i_str8_a": "mean", "i_str9_a": "mean",
                   "i_str10_a": "mean", "i_str11_a": "mean",
                   "i_str12_a": "mean", "rssi_pc": "mean", "pf": "mean",
                   "q_kvar": "mean", "leakage_i_ma": "mean",
                   "isolimit_kohm": "mean", "int_time": "sum"}


def meteo_date(date):
    """
    Converts a string date in the format used by INMET to dt.datetime.
    (https://portal.inmet.gov.br/).

    :param date: The date to be converted.
    :type date: str, format: "%Y-%m-%d %H"
    :return: The converted date.
    :rtype: datetime.datetime

    :example:
        >>> from capitu.dap.parser import meteo_date
        >>> meteo_date("1969-07-20 20")
        datetime.datetime(1969, 7, 20, 20, 0)
    """
    return dt.datetime.strptime(date, "%Y-%m-%d %H")


def generation_date(time):
    """
    Converts a string date in the format used by the inverter platform to
    pd.Timestamp.

    :param time: The time to be converted.
    :type time: str, format: "%m.%d.%Y %H:%M:%S"
    :return: The converted time.
    :rtype: pandas.Timestamp

    :example:
        >>> from capitu.dap.parser import generation_date
        >>> generation_date("07.20.1969 20:17:00")
        Timestamp('1969-07-20 20:17:00')
    """
    return pd.to_datetime(time, format="%m.%d.%Y %H:%M:%S")


def datetime_to_season(datetimes):
    """
    Retrieves seasons corresponding to dates in a pandas DatetimeIndex.

    :param datetimes: Index of a pandas DataFrame containing dates.
    :type datetimes: pandas.core.indexes.datetimes.DatetimeIndex
    :return: List of seasons corresponding to the DataFrame index dates.
    :rtype: list[str]

    :example:
        >>> import pandas as pd
        >>> from capitu.dap.parser import datetime_to_season
        >>> nice_events = pd.DataFrame({"event": [
        ...     "First Moon Landing", "Higgs Boson detection",
        ...     "Gravitational waves"]}, index=pd.to_datetime([
        ...     "1969-7-20", "2012-07-04", "2015-09-14"]))
        >>> datetime_to_season(nice_events.index)
        ['winter', 'winter', 'winter']
    """

    # Calculation based on Quang Hoang's Stack Overflow answer
    # (https://stackoverflow.com/a/60285720). Month * 100 allows month and
    # day sum without losing month info. As summer is split between years,
    # we offset the date so the year "starts" in autumn. Modulo helps handle
    # negative values.
    date_offset = ((datetimes.month * 100 + datetimes.day) - 320) % 1300
    seasons = pd.cut(date_offset, [0, 300, 602, 900, 1300],
                     labels=["autumn", "winter", "spring", "summer"],
                     include_lowest=True)
    return list(seasons)
