#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
The Power Factor module provides functions for computing the power factor
of a facility employing a data frame with the consumption data.
"""
name = "power_factor"
__author__ = "Daniel Campos Pompermayer [gitlab.com/daniel_pompermayer]"
