#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Compute the power factor of a facility employing a data frame with the
consumption data.
"""


import sys
import numpy as np


def compute(data, active="kw", reactive="kvar", capacitive=False,
            signal="+"):
    """
    Compute the power factor of a facility employing a data frame with the
    consumption data.

    :param data: time indexed consumption data frame having a column for
        the active power and a column for the reactive power.
    :type data: pandas.core.frame.DataFrame
    :param active: string with the name of the active power column.
    :type active: str, default: "kw"
    :param reactive: string with the name of the reactive power column.
    :type reactive: str, default: "kvar"
    :param capacitive: string with the name of an optional column having the
        capactive reactive power registered separately. If that column is not
        present, set capacitive as None.
    :type capacitive: str or None, default None
    :param signal: If "-", positive powers are understood as flowing from the
        load to the source. Otherwise, positive powers are understood as
        flowing from the source to the load.
    :type signal: str, default "+"
    :return: pandas Series with the computed power factor.
    :rtype: pandas.core.series.Series

    :example:

    >>> import pandas as pd
    >>> from capitu.dap.power_factor.compute import compute as compute_pf
    >>> data = pd.DataFrame({
    ...    'load_kw': [100, 200, 150],
    ...    'load_kvar_i': [50, 100, 75],
    ...    'load_kvar_c': [10, 25, 20]},
    ...    index=pd.date_range(start='2022-01-01', periods=3, freq='D'))
    >>> compute_pf(data, active="load_kw", reactive="load_kvar_i",
    ...            capacitive="load_kvar_c", signal="+")
    2022-01-01    0.928477
    2022-01-02    0.936329
    2022-01-03    0.938876
    Freq: D, Name: pf, dtype: float64
    """

    # Make a copy so we do not bother the external variables.
    data = data.copy()

    # Check whether "reactive energy" must be calculated.
    if isinstance(capacitive, str):
        kvar = data[reactive] - data[capacitive]
    else:
        kvar = data[reactive]

    # Check whether we must change the signals of the powers.
    if signal == "-":
        data[active] = data[active] * -1
        kvar = kvar * -1

    # Compute the apparent power
    data["apparent"] = np.sqrt(data[active]**2 + kvar**2)

    # Compute the power factor.
    data["pf"] = data[active]/data["apparent"]

    # Check whether the apparent energy is too small for being senseless
    # computing the power factor.
    too_small = data["apparent"] < 0.01
    data.loc[too_small, "pf"] = 1

    # Prevent issues with division by zero caused by a zero power factor.
    zero_pf = data["pf"] == 0
    data.loc[zero_pf, "pf"] = sys.float_info.min

    return data["pf"]
