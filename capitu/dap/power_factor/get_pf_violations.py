#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This module provides a function for analyzing power factor violations in a
balance dataset.
"""

import pandas as pd
from pathlib import Path
from ..plotter.boxplot import boxplot
from ..plotter.labels import table_labels
from ..power_factor.less_than_ideal import less_than_ideal
from ..premises.market import brazilian_ideal_pf as ideal_pf


def get_pf_violations(balance, one_pf_balance, min_q_cost_gen_balance=None,
                      table_export_path=None, image_export_path=None,
                      show=False, ax_height=12, ax_width=16, fontsize=8,
                      old_pf_label="old_pf", new_pf_label="new_pf",
                      min_q_cost_pf_label="min_q_gen_pf", format="pgf"):

    """
    Analyze power factor violations in a balance dataset.

    :param balance: Original balance dataset.
    :type balance: pandas.core.frame.DataFrame
    :param one_pf_balance: Processed balance dataset with unitary power factor
        generation.
    :type one_pf_balance: pandas.core.frame.DataFrame
    :param min_q_cost_gen_balance: Processed balance dataset with an optmized
        power factor generation.
    :type min_q_cost_gen_balance: pandas.core.frame.DataFrame
    :param table_export_path: Path to export the table comparing power factors.
        If None, the table is not exported.
    :type table_export_path: str, Default: None
    :param image_export_path: Path to export the boxplot of the power factor
        with and without the generation integration. If None, the no image will
        be exported.
    :type image_export_path: str, Default: None
    :param show: If True, displays the plot on the screen.
    :param ax_height: The height of each subplot.
    :type ax_height: int or float, default: 12
    :param ax_width: The width of each subplot.
    :type ax_width: int or float, default: 16
    :param fontsize: The font size of the axis labels and title.
    :type fontsize: int, default: 8
    :param old_pf_label: The label of the old pf column in the result data
        frame.
    :type old_pf_label: str, default: "old_pf"
    :param new_pf_label: The label of the new pf column in the result data
        frame.
    :type new_pf_label: str, default: "new_pf"
    :param min_q_cost_pf_label: The label of the min_q_cost pf column in the
        result data frame.
    :type min_q_cost_pf_label: str, default: "min_q_gen_pf"
    :type show: bool, Default: False
    :param format: Specifies the file format for saving the plot.
    :type format: str, default: "pgf"
    :return: DataFrame indicating power factor violations.
    :rtype: pandas.core.frame.DataFrame

    :example:
        >>> import os
        >>> import filecmp
        >>> import pandas as pd
        >>> from PIL import Image, ImageChops
        >>> from capitu import dap
        >>> from capitu.dap.balance.get_one_pf import get_one_pf
        >>> from capitu.dap.power_factor.get_pf_violations import (
        ...     get_pf_violations)
        >>> start_date = '2015-03-01'
        >>> end_date = '2015-05-31'
        >>> meteo_file = '../capitu/datastore/meteo.csv'
        >>> consumption_file = '../capitu/datastore/consumption.csv'
        >>> inverters_file = '../capitu/datastore/inverters_v1.2.0.csv'
        >>> dap.set_language("english")
        >>> balance, one_pf_balance = get_one_pf(
        ...    start_date, end_date, consumption_file, inverters_file,
        ...    meteo_file, extra_kvar_c=300, extra_kvar_i=0, api=None,
        ...    station=None, campus="G", inverters_date="2023-01-01",
        ...    void_sn="drop", compute_temperature=False, uplimit="nominal",
        ...    coherent=False, consumption_agg="hour", fee_date="2024-03-01",
        ...    data_tz="Etc/UTC", target_tz="America/Sao_Paulo",
        ...    localize_tz=True, show=False, image_export_path=None)
        >>> pf_violations  = get_pf_violations(
        ...     balance, one_pf_balance,
        ...     table_export_path="./",
        ...     image_export_path="./", show=False, format="png")
        >>> pf_violations
           old_pf  new_pf
        0   0.00%   0.00%
        1   0.00%   0.00%
        2   0.00%   0.00%
        3   0.00%   0.00%
        4   1.18%   1.18%
        5   0.00%   0.00%
        6   0.00%   0.00%
        7   1.18%  37.65%
        8   0.00%  40.00%
        9   1.18%  54.12%
        10  1.18%  61.18%
        11  1.18%  63.53%
        12  1.18%  58.82%
        13  0.00%  61.18%
        14  0.00%  58.82%
        15  0.00%  54.12%
        16  0.00%  44.71%
        17  0.00%  17.65%
        18  0.00%   1.18%
        19  0.00%   0.00%
        20  0.00%   0.00%
        21  0.00%   0.00%
        22  0.00%   0.00%
        23  0.00%   0.00%
        >>> target = Image.open('./box_new_pf.png')
        >>> template = Image.open('../capitu/datastore/doctest/box_new_pf.png')
        >>> diff = ImageChops.difference(target, template)
        >>> diff.getbbox() is None
        True
        >>> os.remove('./box_new_pf.png')
        >>> target = Image.open('./box_old_pf.png')
        >>> template = Image.open('../capitu/datastore/doctest/box_old_pf.png')
        >>> diff = ImageChops.difference(target, template)
        >>> diff.getbbox() is None
        True
        >>> os.remove('./box_old_pf.png')
        >>> filecmp.cmp(
        ...     './pf_violations.tex',
        ...     '../capitu/datastore/doctest/pf_violations.tex')
        True
        >>> os.remove('./pf_violations.tex')
    """

    # Get how many times the power factor was smaller than the regulatory value
    # with no generation and with unitary power factor generation.
    pf_violations = pd.DataFrame([], index=balance.index.hour.unique())
    pf_violations[old_pf_label] = less_than_ideal(balance, ideal_pf)
    pf_violations.loc[:, new_pf_label] = less_than_ideal(
        one_pf_balance, ideal_pf)

    # Check whether I also have a min_q_cost_gen_balance information.
    if min_q_cost_gen_balance is not None:
        pf_violations.loc[:, min_q_cost_pf_label] = less_than_ideal(
            min_q_cost_gen_balance, ideal_pf)

    # Save a table comparing new and old power factors.
    if table_export_path is not None:
        formated_output = pf_violations.map(
            lambda x: "{:.2f}\\%".format(float(x.strip("%"))))
        formated_output = formated_output.reset_index().rename(
            {"index": "hour"}, axis=1).rename(columns=table_labels)
        formated_output.to_latex(
            Path(table_export_path)/"pf_violations.tex", index=False,
            column_format="c" * formated_output.shape[1],
            float_format="{:.2}\\%".format)

    if show or (image_export_path is not None):

        # Merge the power factor series.
        pfs = pd.DataFrame([], index=balance.index)
        pfs["old_pf"] = balance.pf
        pfs["new_pf"] = one_pf_balance.pf

        # Merge the min_q_cost_gen_balance pf if I have it.
        if min_q_cost_gen_balance is not None:
            pfs["min_q_cost_pf"] = min_q_cost_gen_balance.pf

        # Plot power factor.
        boxplot(pfs, path=image_export_path, show=show, x="hour",
                y="old_pf", scale_factor=1, ylim=None, absolute=True, whis=1.5,
                ax_height=ax_height, ax_width=ax_width, format=format,
                fontsize=fontsize)
        boxplot(pfs, path=image_export_path, show=show, x="hour",
                y="new_pf", scale_factor=1, ylim=None, absolute=True, whis=1.5,
                ax_height=ax_height, ax_width=ax_width, format=format,
                fontsize=fontsize)
        if min_q_cost_gen_balance is not None:
            boxplot(pfs, path=image_export_path, show=show, x="hour",
                    y="min_q_cost_pf", scale_factor=1, ylim=None,
                    absolute=True, whis=1.5, ax_height=ax_height,
                    ax_width=ax_width, format=format, fontsize=fontsize)

    return pf_violations
