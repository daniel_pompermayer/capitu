#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This module provides a function for computing the percentage of hourly records
where the power factor is smaller than the ideal value.
"""

import numpy as np


def less_than_ideal(balance, ideal_pf):
    """
    Compute the percentage of hourly records where the power factor is smaller
    than the ideal value.

    :param balance: A time-indexed DataFrame containing a column named "pf" for
        power factor values.
    :type balance: pandas.core.frame.DataFrame
    :param ideal_pf: The value of the ideal power factor.
    :type ideal_pf: float
    :return: An array representing the percentage of hourly records where the
        power factor is smaller than the ideal.
    :rtype: numpy.ndarray

    :example:
        >>> import pandas as pd
        >>> from capitu.dap.balance.get_one_pf import get_one_pf
        >>> from capitu.dap.power_factor.less_than_ideal import (
        ...     less_than_ideal)
        >>> start_date = '2015-03-01'
        >>> end_date = '2015-05-31'
        >>> meteo_file = '../capitu/datastore/meteo.csv'
        >>> consumption_file = '../capitu/datastore/consumption.csv'
        >>> inverters_file = '../capitu/datastore/inverters_v1.2.0.csv'
        >>> balance, one_pf_balance = get_one_pf(
        ...    start_date, end_date, consumption_file, inverters_file,
        ...    meteo_file, extra_kvar_c=300, extra_kvar_i=0, api=None,
        ...    station=None, campus="G", inverters_date="2023-01-01",
        ...    void_sn="drop", compute_temperature=False, uplimit="nominal",
        ...    coherent=False, consumption_agg="hour", fee_date="2024-03-01",
        ...    data_tz="Etc/UTC", target_tz="America/Sao_Paulo",
        ...    localize_tz=False, show=False, image_export_path=None)
        >>> pf_violations = less_than_ideal(one_pf_balance, ideal_pf=0.92)
        >>> hours = one_pf_balance.index.hour.drop_duplicates()
        >>> lti_pf = pd.DataFrame(
        ...     {"pf_violations": pf_violations, "hour": hours})
        >>> lti_pf
           pf_violations  hour
        0          0.00%     3
        1          0.00%     4
        2          0.00%     5
        3          0.00%     6
        4          1.18%     7
        5          0.00%     8
        6          0.00%     9
        7          1.18%    10
        8          0.00%    11
        9          1.18%    12
        10        22.35%    13
        11        37.65%    14
        12        54.12%    15
        13        63.53%    16
        14        67.06%    17
        15        60.00%    18
        16        60.00%    19
        17        57.65%    20
        18        61.18%    21
        19        69.41%    22
        20        37.65%    23
        21         2.35%     0
        22         0.00%     1
        23         0.00%     2
    """

    # Get the registers of power factor smaller than the regulatory value.
    lti = balance[np.abs(balance.pf) < ideal_pf]

    # Get the number of power factor registers by hour.
    hourly_pf = balance.pf.groupby(balance.index.hour).describe()[["count"]]

    # Add the number of registers of less than ideal power factor by hour.
    hourly_pf["lti"] = lti.pf.groupby(lti.index.hour).count()

    # Compute the percentual of less than ideal power factor registers.
    hourly_pf["lti_pc"] = hourly_pf.lti / hourly_pf["count"]

    # Format the output.
    hourly_pf.fillna(0, inplace=True)
    hourly_pf["lti_pc"] = hourly_pf.lti_pc.map("{:.2%}".format)

    return hourly_pf.lti_pc.values
