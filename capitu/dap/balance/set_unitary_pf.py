#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Compute the active, reactive and apparent powers at a specific bus in the
electrical system, assuming a unitary power factor (PF) for the distributed
energy resource (DER) operation.
"""

import numpy as np
from ..financial.energy_cost import bill as energy_bill
from ..financial.reactive_cost import bill as reactive_energy_bill
from ..power_factor.compute import compute as compute_pf


def set_unitary_pf(balance, fee_date=None):
    """
    This function computes the active, reactive and apparent powers at a
    specific bus in the electrical system, assuming a unitary power factor
    (PF) for the distributed energy resource (DER) operation.

    :param balance: Time-indexed data frame with columns for the load active
        and reactive consumption, for the generated energy and for the time
        band.
    :type balance: pandas.core.frame.DataFrame
    :param fee_date: Date of the billing period. If None, the current date is
        used.
    :type fee_date: str or None, default: None
    :return: Dataframe containing the computed active and reactive powers,
        power factors, and estimated economy provided by full generation.
    :rtype: pandas.core.frame.DataFrame

    :example:

        >>> import os
        >>> import pandas as pd
        >>> from capitu.dap.getter.balance import get as get_balance
        >>> from capitu.dap.balance.set_unitary_pf import set_unitary_pf
        >>> start_date = '2015-03-01'
        >>> end_date = '2015-05-31'
        >>> meteo_file = '../capitu/datastore/meteo.csv'
        >>> consumption_file = '../capitu/datastore/consumption.csv'
        >>> inverters_file = '../capitu/datastore/inverters_v1.2.0.csv'
        >>> balance = get_balance(
        ...     start_date, end_date, consumption_file, inverters_file,
        ...     meteo_file, extra_kvar_c=0, extra_kvar_i=0, campus="G",
        ...     inverters_date="2023-01-01", void_sn="drop",
        ...     consumption_agg="hour", data_tz="Etc/UTC",
        ...     target_tz="America/Sao_Paulo", localize_tz=False, show=False,
        ...     image_export_path=None)
        >>> one_pf_balance = set_unitary_pf(balance, fee_date='2024-03-01')
        >>> one_pf_balance.p_ext_grid_kw.mean()
        -705.0979633218902
        >>> one_pf_balance.q_ext_grid_kvar.mean()
        -707.8916584887145
        >>> one_pf_balance.s_ext_grid_kva.mean()
        1335.7330964319901
        >>> one_pf_balance.raw_economy.sum()
        679711.7616579183
        >>> one_pf_balance.extra_reactive_bill.sum()
        61396.33690637004
    """

    # Create a data frame using the balance index and time_band.
    bus_balance = balance[["time_band"]].copy()

    # Compute the load active, reactive and apparent powers.
    bus_balance["p_load_kw"] = balance.load_kwh / balance.int_time
    bus_balance["q_load_kvar"] = balance.load_kvarh / balance.int_time
    bus_balance["s_load_kva"] = np.sqrt(
        bus_balance.p_load_kw ** 2 + bus_balance.q_load_kvar ** 2)

    # Compute the DER active, reactive and apparent powers.
    bus_balance["p_der_kw"] = -balance.gen_energy_kwh / balance.int_time
    bus_balance["q_der_kvar"] = 0.0
    bus_balance["s_der_kva"] = balance.gen_energy_kwh / balance.int_time

    # Compute demanded active and reactive powers with the unitary pf DER
    # operation.
    bus_balance["p_ext_grid_kw"] = -(
        bus_balance.p_der_kw + bus_balance.p_load_kw)
    bus_balance["q_ext_grid_kvar"] = -(
        bus_balance.q_der_kvar + bus_balance.q_load_kvar)
    bus_balance["s_ext_grid_kva"] = np.sqrt(
        bus_balance.p_ext_grid_kw ** 2 + bus_balance.q_ext_grid_kvar ** 2)

    # Compute the power factor in the full generation scenario.
    bus_balance["pf"] = compute_pf(bus_balance, active="p_ext_grid_kw",
                                   reactive="q_ext_grid_kvar", signal="-")

    # Compute the estimated raw economy provided by a full generation.
    # I'm calling raw economy the economy provided only by the active
    # energy generation. Raw economy doesn't include the reactive energy cost.
    bus_balance["raw_economy"] = energy_bill(
        bus_balance, fee_date, active="p_der_kw", signal="-")

    # Compute the old bill and the bill in the full generation scenario.
    bus_balance["energy_bill"] = energy_bill(
        bus_balance, fee_date, active="p_ext_grid_kw", signal="-")
    old_energy_bill = energy_bill(
        balance, fee_date, active="load_kwh", signal="+")

    bus_balance["extra_reactive_bill"] = reactive_energy_bill(
        bus_balance, fee_date, active="p_ext_grid_kw",
        reactive="q_ext_grid_kvar", signal="-")
    old_extra_reactive_bill = reactive_energy_bill(
        balance, fee_date, active="load_kwh",
        reactive="load_kvarh", signal="+")

    # Compute the estimated economy provided by a full generation.
    bus_balance["economy"] = (
        old_energy_bill + old_extra_reactive_bill - bus_balance.energy_bill
        - bus_balance.extra_reactive_bill)

    return bus_balance
