#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Calculate the balance between electricity generation and consumption using
provided data on consumption and generation of electricity.
"""
name = "balance"
__author__ = "Daniel Campos Pompermayer [gitlab.com/daniel_pompermayer]"
