#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Compute the bill due to the energy consumption."""

from .get_fees import get_fees


def bill(consumption, fee_date=None, active="kwh", signal="+",
         fee_tz="America/Sao_Paulo"):
    """
    Compute the bill due to the energy consumption.

    :param consumption: Time-indexed DataFrame containing a column for each
        register time band named "time_band" and a column for the energy
        consumption.
    :type consumption: pandas.core.frame.DataFrame
    :param fee_date: Date of the applied energy fee in the format "%Y-%m-%d".
    :type fee_date: str, default: None
    :param active: Name of the column of the consumption data registering
        the consumed energy.
    :type active: str, default: "kwh"
    :param signal: If "-", positive powers are understood as flowing from the
        load to the source. Otherwise, positive powers are understood
        as flowing from the source to the load.
    :type signal: str, default "+"
    :param fee_tz: Time zone for the fee date.
    :type fee_tz: str, default: "America/Sao_Paulo"
    :return: pandas Series with the computed energy bill.
    :rtype: pandas.core.series.Series

    :example:

        >>> import pandas as pd
        >>> from capitu.dap.financial.energy_cost import bill
        >>> consumption = pd.DataFrame({
        ...     'time_band': ['peak', 'off_peak', 'peak'],
        ...     'load_kwh': [100, 150, 120]},
        ...     index=pd.date_range(start='2022-01-01', periods=3, freq='D'))
        >>> energy_cost = bill(
        ...     consumption, fee_date="2024-01-01", active="load_kwh",
        ...     signal="+", fee_tz="America/Sao_Paulo")
        >>> energy_cost
        2022-01-01    172.5860
        2022-01-02     61.7370
        2022-01-03    207.1032
        Freq: D, Name: bill, dtype: float64
    """

    # Make a copy so we do not bother the external variables.
    consumption = consumption.copy()

    # Check whether we must change the signals of the powers.
    if signal == "-":
        consumption[[active]] *= -1

    # Get the fee information.
    consumption[["fee", "erex"]] = get_fees(
        consumption.loc[:, ["time_band"]], fee_date, fee_tz)

    # Compute the energy bill.
    consumption["bill"] = consumption[active] * consumption.fee

    return consumption["bill"]
