#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" Retrieve the Brazilian market fees applicable for a given date. """

import pandas as pd
from ..premises.market import brazilian_fees


def get_fees(consumption, fee_date=None, fee_tz="America/Sao_Paulo"):
    """
    Retrieve the Brazilian market fees applicable for a given date.

    This function retrieves the Brazilian market fees that were valid on a
    specified date and associates them with the corresponding consumption data.

    :param consumption: DataFrame containing consumption data.
    :type consumption: pandas.core.frame.DataFrame
    :param fee_date: Date for which the market fees are to be retrieved. If
        None, the index of the consumption DataFrame is used.
    :type fee_date: str or None, default: None
    :param fee_tz: Time zone for the fee date. Default is "America/Sao_Paulo".
    :type fee_tz: str, default: "America/Sao_Paulo"
    :return: DataFrame containing the energy fee and excess reactive values
        associated with the each consumption register.
    :rtype: pandas.core.frame.DataFrame

    :example:

        >>> import pandas as pd
        >>> from capitu.dap.financial.get_fees import get_fees
        >>> consumption = pd.DataFrame({
        ...     "date": ["2024-01-01", "2024-01-02", "2024-01-03"],
        ...     "time_band": ["peak", "off_peak", "peak"],
        ...     "load_kwh": [100, 150, 120]})
        >>> consumption["date"] = pd.to_datetime(consumption["date"])
        >>> consumption.set_index("date", inplace=True)
        >>> fees = get_fees(consumption, fee_date="2024-01-01",
        ...                 fee_tz="America/Sao_Paulo")
        >>> fees # doctest: +NORMALIZE_WHITESPACE
                    energy_fee    erex
        date
        2024-01-01     1.72586  0.3056
        2024-01-02     0.41158  0.3056
        2024-01-03     1.72586  0.3056
    """

    # Do not bother external data.
    consumption = consumption.copy()

    # If no fee date is given, we're using the index as fee date.
    if fee_date is None:
        consumption["fee_date"] = consumption.index
    else:
        consumption["fee_date"] = pd.to_datetime(fee_date)
        consumption["fee_date"] = consumption.fee_date.dt.tz_localize(fee_tz)

    # Merge consumption data frame with the fee data frame for getting the fee
    # values.
    consumption = pd.merge_asof(consumption, brazilian_fees,
                                left_on="fee_date", right_index=True,
                                direction="backward")

    # Select the fee based on the time band.
    consumption["energy_fee"] = ((consumption.time_band == "peak")
                                 * (consumption.peak)
                                 + (consumption.time_band == "off_peak")
                                 * (consumption.off_peak))

    return consumption[["energy_fee", "erex"]]
