#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Compute the bill due to extra reactive power."""

import numpy as np
from .get_fees import get_fees
from ..premises.market import night as night_hours
from ..premises.market import day as day_hours
from ..premises.market import brazilian_ideal_pf


def erex(active, pf, erex_fee):
    """
    Compute the excess reactive charges as stated by Section VIII of the
    `Regulatory Resolution No. 1,000, dated December 7, 2021, from the
    Brazilian National Electric Energy Agency (ANEEL).
    <https://www2.aneel.gov.br/cedoc/ren20211000.pdf>`_

    :param active: Active power consumption.
    :type active: float or array-like
    :param pf: Power factor.
    :type pf: float or array-like
    :param erex_fee: Excess reactive charge rate.
    :type erex_fee: float or array-like
    :return: Excess reactive charges.
    :rtype: float or array-like

    :example:

        >>> from capitu.dap.financial.reactive_cost import erex
        >>> erex(100, 0.92, 0.30)
        0
        >>> erex(98, 0.95, 0.30)
        0
        >>> erex([100, 98, 96], [0.92, 0.95, 0.90], [0.30, 0.30, 0.30])
        array([0.  , 0.  , 0.64])
    """
    active = np.asarray(active)
    pf = np.asarray(pf)
    erex_fee = np.asarray(erex_fee)
    ere = (active * (brazilian_ideal_pf/pf - 1) * erex_fee)
    try:
        ere[ere < 0] = 0
    except TypeError:
        ere = max(0, ere)
    return ere


def bill(consumption, fee_date=None, active="kwh", reactive="kvarh",
         capacitive=False, pf="pf", signal="+", target_tz="America/Sao_Paulo",
         fee_tz="America/Sao_Paulo", hour="index"):

    """
    Compute the bill due to extra reactive power.

    :param consumption: Time-indexed DataFrame containing a column for each
        register time band named "time_band" and a column for the energy
        consumption.
    :type consumption: pandas.core.frame.DataFrame
    :param fee_date: Date of the applied energy fee in the format "%Y-%m-%d".
    :type fee_date: str, default: None
    :param active: Name of the column of the consumption data registering
        the consumed energy.
    :type active: str, default: "kwh"
    :param reactive: Name of the column of the temporal integral of the
        reactive power column.
    :type reactive: str, default: "kvarh"
    :param capacitive: string with the name of an optional column having the
        capactive reactive power registered separately. If that column is not
        present, set capacitive as None.
    :type capacitive: str or None, default None
    :param signal: If "-", positive powers are understood as flowing from the
        load to the source. Otherwise, positive powers are understood
        as flowing from the source to the load.
    :type signal: str, default "+"
    :param target_tz: Time zone for the results.
    :type target_tz: str, default "America/Sao_Paulo".
    :param fee_tz: Time zone for the fee date.
    :type fee_tz: str, default: "America/Sao_Paulo"
    :param hour: name of the column having the hours information.
    :type hour: str, default "index".
    :return: pandas Series with the computed extra reactive energy bill.
    :rtype: pandas.core.series.Series

    :example:

        >>> import pandas as pd
        >>> from capitu.dap.financial.reactive_cost import bill
        >>> consumption = pd.DataFrame({
        ...     'time_band': ['peak', 'off_peak', 'peak'],
        ...     'load_kwh': [100, 150, 120],
        ...     'load_kvarh_i': [50, 100, 75],
        ...     'load_kvarh_c': [10, 25, 20],
        ...     'pf': [0.95, 0.92, 0.90]},
        ...     index=pd.date_range(start='2022-01-01', periods=3, freq='D'))
        >>> reactive_cost = bill(
        ...     consumption, fee_date='2024-01-01', active='load_kwh',
        ...     reactive='load_kvarh_i', capacitive='load_kvarh_c', pf='pf',
        ...     signal='+', target_tz='America/Sao_Paulo',
        ...     fee_tz='America/Sao_Paulo', hour='index')
        >>> reactive_cost
        2022-01-01    0.0
        2022-01-02    0.0
        2022-01-03    0.0
        Freq: D, Name: bill, dtype: float64
    """

    # Make a copy so we do not bother the external variables.
    consumption = consumption.copy()

    # Check whether "reactive energy" must be calculated.
    if isinstance(capacitive, str):
        kvarh = consumption[reactive] - consumption[capacitive]
    else:
        kvarh = consumption[reactive]

    # Check whether we must change the signals of the powers.
    if signal == "-":
        consumption[[active]] *= -1
        kvarh *= -1

    # Get the fee information.
    consumption[["fee", "erex"]] = get_fees(
        consumption.loc[:, ["time_band"]], fee_date, fee_tz)

    # Check whether hour is to be got from the index of from some column.
    if hour == "index":
        hour = consumption.index.hour
    else:
        hour = consumption[hour]

    # Compute the erex cost.
    erex_cost = erex(consumption[active], consumption[pf], consumption.erex)

    # Get capacitive and inductive behaviors.
    capacitive = kvarh < 0
    inductive = kvarh >= 0

    # Get generation behavior.
    generating = consumption[active] <= 0
    consuming = consumption[active] > 0

    # There are no charges when generating.
    consumption.loc[generating, "bill"] = 0

    # Compute power factor charges at night.
    night = hour.isin(night_hours)
    consumption.loc[night & inductive, "bill"] = 0
    consumption.loc[consuming & night & capacitive, "bill"] = erex_cost[
        consuming & night & capacitive]

    # Compute power facotr charges at day.
    day = hour.isin(day_hours)
    consumption.loc[day & capacitive, "bill"] = 0
    consumption.loc[consuming & day & inductive,  "bill"] = erex_cost[
        consuming & day & inductive]

    return consumption["bill"]
