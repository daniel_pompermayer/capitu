#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
The financial module provides the functions that compute the energy bill and
the extra reactive power bill.
"""
name = "financial"
__author__ = "Daniel Campos Pompermayer [gitlab.com/daniel_pompermayer]"
