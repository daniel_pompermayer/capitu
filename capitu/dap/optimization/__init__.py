#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
The optimization module offers functions for solving optimization problems with
the data.
"""
name = "optimization"
__author__ = "Daniel Campos Pompermayer [gitlab.com/daniel_pompermayer]"
