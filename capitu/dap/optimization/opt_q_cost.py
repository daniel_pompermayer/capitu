import logging
from pathlib import Path
from ..plotter.boxplot import boxplot


def opt_q_cost(min_q_cost_gen_balance, one_pf_balance, path=None,
               filename="opt_q_cost", show=False, image_path=None, ax_width=17,
               ax_height=12, fontsize=10):

    logging.info("Starting process.")

    # Get the best of all the worlds.
    opt_gen_balance = one_pf_balance.copy()
    opt = opt_gen_balance.economy < min_q_cost_gen_balance.economy
    opt_gen_balance.loc[opt, min_q_cost_gen_balance.columns] = (
        min_q_cost_gen_balance.loc[opt])

    # Check whether user wants to save the optimum results.
    if path is not None:

        # Convert string of the save directory to path.
        save_path = Path(path) / (filename + ".csv")

        # Save it.
        min_q_cost_gen_balance.to_csv(save_path, sep=";", decimal=".")

    logging.info("Done.")

    # Plot meteorological data.
    if show or (image_path is not None):
        opt_gen_balance = opt_gen_balance.rename({"pf": "opt_pf"}, axis=1)
        boxplot(opt_gen_balance, path=image_path,
                show=show, x="hour", y="opt_pf", ylim=(-0.1, 1.1),
                absolute=True, whis=1.5, ax_height=ax_height,
                ax_width=ax_width, fontsize=fontsize)
        opt_gen_balance = opt_gen_balance.rename({"opt_pf": "pf"}, axis=1)

    return opt_gen_balance
