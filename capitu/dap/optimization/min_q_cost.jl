using JuMP, Ipopt

function min_q_cost(p_load::Float64, q_load::Float64, s_der_max::Float64,
                    p_der_max::Float64, int_time::Float64=1.0,
                    der_min_pf::Float64=0.8, ref_power_factor::Float64=0.92,
                    active_energy_fee::Float64=0.28,
                    reactive_energy_fee::Float64=0.3)

    # Create an empty model.
    model = Model(Ipopt.Optimizer)
    set_silent(model)    

    # Create the variables of the external grid.
    @variable(model, p_ext_grid)
    @variable(model, q_ext_grid <= 0)
    @variable(model, s_ext_grid >= 0)

    # Create the variables of the der.
    @variable(model, -p_der_max <= p_der <= 0)
    @variable(model, q_der <= 0)
    @variable(model, 0 <= s_der <= s_der_max)

    # Create the cost variables.
    @variable(model, energy_bill)
    @variable(model, extra_reactive_bill >= 0)

    # Create the constraints of the external grid.
    @NLconstraint(model, s_ext_grid^2 == p_ext_grid^2 + q_ext_grid^2)

    # Create the constraints of the der.
    @NLconstraint(model, s_der^2 == p_der^2 + q_der^2)
    @NLconstraint(model, abs(p_der) >= der_min_pf * s_der)

    # Create the constraints of the active power cost.
    @constraint(
        model, energy_bill == -active_energy_fee * int_time * p_ext_grid)

    # Create the constraints of the extra reactive power
    @constraint(model, extra_reactive_bill == reactive_energy_fee * int_time
                * (ref_power_factor * s_ext_grid + p_ext_grid))

    # Create the power balance constraint.
    @constraint(model, p_load + p_ext_grid + p_der == 0)
    @constraint(model, q_load + q_ext_grid + q_der == 0)

    @objective(model, Min, extra_reactive_bill + energy_bill)

    optimize!(model)

    return (value(p_der), value(q_der), value(s_der), value(p_ext_grid),
            value(q_ext_grid), value(s_ext_grid), value(energy_bill),
            value(extra_reactive_bill))
end

