import logging
from pathlib import Path
from ..financial.energy_cost import bill as energy_bill
from ..financial.get_fees import get_fees
from ..financial.reactive_cost import bill as reactive_energy_bill
from ..plotter.boxplot import boxplot
from ..power_factor.compute import compute as compute_pf
from ..premises.market import brazilian_ideal_pf


def min_q_cost(balance, runtime, max_capacity, fee_date=None,
               fee_tz="America/Sao_Paulo", min_inv_pf=0.8,
               jl_code=Path(__file__).parent / "min_q_cost.jl", path=None,
               filename="min_q_cost", show=False, image_path=None, ax_width=17,
               ax_height=12, fontsize=10):
    """ Compute the generated powers and the demanded powers trying to
    minimize the extra rective power cost.
    """

    # Declare a instance of julia language.
    logging.info("Starting process.")

    # Do not bother external data.
    balance = balance.copy()

    # Importing julia.
    from julia.api import Julia
    _ = Julia(compiled_modules=False, runtime=runtime)
    from julia import Main

    # Read the reactive cost optimization function.
    jl_min_q_cost = Main.include(str(jl_code))

    logging.info("Julia function included.")

    # Get the fee information.
    balance[["fee", "erex"]] = get_fees(balance, fee_date, fee_tz)

    logging.info("Starting optimization.")

    # Run the optimization algorithm.
    min_q_cost_list = []
    for i, (_, row) in enumerate(balance.iterrows()):
        logging.info("Computing: %d/%d" % (i+1, len(balance)))
        min_q_cost_list.append(jl_min_q_cost(
            row.load_kwh, row.load_kvarh, max_capacity, row.gen_energy_kwh,
            row.int_time, min_inv_pf, brazilian_ideal_pf, row.fee, row.erex))
    min_q_cost_gen_balance = balance.copy()
    min_q_cost_gen_balance[[
        "p_der_kw", "q_der_kvar", "s_der_kva", "p_ext_grid_kw",
        "q_ext_grid_kvar", "s_ext_grid_kva", "energy_bill",
        "extra_reactive_bill"]] = min_q_cost_list

    logging.info("Got the optimum result.")

    # Compute the power factor in the reactive cost optmization scenario.
    min_q_cost_gen_balance["pf"] = compute_pf(
        min_q_cost_gen_balance, active="p_ext_grid_kw",
        reactive="q_ext_grid_kvar", signal="-")

    # Compute the estimated raw economy provided by the opt algorithm.
    # I'm calling raw economy the economy provided only by the active
    # energy generation. Raw economy doesn't include the reactive energy cost.
    min_q_cost_gen_balance["raw_economy"] = energy_bill(
        min_q_cost_gen_balance, fee_date, active="p_der_kw", signal="-",
        fee_tz=fee_tz)

    # Compute the old bill and the bill in the full generation scenario.
    old_energy_bill = energy_bill(
        balance, fee_date, active="load_kwh", signal="+")
    old_extra_reactive_bill = reactive_energy_bill(
        balance, fee_date, active="load_kwh",
        reactive="load_kvarh", signal="+")

    # Compute the estimated economy provided by a full generation.
    min_q_cost_gen_balance["economy"] = (
        old_energy_bill + old_extra_reactive_bill
        - min_q_cost_gen_balance.energy_bill
        - min_q_cost_gen_balance.extra_reactive_bill)

    # Compute the estimated economy provided by the opt algorithm.
    min_q_cost_gen_balance["economy"] = (
        min_q_cost_gen_balance.raw_economy
        - min_q_cost_gen_balance.extra_reactive_bill)

    # Keep only the useful columns.
    min_q_cost_gen_balance = min_q_cost_gen_balance[
        ["p_der_kw", "q_der_kvar", "s_der_kva", "p_ext_grid_kw",
         "q_ext_grid_kvar", "s_ext_grid_kva", "pf", "raw_economy",
         "energy_bill", "extra_reactive_bill", "economy"]]

    # Check whether user wants to save the optimum results.
    if path is not None:

        # Convert string of the save directory to path.
        save_path = Path(path) / (filename + ".csv")

        # Save it.
        min_q_cost_gen_balance.to_csv(save_path, sep=";", decimal=".")

    logging.info("Done.")

    # Plot power factor.
    if show or (image_path is not None):
        min_q_cost_gen_balance = min_q_cost_gen_balance.rename(
            {"pf": "min_q_cost_pf"}, axis=1)
        boxplot(min_q_cost_gen_balance, path=image_path,
                show=show, x="hour", y="min_q_cost_pf", ylim=(-0.1, 1.1),
                absolute=True, whis=1.5, ax_height=ax_height,
                ax_width=ax_width, fontsize=fontsize)
        min_q_cost_gen_balance = min_q_cost_gen_balance.rename(
            {"min_q_cost_pf": "pf"}, axis=1)

    return min_q_cost_gen_balance
