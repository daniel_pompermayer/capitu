#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
The estimator module provides functions that estimate many of the models'
states.
"""
name = "estimator"
__author__ = "Daniel Campos Pompermayer [gitlab.com/daniel_pompermayer]"
