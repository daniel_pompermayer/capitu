#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
A central collection of color dictionaries used for various data
representations in plots.

    :variables:
        boxplot (dict): A dictionary containing colors for box plots.

        lineplot (list): A list containing colors for line plots.

        histplot (list): A list containing colors for histogram plots.

        pdfplot (list): A list containing colors for probability density
        function (PDF) plots.

        ecdfplot (list): A list containing colors for empirical cumulative
        distribution function (ECDF) plots.

    :example:
        >>> from capitu.dap.plotter.colors import (
        ...     boxplot, lineplot, histplot, pdfplot, ecdfplot)
        >>> print(boxplot['load_kwh'])
        bordeaux
        >>> print(lineplot[0])
        [0.85, 0.4, 0.0]
        >>> print(histplot[1])
        [0.851, 0.851, 0.851]
        >>> print(pdfplot[2])
        [0.6, 0.0, 0.0]
        >>> print(ecdfplot[1])
        [0.851, 0.851, 0.851]
"""

boxplot = {"e_kj_per_m2": "goldenrod", "load_kwh": "bordeaux",
           "load_kvarh": "neon purple", "gen_energy_kwh": "greenish",
           "old_pf": "light red", "new_pf": "sea blue",
           "min_q_cost_pf": "greenish", "opt_pf": "goldenrod",
           "temperature_celsius": "orange",
           "season": [[0.85, 0.40, 0.00], [0.03, 0.30, 0.30],
                      [0.15, 0.65, 0.55], [0.60, 0.00, 0.00],
                      [0.80, 0.60, 1.00]],
           "cluster": [[0.851, 0.851, 0.851], [0.349, 0.459, 0.643],
                       [0.15, 0.65, 0.55], [0.60, 0.00, 0.00],
                       [0.80, 0.60, 1.00]],
           "time_band": [[0.851, 0.851, 0.851],
                         [0.60, 0.00, 0.00]],
           "type_of_day": [[0.851, 0.851, 0.851],
                           [0.349, 0.459, 0.643],
                           [0.80, 0.60, 1.00]]}

lineplot = [[0.85, 0.40, 0.00],
            [0.03, 0.30, 0.30],
            [0.15, 0.65, 0.55],
            [0.60, 0.00, 0.00],
            [0.80, 0.60, 1.00]]

histplot = [[0.349, 0.459, 0.643],
            [0.851, 0.851, 0.851]]

pdfplot = [[0.349, 0.459, 0.643],
           [0.851, 0.851, 0.851],
           [0.60, 0.00, 0.00]]

ecdfplot = [[0.349, 0.459, 0.643],
            [0.851, 0.851, 0.851],
            [0.60, 0.00, 0.00]]
