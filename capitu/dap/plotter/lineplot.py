#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Plot and save lineplots.
"""

import seaborn as sns
from . import prepare_plot, close_plot


def lineplot(data, path=None, show=False, x="hour", y="e_kj_per_m2",
             hue="season", scale_factor=1, ylim=None, xlim=None, prefix="",
             ax_height=12, ax_width=16, fontsize=8, ylabel="auto",
             format="pgf", seed=42):
    """
    Plot and save linexplots.

    :param data: time indexed data frame having the data we're plotting.
    :type data: pandas.core.frame.DataFrame
    :param path: string having the address to the directory of where the plot
        will be saved. If None, the plot will not be saved.
    :type path: str or None, default: None
    :param show: if True the plot will be presented in the screen.
    :type show: bool, default: False
    :param x: Name of the column having the x-axis values.
    :type x: str, default: "hour"
    :param y: string having the name of the column having the data to be
              plotted.
    :type y: str, default: "e_kj_per_m2"
    :param hue: name of the column having the information that will split the
                data in colors.
    :type hue: str, default: "season"
    :param scale_factor: Scale factor to whom the data will be divided.
    :type scale_factor: int, default: 1
    :param ylim: seaborn ylim parameter.
    :type ylim: float or None, default: None
    :param xlim: seaborn xlim parameter.
    :type xlim: float or None, default: None
    :param prefix: String to be prepended to the filename of the saved plot.
    :type prefix: str, default: ""
    :param ax_height: The height of each subplot.
    :type ax_height: int or float, default: 12
    :param ax_width: The width of each subplot.
    :type ax_width: int or float, default: 16
    :param fontsize: The font size of the axis labels and title.
    :type fontsize: int, default: 8
    :param ylabel: The label text to attribute to the y-axis. If "auto",
        will use the column name.
    :type ylabel: str or "auto", default: "auto"
    :param format: Specifies the file format for saving the plot.
    :type format: str, default: "pgf"
    :param seed: Seed or random number generator for reproducible
        bootstrapping.
    :type seed: int, default: 42

    :examples:
        >>> import os
        >>> import numpy as np
        >>> import pandas as pd
        >>> from PIL import Image, ImageChops
        >>> from capitu import dap
        >>> from scipy.stats import norm, triang, beta
        >>> from capitu.dap.plotter.lineplot import lineplot
        >>> dap.set_language("english")
        >>> nine_am = pd.date_range(
        ...     start='2023-01-01 09:00:00', end='2023-12-31 09:00:00',
        ...     freq='D')
        >>> data_size = len(nine_am)
        >>> np.random.seed(42)
        >>> temp_9am = norm.rvs(loc=26, scale=3, size=data_size)
        >>> eleven_am = pd.date_range(
        ...     start='2023-01-01 11:00:00', end='2023-12-31 11:00:00',
        ...     freq='D')
        >>> np.random.seed(42)
        >>> temp_11am = norm.rvs(loc=28, scale=3, size=data_size)
        >>> one_pm = pd.date_range(
        ...     start='2023-01-01 13:00:00', end='2023-12-31 13:00:00',
        ...     freq='D')
        >>> np.random.seed(42)
        >>> temp_1pm = triang.rvs(c=0.6, loc=19, scale=17, size=data_size)
        >>> three_pm = pd.date_range(
        ...     start='2023-01-01 15:00:00', end='2023-12-31 15:00:00',
        ...     freq='D')
        >>> np.random.seed(42)
        >>> temp_3pm = beta.rvs(a=5, b=3, loc=16, scale=19, size=data_size)
        >>> data = pd.DataFrame({
        ...     "temperature_celsius": np.concatenate([
        ...         temp_9am, temp_11am, temp_1pm, temp_3pm])},
        ...         index=np.concatenate([nine_am, eleven_am, one_pm,
        ...                               three_pm]))
        >>> data["hour"] = data.index.hour
        >>> lineplot(data, path="./", show=False, x="hour",
        ...          y="temperature_celsius", hue=None, format="png")
        >>> target = Image.open('./line_temperature_celsius.png')
        >>> template = Image.open('../capitu/datastore/doctest/lineplot.png')
        >>> diff = ImageChops.difference(target, template)
        >>> diff.getbbox() is None
        True
        >>> os.remove('./line_temperature_celsius.png')
    """

    # Prepare the plot setting a nice theme, scaling the data.
    data, _, colors = prepare_plot(data, y, None, scale_factor, False, hue)

    # Lineplotting.
    if hue is None:
        ax = sns.lineplot(data=data, x=x, y=y, seed=seed)
    else:
        ax = sns.lineplot(data=data, x=x, y=y, hue=hue, style=hue,
                          palette=colors, seed=seed)

    # Closing.
    close_plot(x, y, ylim, xlim, None, path, show, format,
               filename=prefix+"line_"+y, ax=ax, fontsize=fontsize,
               ylabel=ylabel, ax_height=ax_height, ax_width=ax_width)
