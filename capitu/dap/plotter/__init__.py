#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
The Plotter module provides functions for making easy to plot the data used in
the application.
"""

import re
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from pathlib import Path
from mpl_toolkits.axes_grid1 import Divider, Size
from .labels import x_label, y_label, legend_labels
from .colors import boxplot as bp_colors


name = "plotter"
__author__ = "Daniel Campos Pompermayer [gitlab.com/daniel_pompermayer]"


def prepare_plot(data, y, x=None, scale_factor=None, absolute=False, hue=None):
    """
    Function to be called before plotting, for setting a nice theme, scaling
    the data and etc.

    :param data: Time-indexed data frame containing the data to be plotted.
    :type data: pandas.core.frame.DataFrame
    :param y: The name of the column containing the data to be plotted.
    :type y: str
    :param x: Determines how the data will be split. Options: None (no split),
        "hour", "month", "month/year".
    :type x: str, default: None
    :raise ValueError: If x is not None, "hour", "month", nor "month/year".
    :param scale_factor: Scale factor by which the data will be divided.
    :type scale_factor: int, default: None
    :param absolute: If True the absolute value of data will be plotted.
    :type absolute: bool, default: False
    :param hue: The name of the column containing the information to split the
        data into colors.
    :type hue: str, default: "season"
    :return: A tuple containing the processed data, the x values, and the
        colors.
    :rtype: tuple(pandas.core.frame.DataFrame, array_like, list)

    :example:
        >>> import pandas as pd
        >>> import numpy as np
        >>> from capitu.dap.plotter import prepare_plot

        Let's create a dataframe with temperature data to perform the test:

        >>> np.random.seed(42)
        >>> data = pd.DataFrame({
        ...    'temperature_celsius': np.random.uniform(-10, 30, 100),
        ...    'season': np.random.choice(
        ...        ['Spring', 'Summer', 'Autumn', 'Winter'], 100)},
        ...    index=pd.date_range(start='2022-01-01', periods=100, freq='D'))

       Let's test dividing the data by month and without grouping into
       different colors:

        >>> processed_data, x, colors = prepare_plot(
        ...     data, y='temperature_celsius', x='month', scale_factor=None,
        ...     absolute=False, hue=None)
        >>> processed_data.temperature_celsius.mean()
        8.807229735128372
        >>> colors
        [(0.9764705882352941, 0.45098039215686275, 0.023529411764705882)]
        >>> unique_values, counts = np.unique(x, return_counts=True)
        >>> print(dict(zip(unique_values, counts)))
        {1: 31, 2: 28, 3: 31, 4: 10}

        What if we use the scale factor?

        >>> processed_data, x, colors = prepare_plot(
        ...     data, y='temperature_celsius', x='month', scale_factor=10,
        ...     absolute=False, hue=None)
        >>> processed_data.temperature_celsius.mean()
        0.8807229735128373

        What if we use the absolute value option too?

        >>> processed_data, x, colors = prepare_plot(
        ...     data, y='temperature_celsius', x='month', scale_factor=10,
        ...     absolute=True, hue=None)
        >>> processed_data.temperature_celsius.mean()
        1.1979878427455426

       Let's test dividing the data by month/year and grouping by season:

        >>> processed_data, x, colors = prepare_plot(
        ...     data, y='temperature_celsius', x='month/year',
        ...     scale_factor=None, absolute=False, hue='season')
        >>> processed_data.temperature_celsius.mean()
        8.807229735128372
        >>> len(colors)
        4
        >>> colors[2]
        [0.15, 0.65, 0.55]
        >>> unique_values, counts = np.unique(x, return_counts=True)
        >>> print(dict(zip(unique_values, counts)))
        {'01/2022': 31, '02/2022': 28, '03/2022': 31, '04/2022': 10}
    """

    # Set a seaborn nice theme.
    sns.set_theme(style="whitegrid")

    # Do not bother external data.
    data = data.copy()

    # Scale the data to a better plot.
    if scale_factor is not None:
        data[y] = data[y]/scale_factor

    # Check whether user wants an absolute value of the data.
    if absolute:
        data[y] = np.abs(data[y])

    # Get the color of the boxes.
    if hue is not None:

        # Get the number of colors. If data is categorical, I get the number of
        # categories. If not, I get the number of unrepeated values.
        if data[hue].dtype.name == "category":
            number_of_colors = len(data[hue].cat.categories)
        else:
            number_of_colors = len(data[hue].drop_duplicates())

        # Get the colors.
        colors = bp_colors[hue][0:number_of_colors]

    else:
        colors = sns.xkcd_palette([bp_colors[y]])

    # Check how user wants to split data.
    if x is not None:
        if x == "hour":
            x = data.index.hour
        elif x == "month":
            x = data.index.month
        elif x == "month/year":
            x = data.index.strftime("%m/%Y").values
        elif x == "time_band":
            x = "time_band"
        elif x == "type_of_day":
            x = "type_of_day"
        else:
            raise ValueError("I don't know how to split the data this way.")

    return data, x, colors


def close_plot(x=None, y=None, ylim=None, xlim=None, yticks=None, path=None,
               show=False, format="pgf", filename="my_plot", fontsize=8,
               fig=None, ax=None, ylabel="auto", ax_height=12, ax_width=16,
               tight_layout=True):
    """
    Function to be called after plotting, for adjusting and saving the plot.

    :param x: x-axis identification.
    :type x: str, default: None
    :param y: Name of the column containing the data to be plotted.
    :type y: str, default: None
    :param ylim: matplotlib.pyplot ylim parameter.
    :type ylim: float or None, default: None
    :param xlim: matplotlib.pyplot xlim parameter.
    :type xlim: float or None, default: None
    :param yticks: matplotlib.pyplot yticks parameter.
    :type yticks: list or None, default: None
    :param path: Address to the directory where the plot will be saved. If
        None, the plot will not be saved.
    :type path: str or None, default: None
    :param show: If True, the plot will be presented on the screen.
    :type show: bool, default: False
    :param format: Specifies the file format for saving the plot.
    :type format: str, default: "pgf"
    :param filename: Name of the file to be saved.
    :type filename: str, default: "my_plot"
    :param fontsize: specifies the font size to be used in the plot.
    :type fontsize: int, default: 8
    :param ax: Figure object for the plot.
    :type ax: matplotlib.figure.Figure, default: None
    :param ax: Axes object for the plot.
    :type ax: matplotlib.axes.Axes, default: None
    :param ylabel: Label for the y-axis.
        This parameter specifies the label for the y-axis of the plot.
    :type ylabel: str, default: "auto"
    :param ax_height: specifies the height of the plot in centimeters.
    :type ax_height: float, default: 12
    :param ax_width: specifies the width of the plot in centimeters.
    :type ax_width: float, default: 16
    :param tight_layout: If True, automatically adjusts subplot params.
    :type tight_layout: bool, default: True

    :example:
        >>> import os
        >>> import pandas as pd
        >>> import numpy as np
        >>> import seaborn as sns
        >>> from PIL import Image, ImageChops
        >>> from capitu import dap
        >>> from capitu.dap.plotter import prepare_plot, close_plot

        Let's create a dataframe with temperature data to perform the test:

        >>> np.random.seed(42)
        >>> data = pd.DataFrame({
        ...    'temperature_celsius': np.random.uniform(-10, 30, 100),
        ...    'season': np.random.choice(
        ...        ['Spring', 'Summer', 'Autumn', 'Winter'], 100)},
        ...    index=pd.date_range(start='2022-01-01', periods=100, freq='D'))

        Let's test a boxplot of the data split by month/year and grouped by
        season:

        >>> dap.set_language("english")
        >>> processed_data, x_values, colors = prepare_plot(
        ...     data, y='temperature_celsius', x='month/year',
        ...     scale_factor=None, absolute=False, hue='season')
        >>> ax = sns.boxplot(
        ...     data=processed_data, x=x_values, y='temperature_celsius',
        ...     hue='season', palette=colors)
        >>> close_plot(x='month/year', y='temperature_celsius',
        ...            ylim=(-10, 30), yticks=[-10, 0, 10, 20, 30],
        ...            path='./', show=False, format='png', fontsize=10,
        ...            ylabel='Temperature (°C)', ax_height=15, ax_width=20,
        ...            filename='test', ax=ax)

        If everything has gone well, the generated image is identical to the
        one in our datastore:

        >>> target = Image.open('./test.png')
        >>> template = Image.open('../capitu/datastore/doctest/close_plot.png')
        >>> diff = ImageChops.difference(target, template)
        >>> diff.getbbox() is None
        True

       Let's clean up the test file:

        >>> os.remove('test.png')
    """

    # Adjust legend.
    if ax is not None:
        legend = ax.get_legend()
        if legend is not None:
            handles = legend.legend_handles
            new_labels = []
            for legend_text in legend.texts:
                if legend_text.get_text() in legend_labels.keys():
                    new_labels.append(legend_labels[legend_text.get_text()])
                else:
                    new_labels.append(legend_text.get_text())
            legend.remove()
            ax.legend(handles, new_labels, fontsize=fontsize)

    # Check whether user wants to change the y ticks.
    if yticks is not None:
        _ = plt.yticks(yticks)

    # Set the labels.
    if x is not None:
        _ = plt.xlabel(x_label[x])

    if ylabel == "auto":
        if y is not None:
            _ = plt.ylabel(y_label[y])
    else:
        _ = plt.ylabel(ylabel)

    # Use well formated xticklabels.
    new_xticklabels = []
    locs, xticklabels = plt.xticks()
    for xticklabel in xticklabels:
        xticktext = xticklabel.get_text()
        if xticktext in x_label.keys():
            new_xticklabels.append(x_label[xticktext])
        else:
            new_xticklabels.append(xticktext)
    plt.xticks(locs, new_xticklabels)

    # Check whether user wants to change the y limits.
    if ylim is not None:
        _ = plt.ylim(ylim)

    # Check whether user wants to change the x limits.
    if xlim is not None:
        _ = plt.xlim(xlim)

    # Set size of my beautiful plot.
    if fig is None:
        fig = plt.gcf()

    if tight_layout:
        fig.tight_layout()

    if ax is not None:

        # Get current sizes.
        old_fig_width, old_fig_height = fig.get_size_inches()
        left = ax.figure.subplotpars.left
        right = ax.figure.subplotpars.right
        top = ax.figure.subplotpars.top
        bottom = ax.figure.subplotpars.bottom

        # Compute the desired size.
        cm = 1/2.54
        new_fig_width = ax_width * cm + old_fig_width * (1 - right + left)
        new_fig_height = ax_height * cm + old_fig_height * (1 - top + bottom)

        # Get the new ratio for padding.
        new_left = left * old_fig_width / new_fig_width
        new_right = right * old_fig_width / new_fig_width
        new_top = top * old_fig_height / new_fig_height
        new_bottom = bottom * old_fig_height / new_fig_height

        # Set right and top padding, fixed axes size and left and bottom
        # padding
        horizontal = [Size.Scaled(new_right), Size.Fixed(ax_width * cm),
                      Size.Scaled(new_left)]
        vertical = [Size.Scaled(new_top), Size.Fixed(ax_height * cm),
                    Size.Scaled(new_bottom)]
        divider = Divider(fig, (0.0, 0.0, 1., 1.), horizontal, vertical,
                          aspect=False)
        ax.set_axes_locator(divider.new_locator(nx=1, ny=1))

        fig.set_size_inches(new_fig_width, new_fig_height)

    # Maybe showing.
    if show:
        plt.show()

    # Maybe Saving.
    if path is not None:

        # Convert string of the save directory to path.
        save_path = Path(path) / (filename + "." + format)

        # Save default rcParams.
        default_texsystem = plt.rcParams["pgf.texsystem"]
        default_preamble = plt.rcParams["pgf.preamble"]
        default_fontsize = plt.rcParams["font.size"]
        default_figure_facecolor = plt.rcParams["figure.facecolor"]
        default_savefig_facecolor = plt.rcParams["savefig.facecolor"]

        # Set backend and TeX implementation and import amsmath package.
        plt.rcParams["pgf.texsystem"] = "pdflatex"
        plt.rcParams["pgf.preamble"] = "\n".join([default_preamble,
                                                  r"\usepackage{amsmath}"])
        plt.rcParams["font.size"] = fontsize
        plt.rcParams["figure.facecolor"] = default_figure_facecolor
        plt.rcParams["savefig.facecolor"] = default_savefig_facecolor

        # Save it
        _ = fig.savefig(save_path, format=format)

        # Recover default.
        plt.rcParams["pgf.texsystem"] = default_texsystem
        plt.rcParams["pgf.preamble"] = default_preamble
        plt.rcParams["font.size"] = default_fontsize
        plt.rcParams["figure.facecolor"] = (0.0, 0.0, 0.0, 0.0)
        plt.rcParams["savefig.facecolor"] = (0.0, 0.0, 0.0, 0.0)

        # It seems that plt.rcParams["font.size"] = fontsize is not working
        # for pgf files.
        if format == "pgf":

            # So I'm gonna manually change the font size.
            with open(save_path, "r") as filp:
                content = filp.read()
                content = re.sub(
                    r"\\fontsize\{.*?\}\{.*?\}",
                    r"\\fontsize{%f}{%f}" % (fontsize, fontsize * 1.2),
                    content)

            with open(save_path, "w") as filp:
                filp.write(content)

    plt.close()

    sns.reset_defaults()
