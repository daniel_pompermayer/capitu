#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
A central collection of label dictionaries used for various data
representations in plots.

    :variables:
        y_label (dict): A dictionary containing labels for y-axis data.

        x_label (dict): A dictionary containing labels for x-axis data.

        legend_labels (dict): A dictionary containing legend labels.

        hp_legend (dict): A dictionary containing legends to hist plots.

        table_labels (dict): A dictionary containing labels for tables.

    :example:
        >>> from capitu.dap.plotter.labels import (
        ...     y_label, x_label, legend_labels, hp_legend, table_labels,
        ...     set_language)

        Testing dictionaries in English:

        >>> set_language("english")
        'english'
        >>> print(y_label['load_kwh'])
        Consumed Active Energy [$MWh$]
        >>> print(x_label['month'])
        Month
        >>> print(legend_labels['summer'])
        Summer
        >>> print(hp_legend['temperature_celsius'])
        Temperature
        >>> print(table_labels['type_of_day'])
        Type of Day

        Testing dictionaries in Portuguese:

        >>> set_language("portuguese")
        'portuguese'
        >>> print(y_label['load_kwh'])
        Energia Ativa Consumida
        [$MWh$]
        >>> print(x_label['month'])
        Mês
        >>> print(legend_labels['summer'])
        Verão
        >>> print(hp_legend['temperature_celsius'])
        Temperatura
        >>> print(table_labels['type_of_day'])
        Natureza do Dia
"""

en_y_label = {"e_kj_per_m2": "Solar Radiation [$MJ/m^2$]",
              "load_kwh": "Consumed Active Energy [$MWh$]",
              "load_kvarh": "Consumed Reactive Energy [$Mvarh$]",
              "gen_energy_kwh": "Generated Energy [$MWh$]",
              "temperature_celsius": "Temperature [$^{o}C$]",
              "pf": "Power Factor",
              "old_pf": "Old Power Factor",
              "new_pf": "Absolute Value of the \nNew Power Factor",
              "min_q_cost_pf":
              "Absolute Value of the \nMaximized Power Factor",
              "opt_pf": "Absolute Value of the \nOptimized Power Factor",
              "percentual_extra_reactive_charges":
              "Excess Reactive Charges in \nPercentual of the Raw economy"}

pt_y_label = {"e_kj_per_m2": "Radiação Solar [$MJ/m^2$]",
              "load_kwh": "Energia Ativa Consumida\n[$MWh$]",
              "load_kvarh": "Energia Reativa Consumida\n[$Mvarh$]",
              "gen_energy_kwh": "Energia Gerada [$MWh$]",
              "temperature_celsius": "Temperatura [$^{o}C$]",
              "pf": "Fator de Potência",
              "old_pf": "Fator de Potência Antigo",
              "new_pf": "Valor Absoluto do Novo \nFator de Potência",
              "min_q_cost_pf":
              "Valor Absoluto do \nFator de Potência Maximizado",
              "opt_pf": "Valor Absoluto do \nFator de Potência Otimizado",
              "percentual_extra_reactive_charges":
              "Encargos por Reativos Excedentes em "
              + "\nPercentual da Economia Bruta"}

y_label = en_y_label.copy()

en_x_label = {"hour": "Hour", "month": "Month", "month/year": "Month/Year",
              "spring": "Spring", "winter": "Winter", "autumn": "Autumn",
              "summer": "Summer", "percentiles": "Percentiles",
              "not_winter": "Not Winter", "not_summer": "Not Summer",
              "holidays": "Holidays", "vacances": "Vacation",
              "business": "Business Day", "holiday": "Holiday",
              "vacation": "Vacation",
              "not_summer/holidays": "Not Sum. Hol.",
              "not_summer/not_winter": "Spr. Aut. Bus",
              "not_summer/vacances": "Not Sum. Vac.",
              "not_summer/winter": "Win. Bus",
              "summer/holidays": "Sum. Hol.", "summer/not_winter": "Sum. Bus.",
              "summer/vacances": "Sum. Vac.",
              "time_band": "Time Band",
              "season": "Season",
              "cluster": "Cluster",
              "off_peak": "Off Peak", "peak": "Peak",
              "type_of_day": "Type of Day"}

pt_x_label = {"hour": "Hora", "month": "Mês", "month/year": "Mês/Ano",
              "spring": "Primavera", "winter": "Inverno", "autumn": "Outono",
              "summer": "Verão", "percentiles": "Percentis",
              "not_winter": "Não Inverno", "not_summer": "Não Verão",
              "holidays": "Feriados", "vacances": "Férias",
              "business": "Dia Útil", "holiday": "Feriado",
              "vacation": "Férias",
              "not_summer/holidays": "Feriado Não Ver.",
              "not_summer/not_winter": "Dia Útil Pri. Out.",
              "not_summer/vacances": "Férias Não Ver.",
              "not_summer/winter": "Dia Útil Inv.",
              "summer/holidays": "Feriado Ver.",
              "summer/not_winter": "Dia Útil Ver.",
              "summer/vacances": "Férias Ver.",
              "time_band": "Posto Tarifário",
              "season": "Estação",
              "cluster": "Grupo",
              "off_peak": "Fora Ponta", "peak": "Ponta",
              "type_of_day": "Natureza do Dia"}

x_label = en_x_label.copy()

en_legend_labels = {"spring": "Spring", "winter": "Winter", "autumn": "Autumn",
                    "summer": "Summer", "not_winter": "Not Winter",
                    "not_summer": "Not Summer", "holidays": "Holidays",
                    "holiday": "Holiday", "vacation": "Vacation",
                    "vacances": "Vacation", "business": "Business"}

pt_legend_labels = {"spring": "Primavera", "winter": "Inverno",
                    "autumn": "Outono", "summer": "Verão",
                    "not_winter": "Não Inverno", "not_summer": "Não Verão",
                    "holidays": "Feriados", "holiday": "Feriado",
                    "vacances": "Férias", "vacation": "Férias",
                    "business": "Dia Útil"}

legend_labels = en_legend_labels.copy()

en_hp_legend = {"e_kj_per_m2": "Solar Radiation",
                "load_kwh": "Consumed Active Energy",
                "load_kvarh": "Consumed Reactive Energy",
                "gen_energy_kwh": "Generated Energy",
                "temperature_celsius": "Temperature",
                "old_pf": "Old Power Factor",
                "new_pf": "New Power Factor",
                "pf": "Power Factor",
                "old_q_cost": "Old Power Factor Charge",
                "new_q_cost": "New Power Factor Charge"}

pt_hp_legend = {"e_kj_per_m2": "Radiação Solar",
                "load_kwh": "Energia Ativa Consumida",
                "load_kvarh": "Energia Reativa Consumida",
                "gen_energy_kwh": "Energia Gerada",
                "temperature_celsius": "Temperatura",
                "old_pf": "Fator de Potência Antigo",
                "new_pf": "Novo Fator de Potência",
                "pf": "Fator de Potência",
                "old_q_cost": "Encargo por Reativos Antigo",
                "new_q_cost": "Novo Encargo por Reativos"}

hp_legend = en_hp_legend.copy()

en_table_labels = {"type_of_day": "Type of Day", "time_band": "Time Band",
                   "off_peak": "Off Peak", "peak": "Peak",
                   "load_kvarh": "Consumed Reactive Energy",
                   "load_kwh": "Consumed Active Energy",
                   "business": "Business Day", "holiday": "Holiday",
                   "holidays": "Holidays", "vacation": "Vacation",
                   "vacances": "Vacation", "autumn": "Autumn",
                   "spring": "Spring", "winter": "Winter",
                   "not_winter": "Not Winter", "summer": "Summer",
                   "not_summer": "Not Summer",
                   "number_of_days": "Number of Days", "season": "Season",
                   "whole": "Whole", "no_cluster": "No Cluster",
                   "cluster": "Cluster", "iqr_cv": "IQR CV",
                   "segmentation": "Segmentation", "generation": "Generation",
                   "consumption": "Consumption", "hour": "Hour",
                   "density": "Density", "aic": "AIC",
                   "ks_approved": "KS Approved", "variable": "Variable",
                   "temperature_celsius": "Temperature",
                   "e_kj_per_m2": "Solar Radiation",
                   "single": "Single", "best": "Best", True: "Yes",
                   False: "No", "continue": "Continued on next page",
                   "old_q_cost": "Old Power Factor Charge",
                   "new_q_cost": "New Power Factor Charge",
                   "old_pf": "Old Power Factor",
                   "new_pf": "New Power Factor",
                   "min_q_gen_pf": "Maximized Power Factor",
                   "opt_gen_pf": "Optimized Power Factor",
                   "energy_savings": "Savings (R\\$)",
                   "one_pf": "$pf=1$",
                   "min_q_cost_gen": "Maximized $pf$",
                   "opt_gen": "Optimized $pf$",
                   "energy": "Energy Bill",
                   "reactive": "Excess Reactive Bill",
                   "total": "Total",
                   "value": "Value",
                   "no_gen": "No Generation",
                   "savings": "Savings",
                   "method": "Method",
                   "type": "Type"}

pt_table_labels = {"type_of_day": "Natureza do Dia",
                   "time_band": "Posto Tarifário", "off_peak": "Fora Ponta",
                   "peak": "Ponta", "load_kvarh": "Energia Reativa Consumida",
                   "load_kwh": "Energia Ativa Consumida",
                   "business": "Dia Útil", "holiday": "Feriado",
                   "holidays": "Feriados", "vacation": "Férias",
                   "vacances": "Férias", "autumn": "Outono",
                   "spring": "Primavera", "winter": "Inverno",
                   "not_winter": "Não Inverno", "summer": "Verão",
                   "not_summer": "Não Verão",
                   "number_of_days": "Número de Dias", "season": "Estação",
                   "whole": "Inteiro", "no_cluster": "Sem segmentação",
                   "cluster": "Grupo", "iqr_cv": "CVI",
                   "segmentation": "Segmentação", "generation": "Geração",
                   "consumption": "Consumo", "hour": "Hora",
                   "density": "Distribuição", "aic": "AIC",
                   "ks_approved": "Aprovado no Teste KS",
                   "variable": "Variável",
                   "temperature_celsius": "Temperatura",
                   "e_kj_per_m2": "Radiação Solar",
                   "single": "Escolhida", "best": "Melhor", True: "Sim",
                   False: "Não", "continue": "Continua na próxima página",
                   "old_q_cost": "Encargo por Reativos Antigo (R\\$)",
                   "new_q_cost": "Novo Encargo por Reativos (R\\$)",
                   "old_pf": "Fator de Potência Antigo",
                   "new_pf": "Novo Fator de Potência",
                   "min_q_gen_pf": "Fator de Potência Maximizado",
                   "opt_gen_pf": "Fator de Potência Otimizado",
                   "energy_savings": "Economia (R\\$)",
                   "one_pf": "$fp=1$",
                   "min_q_cost_gen": "$fp$ Maximizado",
                   "opt_gen": "$fp$ Otimizado",
                   "energy": "Energia",
                   "reactive": "Encargos por Reativos",
                   "total": "Total",
                   "value": "Valor",
                   "no_gen": "Sem Geração",
                   "savings": "Economia",
                   "method": "Paradigma",
                   "type": "Tipo"}

table_labels = en_table_labels.copy()


def set_language(lang="english"):
    """
    This function modifies the language settings for labels used in plots.

    :param lang: The language to use for the labels. It can be "english" or
        "portuguese".
    :type lang: str, default "english"
    :raises ValueError: If the language specified is not supported.
    :return: The language that has been set.
    :rtype: str

    :example:

        >>> from capitu.dap.plotter.labels import set_language

        To set the language to English:

        >>> set_language("english")
        'english'

        To set the language to Portuguese:

        >>> set_language("portuguese")
        'portuguese'

        Attempting to set an unsupported language will raise a ValueError:

        >>> set_language("french")
        Traceback (most recent call last):
        ...
        ValueError: I don't know how to speak this language yet.
    """

    # These are the dictionaries actually used in the plots.
    original_dicts = [y_label, x_label, legend_labels, hp_legend, table_labels]

    # Check which language to use. If it's English, load the English versions.
    if lang == "english":
        new_dicts = [en_y_label, en_x_label, en_legend_labels, en_hp_legend,
                     en_table_labels]

    # If it's Portuguese, load the Portuguese versions.
    elif lang == "portuguese":
        new_dicts = [pt_y_label, pt_x_label, pt_legend_labels, pt_hp_legend,
                     pt_table_labels]

    # If anything else, raise an error.
    else:
        raise ValueError("I don't know how to speak this language yet.")

    # Now change the original dicts to the loaded values.
    for i, original_dict in enumerate(original_dicts):
        for key, new_value in new_dicts[i].items():
            if key in original_dict:
                original_dict[key] = new_value

    return lang
