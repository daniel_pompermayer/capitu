#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Prepare subplots of my custom facet grid plots.
"""

import numpy as np
from .labels import x_label


def prepare_column_subplot(fig, ncol, number_of_cols, number_of_categories):
    """
    Create a subplot for a whole column of subplots with xticks disabled.
    It has been created for having the xticks, in my custom facet grid plots
    as the pdfplot. I'm creating a big subplot as the whole column of subplots.
    That big subplot must have all the splines and grids and ticks off, so it
    will not bother the subplots on it.

    :param fig: The figure to add the subplot to.
    :type fig: matplotlib.figure.Figure
    :param ncol: The column index of the subplot.
    :type ncol: int
    :param number_of_cols: Total number of columns in the subplot grid.
    :type number_of_cols: int
    :param number_of_categories: Total number of categories.
    :type number_of_categories: int
    :return: The created column subplot.
    :rtype: matplotlib.axes.Axes
    """

    col_ax = fig.add_subplot(
        1, int(number_of_cols/number_of_categories), ncol + 1)
    _ = col_ax.patch.set_visible(False)
    _ = col_ax.spines['left'].set_visible(False)
    _ = col_ax.spines['right'].set_visible(False)
    _ = col_ax.spines['top'].set_visible(False)
    _ = col_ax.spines['bottom'].set_visible(False)
    _ = col_ax.grid(visible=False, axis="x")
    _ = col_ax.grid(visible=False, axis="y")
    _ = col_ax.tick_params(axis='both', which='both', bottom=False,
                           top=False, left=False, right=False,
                           labelbottom=False, labelleft=False)
    return col_ax


def prepare_cell_subplot(fig, nrow, ncol, nvar, number_of_rows,
                         number_of_cols, number_of_categories):
    """
    Create a cellular subplot for plotting the data parcel in my custo facet
    grid plots like the pdfplot.

    :param fig: The figure to add the subplot to.
    :type fig: matplotlib.figure.Figure
    :param nrow: The row index of the subplot.
    :type nrow: int
    :param ncol: The column index of the subplot.
    :type ncol: int
    :param nvar: The variable index of the subplot.
    :type nvar: int
    :param number_of_rows: Total number of rows in the subplot grid.
    :type number_of_rows: int
    :param number_of_cols: Total number of columns in the subplot grid.
    :type number_of_cols: int
    :param number_of_categories: Total number of categories.
    :type number_of_categories: int
    :return: The created cellular subplot.
    :rtype: matplotlib.axes.Axes
    """

    # Add the subplot in the right position.
    pos = (nrow * number_of_cols + number_of_categories * ncol + nvar + 1)
    ax = fig.add_subplot(number_of_rows, number_of_cols, pos)

    # Hide the box of the plots.
    _ = ax.spines['left'].set_visible(False)
    _ = ax.spines['right'].set_visible(False)
    _ = ax.spines['top'].set_visible(False)
    _ = ax.spines['bottom'].set_visible(False)

    # Hide the ticks.
    _ = ax.tick_params(
        axis='both', which='both', bottom=False, top=False,
        left=False, right=False, labelbottom=False,
        labelleft=False)

    return ax


def close_cell_subplot(ax, density_peak, hist_xlim, a, c, nvar, ncol, nrow,
                       rows, row_category, fontsize, percentual_yticks=True,
                       yticklabel_format="{:.2f}", ylim=None):
    """
    Make adjustments in a cell subplot of my custom facet grid plots like the
    pdfplot.

    :param ax: The subplot to adjust.
    :type ax: matplotlib.axes.Axes
    :param density_peak: The coefficient determining how much the peak of the
        pdf can be greater than the peak of the histogram. If the ratio between
        the peaks exceeds this coefficient, the pdf will appear clipped. If
        None, the pdf will not be clipped.
    :type density_peak: float or None
    :param hist_xlim: The limits of the histogram x-axis.
    :type hist_xlim: tuple
    :param a: Lower limit of the data.
    :type a: float
    :param c: Upper limit of the data.
    :type c: float
    :param nvar: The variable index of the subplot.
    :type nvar: int
    :param ncol: The column index of the subplot.
    :type ncol: int
    :param nrow: The row index of the subplot.
    :type nrow: int
    :param rows: Total number of rows in the subplot grid.
    :type rows: int
    :param row_category: Category of the row.
    :type row_category: str or None
    :param fontsize: Font size for the ylabel.
    :type fontsize: int
    :param percentual_yticks: Whether to display yticks as percentages.
    :type percentual_yticks: bool, default True
    :param yticklabel_format: Format for ytick labels.
    :type yticklabel_format: str, default "{:.2f}"
    :param ylim: Limits for the y-axis.
    :type ylim: tuple or None
    """

    # Compare the new xlim after plotting the density with the histogram bins
    # size.
    if density_peak is not None:
        density_xlim = ax.get_xlim()
        if density_xlim[1] > (hist_xlim[1] * density_peak):
            ax.set_xlim([hist_xlim[0], hist_xlim[1] * density_peak])

    # Get y ticks values and labels.
    if percentual_yticks:
        yticks = np.linspace(a, c, 5)
        yticklabels = ["0%", "25%", "50%", "75%", "100%"]
    else:
        if ylim is not None:
            yticks = np.linspace(*ylim, 5)
        else:
            yticks = np.linspace(a, c, 5)
            ylim = (a, c)
        yticklabels = [yticklabel_format.format(tick) for tick in yticks]

    # Turn on the y grid and off the x grid. Rememeber the plots is rotated.
    _ = ax.set_ylim(ylim)
    _ = ax.set_yticks(yticks)
    _ = ax.set_yticklabels(yticklabels)
    _ = ax.grid(visible=False, axis="x")
    _ = ax.grid(visible=True, axis="y")
    _ = ax.set_axisbelow(True)

    # The first variable, the one in the left, must be mirrored and must not
    # have the right spine hidden.
    if nvar == 0:
        _ = ax.set_xlim(ax.get_xlim()[::-1])
        _ = ax.spines['right'].set_visible(True)

        # If I'm plotting the first column of the first variable, I wanna have
        # y ticks and maybe labels.
        if ncol == 0:
            _ = ax.tick_params(
                axis='both', which='both', bottom=False, top=False,
                left=False, right=False, labelbottom=False,
                labeltop=False, labelleft=True, labelright=False)
            if row_category is not None:
                _ = ax.set_ylabel(x_label[rows[nrow]], fontsize=fontsize)
