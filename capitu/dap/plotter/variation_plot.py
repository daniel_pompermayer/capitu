from .lineplot import lineplot


def variation_plot(data, path=None, show=False, classes=None, variables=[],
                   categories=None, scale_factor=None, xlim=None,
                   prefix="", ax_height=12, ax_width=16, fontsize=8,
                   ylabel="auto", format="pgf", seed=42):

    """
    Plot variation of variables across categories and classes of a data frame.
    The variation plots consist of the mean of each variable with a confidence
    interval around it, showing how it varies across all categories and
    classes.

    :param data: Pandas DataFrame containing the data to be plotted.
    :type data: pandas.core.frame.DataFrame
    :param path: Path to save the plot. If None, the plot will not be saved.
    :type path: str or None, default: None
    :param show: If True, the plot will be displayed.
    :type show: bool, default: False.
    :param classes: Name of the variable representing classes.
    :type classes: str or None, default: None.
    :param variables: List of variables to be plotted. Default is an empty
        list.
    :type variables: list, optional
    :param categories: Name of the variable representing categories.
    :type categories: str or None, default: None.
    :param scale_factor: List of scale factors for each variable.
    :type scale_factor: list or None, default: None.
    :param xlim: Limits for the x-axis.
    :type xlim: tuple or None, default: None.
    :param prefix: Prefix to be added to the plot filename.
    :type prefix: str, default: ""
    :param ax_height: The height of each subplot.
    :type ax_height: int or float, default: 12
    :param ax_width: The width of each subplot.
    :type ax_width: int or float, default: 16
    :param fontsize: The font size of the axis labels and title.
    :type fontsize: int, default: 8
    :param ylabel: The label text to attribute to the y-axis. If "auto",
        will use the column name.
    :type ylabel: str or "auto", default: "auto"
    :param format: Specifies the file format for saving the plot.
    :type format: str, default: "pgf"
    :param seed: Seed or random number generator for reproducible
        bootstrapping.
    :type seed: int, default: 42

    :examples:
        >>> import os
        >>> import numpy as np
        >>> import pandas as pd
        >>> from scipy.stats import norm, triang, beta
        >>> from PIL import Image, ImageChops
        >>> from capitu import dap
        >>> from capitu.dap.parser import datetime_to_season
        >>> from capitu.dap.plotter.variation_plot import variation_plot
        >>> dap.set_language("english")
        >>> nine_am = pd.date_range(
        ...     start='2023-01-01 09:00:00', end='2023-12-31 09:00:00',
        ...     freq='D')
        >>> data_size = len(nine_am)
        >>> np.random.seed(42)
        >>> temp_9am = norm.rvs(loc=26, scale=3, size=data_size)
        >>> np.random.seed(42)
        >>> solar_rad_9am = norm.rvs(loc=1345, scale=575, size=data_size)
        >>> eleven_am = pd.date_range(
        ...     start='2023-01-01 11:00:00', end='2023-12-31 11:00:00',
        ...     freq='D')
        >>> np.random.seed(42)
        >>> temp_11am = norm.rvs(loc=28, scale=3, size=data_size)
        >>> np.random.seed(42)
        >>> solar_rad_11am = norm.rvs(loc=2300, scale=850, size=data_size)
        >>> one_pm = pd.date_range(
        ...     start='2023-01-01 13:00:00', end='2023-12-31 13:00:00',
        ...     freq='D')
        >>> np.random.seed(42)
        >>> temp_1pm = triang.rvs(c=0.6, loc=19, scale=17, size=data_size)
        >>> np.random.seed(42)
        >>> solar_rad_1pm = triang.rvs(c=1, loc=-30, scale=4100,
        ...                            size=data_size)
        >>> three_pm = pd.date_range(
        ...     start='2023-01-01 15:00:00', end='2023-12-31 15:00:00',
        ...     freq='D')
        >>> np.random.seed(42)
        >>> temp_3pm = beta.rvs(a=5, b=3, loc=16, scale=19, size=data_size)
        >>> np.random.seed(42)
        >>> solar_rad_3pm = beta.rvs(a=1, b=1, loc=66, scale=3560,
        ...                          size=data_size)
        >>> data = pd.DataFrame({
        ...     "temperature_celsius": np.concatenate([
        ...         temp_9am, temp_11am, temp_1pm, temp_3pm]),
        ...     "e_kj_per_m2": np.concatenate([
        ...         solar_rad_9am, solar_rad_11am, solar_rad_1pm,
        ...         solar_rad_3pm])},
        ...         index=np.concatenate([nine_am, eleven_am, one_pm,
        ...                               three_pm]))
        >>> data["hour"] = data.index.hour
        >>> data["season"] = datetime_to_season(data.index)
        >>> variation_plot(data, path="./", show=False, classes="hour",
        ...                variables=["temperature_celsius", "e_kj_per_m2"],
        ...                categories="season", xlim=[9, 15], format="png")
        >>> target = Image.open('./line_e_kj_per_m2.png')
        >>> template = Image.open(
        ...     '../capitu/datastore/doctest/line_e_kj_per_m2.png')
        >>> diff = ImageChops.difference(target, template)
        >>> diff.getbbox() is None
        True
        >>> os.remove('./line_e_kj_per_m2.png')
        >>> target = Image.open('./line_temperature_celsius.png')
        >>> template = Image.open(
        ...     '../capitu/datastore/doctest/line_temperature_celsius.png')
        >>> diff = ImageChops.difference(target, template)
        >>> diff.getbbox() is None
        True
        >>> os.remove('./line_temperature_celsius.png')
    """

    if scale_factor is None:
        scale_factor = len(variables) * [None]

    # Iterate over the variables to be filtered.
    for n, variable in enumerate(variables):

        # Plot the mean of the variable with a confidence interval around
        # it, showing how it varies across all categories and classes.
        lineplot(data.reset_index(), path=path, show=show, x=classes,
                 y=variable, hue=categories, scale_factor=scale_factor[n],
                 ylim=None, xlim=xlim, prefix=prefix, ax_height=ax_height,
                 ax_width=ax_width, fontsize=fontsize, ylabel=ylabel,
                 format=format, seed=seed)
