#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Plot and save a facet grid of histograms with densities.
"""

import scipy.stats
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from matplotlib.lines import Line2D
from .colors import pdfplot as pdf_colors
from .labels import x_label, hp_legend
from .subplots import prepare_cell_subplot, prepare_column_subplot
from .subplots import close_cell_subplot
from . import close_plot


def pdfplot(data, pdfs, y, best_pdfs=None, x="hour", path=None, show=False,
            row_category=None, fig_height=12, fig_width=17, fontsize=10,
            format="pgf", density_peak=1.1, filename="my_data",
            pdfs_label=None, best_label=None):
    """
    Plot and save a facet grid of histograms with densities.

    :param data: time indexed data frame having the data we're plotting.
    :type data: pandas.core.frame.DataFrame
    :param pdfs: Pandas DataFrame containing the data pdf information.
    :type pdfs: pandas.core.frame.DataFrame
    :param y: String or list of strings containing the name(s) of the column(s)
        with the data to be plotted.
    :type y: str or list[str], default: "e_kj_per_m2"
    :raise ValueError: If y is not a string nor a list or has more than 2
        members.
    :param best_pdfs: Pandas DataFrame containing the best density information.
        It's similar to the `pdfs` parameter, except that while you may draw a
        good density chosen to model the data in the `pdfs` parameter, the
        `best_pdfs` parameter should contain the very best density of that
        parcel of data. Pass `None` if you do not wish to plot this best
        density.
    :type best_pdfs: pandas.core.frame.DataFrame, default: None
    :param x: Determines how data will be split. Options are "hour" to split by
        hours, "month" to split by months, or "month/year" to split into month
        and year.
    :type x: str, default: "hour"
    :param path: Directory where the plot will be saved. If None, the plot will
        not be saved.
    :type path: str or None, default: None
    :param show: if True the plot will be presented in the screen.
    :type show: bool, default: False
    :param row_category: name of the column having the information that will
        split the data in rows.
    :type row_category: str, default: None
    :param fig_height: Height of the figure that will be plotted.
    :type fig_height: int, default: 12
    :param fig_width: Width of the figure that will be plotted.
    :type fig_width: int, default: 17
    :param fontsize: Fontsize of the texts of the plot.
    :type fontsize: int, default: 10
    :param format: Specifies the file format for saving the plot.
    :type format: str, default: "pgf"
    :param density_peak: Maximum ratio between the biggest histogram bar and
        the density plot size. If the density plot size is bigger than the
        biggest histogram bar, it will be cropped in density_peak times the
        size of biggest bar.
    :type density_peak: float, default: 1.1
    :param filename: string having the name of the file to be saved.
    :type filename: str, default: "my_data"
    :param pdfs_label: Label for the density plot represented by the `pdfs`
        parameter.
    :type pdfs_label: str or None
    :param best_label: Label for the density plot represented by the
        `best_pdfs` parameter.
    :type best_label: str or None

    :examples:
        >>> import os
        >>> import numpy as np
        >>> import pandas as pd
        >>> from PIL import Image, ImageChops
        >>> from capitu import dap
        >>> from scipy.stats import norm, triang, beta
        >>> from capitu.dap.stats.find_rvs import find_rvs
        >>> from capitu.dap.plotter.pdfplot import pdfplot
        >>> dap.set_language("english")
        >>> nine_am = pd.date_range(
        ...     start='2023-01-01 09:00:00', end='2023-12-31 09:00:00',
        ...     freq='D')
        >>> data_size = len(nine_am)
        >>> np.random.seed(42)
        >>> temp_9am = norm.rvs(loc=26, scale=3, size=data_size)
        >>> np.random.seed(42)
        >>> solar_rad_9am = norm.rvs(loc=1345, scale=575, size=data_size)
        >>> eleven_am = pd.date_range(
        ...     start='2023-01-01 11:00:00', end='2023-12-31 11:00:00',
        ...     freq='D')
        >>> np.random.seed(42)
        >>> temp_11am = norm.rvs(loc=28, scale=3, size=data_size)
        >>> np.random.seed(42)
        >>> solar_rad_11am = norm.rvs(loc=2300, scale=850, size=data_size)
        >>> one_pm = pd.date_range(
        ...     start='2023-01-01 13:00:00', end='2023-12-31 13:00:00',
        ...     freq='D')
        >>> np.random.seed(42)
        >>> temp_1pm = triang.rvs(c=0.6, loc=19, scale=17, size=data_size)
        >>> np.random.seed(42)
        >>> solar_rad_1pm = triang.rvs(c=1, loc=-30, scale=4100,
        ...                            size=data_size)
        >>> three_pm = pd.date_range(
        ...     start='2023-01-01 15:00:00', end='2023-12-31 15:00:00',
        ...     freq='D')
        >>> np.random.seed(42)
        >>> temp_3pm = beta.rvs(a=5, b=3, loc=16, scale=19, size=data_size)
        >>> np.random.seed(42)
        >>> solar_rad_3pm = beta.rvs(a=1, b=1, loc=66, scale=3560,
        ...                          size=data_size)
        >>> data = pd.DataFrame({
        ...     "temperature_celsius": np.concatenate([
        ...         temp_9am, temp_11am, temp_1pm, temp_3pm]),
        ...     "e_kj_per_m2": np.concatenate([
        ...         solar_rad_9am, solar_rad_11am, solar_rad_1pm,
        ...         solar_rad_3pm])},
        ...         index=np.concatenate([nine_am, eleven_am, one_pm,
        ...                               three_pm]))
        >>> data["hour"] = data.index.hour
        >>> single_params, best_params, comparison = find_rvs(
        ...     data, y=["temperature_celsius", "e_kj_per_m2"],
        ...     approach="single", categories="hour", pdfs_path=None,
        ...     tables_path=None, filename="pdfs",
        ...     candidates = ["norm", "triang", "uniform", "beta"])
        >>> pdfplot(data, pdfs=single_params,
        ...         y=["temperature_celsius", "e_kj_per_m2"],
        ...         best_pdfs=best_params, x="hour", path="./", show=False,
        ...         row_category=None, filename="my_data",
        ...         pdfs_label="single", best_label="best", format="png")
        >>> target = Image.open('./my_data.png')
        >>> template = Image.open('../capitu/datastore/doctest/pdfplot.png')
        >>> diff = ImageChops.difference(target, template)
        >>> diff.getbbox() is None
        True
        >>> os.remove('./my_data.png')
    """

    # Do not bother external data.
    data = data.copy()
    pdfs = pdfs.copy()
    best_pdfs = best_pdfs.copy()

    # Below I'm getting the number of rows, cols and categories and I'll use
    # that information in the whole function.
    # Check how many rows we're plotting.
    if row_category is None:
        rows = None
        number_of_rows = 1
    else:
        rows = data[row_category].drop_duplicates().tolist()
        number_of_rows = len(rows)

    # I need y to be a list of column names. So I'll check whether y is a
    # single column name or a list of column names and adjust it. I'm also
    # getting the number of variables from the length of y.
    if isinstance(y, str):
        number_of_variables = 1
        y = [y]
    elif isinstance(y, list):
        number_of_variables = len(y)
    else:
        raise ValueError("y should be a name of a column or a list of names.")

    # I cannot handle more than two variables.
    if number_of_variables > 2:
        raise ValueError("I cannot handle more than two variables.")

    # Check how many columns we're plotting.
    cols = list(sorted(data[x].drop_duplicates().values))
    number_of_cols = len(cols) * number_of_variables

    # Search for the densities in scipy.stats package.
    pdfs["rv"] = pdfs.apply(_get_rv, axis=1)
    if best_pdfs is not None:
        best_pdfs["rv"] = best_pdfs.apply(_get_rv, axis=1)

    # Create a figure for my beautiful plot.
    cm = 1/2.54
    fig = plt.figure(figsize=(fig_width * cm, fig_height * cm))
    fig.subplots_adjust(left=0.2, bottom=0.095, right=0.96, top=0.88,
                        wspace=0, hspace=0.2)

    # Create a list of patches, that are the little squares in the legend.
    # They'll be filled in in the end of each variable iteration.
    patches = []

    # I'll iterate over the variables and the cols to plot the data.
    for nvar, variable in enumerate(y):
        for ncol, col in enumerate(cols):

            # Create a big subplot for the whole column.
            col_ax = prepare_column_subplot(
                fig, ncol, number_of_cols, number_of_variables)

            # I'm plotting row by row.
            for nrow in range(0, number_of_rows):

                # Write a query to get only the matched features.
                if row_category is None:
                    query = data[x] == col
                else:
                    query = np.all([data[row_category] == rows[nrow],
                                    data[x] == col], axis=0)
                plotting_parcel = data.loc[query, variable].dropna()

                # For plotting every parcel in the same y-axis, I'll need to
                # normalize it using minimun and maximum values.
                a = plotting_parcel.min()
                c = plotting_parcel.max()

                # Create a cellular subplot for the ploting the data parcel.
                ax = prepare_cell_subplot(
                    fig, nrow, ncol, nvar, number_of_rows, number_of_cols,
                    number_of_variables)

                # Plot the histogram.
                _ = ax.hist(plotting_parcel, bins=10, orientation="horizontal",
                            density=True, color=pdf_colors[-nvar - 2],
                            edgecolor=pdf_colors[nvar])

                # Some densities may exceed very much the size of the bars of
                # the histogram, what will result in an awful plot. So I'll
                # store the xlim so I can check it further if needed.
                hist_xlim = ax.get_xlim()

                # Write a query to get only the matched features.
                if row_category is None:
                    query = np.all([
                        pdfs[x] == col, pdfs["variable"] == variable], axis=0)
                else:
                    query = np.all([
                        pdfs[row_category] == rows[nrow], pdfs[x] == col,
                        pdfs["variable"] == variable], axis=0)
                rv = pdfs.loc[query, "rv"].iloc[0]
                if best_pdfs is not None:
                    best_rv = best_pdfs.loc[query, "rv"].iloc[0]

                # Plot the density.
                if best_pdfs is not None:
                    _ = ax.plot(best_rv.pdf(np.linspace(a, c, 1000)),
                                np.linspace(a, c, 1000), color=pdf_colors[2],
                                linestyle="dashed", linewidth=0.85)
                _ = ax.plot(rv.pdf(np.linspace(a, c, 1000)),
                            np.linspace(a, c, 1000), color=pdf_colors[0],
                            linewidth=1)

                # Plot adjusts.
                close_cell_subplot(ax, density_peak, hist_xlim, a, c, nvar,
                                   ncol, nrow, rows, row_category, fontsize)

            # Use the column subplot to plot my xtick label.
            _ = col_ax.set_xlabel(str(col))

        # Now that the variable iteration is almost over, I can create my
        # patch.
        patches.append(mpatches.Patch(color=pdf_colors[-nvar - 2],
                                      label=hp_legend[variable]))

    # Add, if I must, patch to the densities.
    legend_n_col = 1
    if pdfs_label is not None:
        patches.append(Line2D([0], [0], label=pdfs_label, color=pdf_colors[0]))
        legend_n_col = 2

    if best_label is not None:
        patches.append(Line2D([0], [0], label=best_label, color=pdf_colors[2],
                              linestyle="dashed", linewidth=0.85))
        legend_n_col = 2

    # Create the legend and the figure labels.
    _ = fig.legend(handles=patches, fontsize=fontsize, ncol=legend_n_col)
    _ = fig.supxlabel(x_label[x], fontsize=fontsize)
    _ = fig.supylabel("Fraction of the range $\\left [ \\dfrac{Value - min}"
                      + "{max - min} \\times 100\\% \\right ]$",
                      fontsize=fontsize)

    close_plot(path=path, show=show, format=format, filename=filename, fig=fig,
               ax=None, tight_layout=False)


def _get_rv(cluster_row):

    # Get rid of the void attributes.
    params = cluster_row[cluster_row == cluster_row].copy()

    # Get the density.
    density = params.density

    # Get rid of the cluster identifications.
    params = params.drop(["hour", "cluster", "variable", "aic", "ks_approved",
                          "density"], errors="ignore")

    return getattr(scipy.stats, density).freeze(**params.to_dict())
