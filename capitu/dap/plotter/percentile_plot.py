#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Plot and save a cumulative percentile plot.
"""

import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from . import close_plot


def percentile_plot(data, cutoff, y="percentual_extra_reactive_charges",
                    path=None, show=False, yticks=[0, 5, 10, 15, 20],
                    legend=False, ylim=(0, 20), fig_height=10, fig_width=15,
                    cutoff_unit="$\\%$", cutoff_format="{:.0f}", format="pgf"):
    """
    Plot and save a cumulative percentile plot.

    :param data: Data to plot or a list of data.
    :type data: pandas.core.series.Series or list of pandas.core.series.Series
        or pandas.DataFrame
    :param cutoff: Percentage to be highlighted.
    :type cutoff: int
    :param y: The column name of the data frame containing the y-values.
    :type y: str, Default: "percentual_extra_reactive_charges"
    :param path: Address to the directory where the plot will be saved.
        If None, the plot will not be saved.
    :type path: str or None, Default: None
    :param show: If True, the plot will be displayed on the screen.
    :type show: bool, Default: False
    :param yticks: List of values determining the y tick marks.
    :type yticks: [float], Default: [0, 5, 10, 15, 20]
    :param legend: If True, a legend will be plotted. If list, legend labels
        for each dataset provided in `data`.
    :type legend: bool or list, Default: False
    :param ylim: Seaborn ylim parameter.
    :type ylim: tuple, Default: (0, 20)
    :param fig_height: Height of the figure in cm.
    :type fig_height: float, Default: 10
    :param fig_width: Width of the figure in cm.
    :type fig_width: float, Default: 15
    :param cutoff_unit: Unit of the cutoff value.
    :type cutoff_unit: str, Default: "$\\%$"
    :param cutoff_format: Format for the cutoff value.
    :type cutoff_format: str, Default: "{:.0f}"
    :param format: Specifies the file format for saving the plot.
    :type format: str, default: "pgf"

    :example:
        >>> import os
        >>> import numpy as np
        >>> import pandas as pd
        >>> from PIL import Image, ImageChops
        >>> from capitu import dap
        >>> from capitu.dap.plotter.percentile_plot import percentile_plot
        >>> dap.set_language("english")
        >>> np.random.seed(42)
        >>> old_pf = pd.DataFrame({'pf': np.random.beta(2, 3, 1000)})
        >>> np.random.seed(42)
        >>> new_pf = pd.DataFrame({'pf': np.random.beta(2, 5, 1000)})
        >>> percentile_plot(
        ...     [old_pf, new_pf], cutoff=90, y="pf", path="./", show=False,
        ...     yticks=[0, 0.25, 0.5, 0.75, 1], legend=["Old pf", "New pf"],
        ...     ylim=(0, 1), cutoff_unit="", cutoff_format="{:.2f}",
        ...     format="png")
        >>> target = Image.open('./percentile_plot.png')
        >>> template = Image.open(
        ...     '../capitu/datastore/doctest/percentile_plot.png')
        >>> diff = ImageChops.difference(target, template)
        >>> diff.getbbox() is None
        True
        >>> os.remove('percentile_plot.png')
    """

    # Check whether data is a single datus or a very set of data.
    if isinstance(data, (pd.DataFrame, pd.Series)):
        data = [data]

    if not isinstance(legend, list):
        legend = [""] * len(data)
        plot_legend = False
    else:
        plot_legend = True

    palette = sns.color_palette(palette="colorblind", n_colors=len(data))

    # Create a figure for my beautiful plot.
    cm = 1/2.54
    fig, ax = plt.subplots(figsize=(fig_width * cm, fig_height * cm))

    for n, datus in enumerate(data):

        # Get the colors to be used.
        colors = sns.light_palette(palette[n], n_colors=5)

        # Get the full percentile curve.
        x = range(0, 100)
        y_values = np.percentile(datus, x)

        # Get the cutoff curve.
        cutoff_x = range(0, cutoff + 1)
        cutoff_y = np.percentile(datus, cutoff_x)

        # Fill below the full percentile and the cutoff curve.
        _ = ax.fill_between(x, y_values, color=colors[2], label=legend[n])
        _ = ax.fill_between(cutoff_x, cutoff_y, color=colors[0])

        # Plot the full percentile and the cutoff curve.
        _ = ax.plot(x, y_values, color=colors[3], zorder=1)
        _ = ax.plot(cutoff_x, cutoff_y, color=colors[1], zorder=1)

        # Mark the cutoff point.
        _ = ax.scatter(cutoff, np.percentile(datus, cutoff), s=30,
                       color=colors[4], zorder=2)
        _ = ax.hlines(np.percentile(datus, cutoff), 0, cutoff, linestyle="--",
                      color=colors[4])
        _ = ax.vlines(cutoff, 0, np.percentile(datus, cutoff), linestyle="--",
                      color=colors[4])

        # Label the cutoff point.
        _ = ax.text(cutoff, np.percentile(datus, cutoff),
                    str(cutoff) + "$\\%$, "
                    + cutoff_format.format(np.percentile(datus, cutoff))
                    + cutoff_unit,
                    ha="right", va="bottom", color=colors[4])

        # Legend it.
        if plot_legend is not False:
            ax.legend()

    close_plot("percentiles", y, ylim, (0, 99), yticks, path, show,
               format=format, filename="percentile_plot", ax=ax)
