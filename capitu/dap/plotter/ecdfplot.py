import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from matplotlib.lines import Line2D
from .colors import ecdfplot as ecdf_colors
from .labels import x_label, hp_legend
from .subplots import prepare_cell_subplot, prepare_column_subplot
from .subplots import close_cell_subplot
from . import close_plot


def ecdfplot(data, y="variable", x="hour", cutoff=0.9,
             cutoff_mode="probability", cutoff_label="", fig_height=5.5,
             fig_width=17, format="pgf", fontsize=10, path=None, show=False,
             filename="my_data", ylabel="", ylim=None):
    """
    Plot and save a facet grid of empirical cdfs.

    :param data: time indexed data frame having the data we're plotting.
    :type data: pandas.core.frame.DataFrame
    :param y: string or list of strings having the name of the column with the
        data to be plotted.
    :type y: str or list[str], default: "e_kj_per_m2"
    :raise ValueError: If y is not a string nor a list or has more than 2
        members.
    :param x: if "hour", data will be splited by hours; if "month", data will
        be splited by months; if "month/year", data will be splited into month
        and year.
    :type x: str, default: "hour"
    :param cutoff: Float representing the threshold value for the empirical
        cumulative distribution function (ECDF) plot.
    :type cutoff: float, default: 0.9
    :param cutoff_mode: String specifying the mode of the cutoff parameter.
        Options are "probability" or "value". If set to "probability", the
        cutoff parameter represents a probability value (between 0 and 1). If
        set to "value", the cutoff parameter represents an actual data value.
    :type cutoff_mode: str, default: "probability"
    :param cutoff_label: String representing the label for the cutoff line in
        the plot. This label will be displayed in the plot legend.
    :type cutoff_label: str, default: ""
    :param fig_height: Height of the figure that will be plotted.
    :type fig_height: int, default: 5.5
    :param fig_width: Width of the figure that will be plotted.
    :type fig_width: int, default: 17
    :param format: Specifies the file format for saving the plot.
    :type format: str, default: "pgf"
    :param fontsize: Fontsize of the texts of the plot.
    :type fontsize: int, default: 10
    :param path: string having the address to the directory of where the plot
        will be saved. If None, the plot will not be saved.
    :type path: str or None, default: None
    :param show: if True the plot will be presented in the screen.
    :type show: bool, default: False
    :param filename: string having the name of the file to be saved.
    :type filename: str, default: "my_data"
    :param ylabel: Label for the y-axis.
        This parameter specifies the label for the y-axis of the plot.
    :type ylabel: str, default: "auto"
    :param ylim: matplotlib.pyplot ylim parameter.
    :type ylim: float or None, default: None

    :examples:
        >>> import os
        >>> import numpy as np
        >>> import pandas as pd
        >>> from PIL import Image, ImageChops
        >>> from capitu import dap
        >>> from scipy.stats import norm
        >>> from capitu.dap.stats.group_ecdf import group_ecdf
        >>> from capitu.dap.plotter.ecdfplot import ecdfplot
        >>> dap.set_language("english")
        >>> dates = pd.date_range(start='2023-01-01 09:00:00',
        ...                       end='2023-12-31', freq='2h')
        >>> dates = dates[np.logical_and(dates.hour >= 9, dates.hour <= 15)]
        >>> np.random.seed(42)
        >>> new_pf = norm.rvs(loc=0.8, scale=0.1, size=len(dates))
        >>> np.random.seed(42)
        >>> old_pf = norm.rvs(loc=0.92, scale=0.05, size=len(dates))
        >>> data = pd.DataFrame({"new_pf": new_pf, "old_pf": old_pf},
        ...                     index=dates)
        >>> data["hour"] = data.index.hour
        >>> ecdfs = group_ecdf(data, y=["new_pf", "old_pf"], x="hour")
        >>> ecdfplot(ecdfs, y=["new_pf", "old_pf"], x="hour", cutoff=0.92,
        ...          ylim=[0.5, 1], cutoff_mode="value", format="png",
        ...          cutoff_label="pf=0.92", path="./", show=False)
        >>> target = Image.open('./my_data.png')
        >>> template = Image.open(
        ...     '../capitu/datastore/doctest/ecdfplot.png')
        >>> diff = ImageChops.difference(target, template)
        >>> diff.getbbox() is None
        True
        >>> os.remove('./my_data.png')
    """

    # Do not bother external data.
    data = data.copy()

    # I need y to be a list of column names. So I'll check whether y is a
    # single column name or a list of column names and adjust it. I'm also
    # getting the number of variables from the length of y.
    if isinstance(y, str):
        number_of_variables = 1
        y = [y]
    elif isinstance(y, list):
        number_of_variables = len(y)
    else:
        raise ValueError("y should be a name of a column or a list of names.")

    # Check how many columns we're plotting.
    cols = list(sorted(data[x].drop_duplicates().values))
    number_of_cols = len(cols) * number_of_variables

    # Get the maximum and the minimum quantile value.
    limits = pd.DataFrame(dict(zip(y, [None, None])), index=["min", "max"])
    for variable in y:
        limits.loc["min", variable] = np.min(
            [datus.cdf.quantiles.min() for i, datus in data[variable].items()])
        limits.loc["max", variable] = np.max(
            [datus.cdf.quantiles.max() for i, datus in data[variable].items()])
    min_value = limits.loc["min"].min()
    max_value = limits.loc["max"].max()

    # Create a figure for my beautiful plot.
    cm = 1/2.54
    fig = plt.figure(figsize=(fig_width * cm, fig_height * cm))
    fig.subplots_adjust(left=0.1, bottom=0.18, right=0.98, top=0.85,
                        wspace=0, hspace=0.2)

    # Create a list of patches, that are the little squares in the legend.
    # They'll be filled in in the end of each variable iteration.
    patches = []

    # Compute the quantiles I'm using to plot, since I don't need to plot
    # them all.
    quantiles = np.linspace(min_value, max_value, 100)

    # I'll iterate over the variables and the cols to plot the data.
    for nvar, variable in enumerate(y):
        for ncol, col in enumerate(cols):

            # Create a big subplot for the whole column.
            col_ax = prepare_column_subplot(
                fig, ncol, number_of_cols, number_of_variables)

            # Compute the proabilities.
            probabilities = data.loc[ncol, variable].cdf.evaluate(quantiles)

            # Check whether the parsed cutoff is a quantile or a probability.
            if cutoff_mode == "probability":
                cutoff_level = quantiles[
                    np.abs(probabilities - cutoff).argmin()]
            else:
                cutoff_level = cutoff

            # Create a cellular subplot for the ploting the data parcel.
            ax = prepare_cell_subplot(
                fig, 0, ncol, nvar, 1, number_of_cols, number_of_variables)

            # Plot the histogram.
            _ = ax.plot(probabilities, quantiles, color=ecdf_colors[0],
                        linewidth=0.5)
            _ = ax.fill_betweenx(quantiles, x1=0, x2=probabilities,
                                 color=ecdf_colors[nvar])
            _ = ax.axhline(y=cutoff_level, color=ecdf_colors[2], linewidth=1)
            _ = ax.set_xlim([0, 1])

            # Plot adjusts.
            close_cell_subplot(ax, None, None, min_value, max_value, nvar,
                               ncol, None, None, None, fontsize, False,
                               ylim=ylim)

            # close_cell_subplot(ax, None, None, a, c, nvar,
            #                    ncol, nrow, rows, row_category, fontsize)

            # Use the column subplot to plot my xtick label.
            _ = col_ax.set_xlabel(str(col))

        # Now that the variable iteration is almost over, I can create my
        # patch.
        patches.append(mpatches.Patch(color=ecdf_colors[nvar],
                                      label=hp_legend[variable]))

    # Add the cutoff legend.
    patches.append(Line2D([0], [0], label=cutoff_label, color=ecdf_colors[2]))

    # Create the legend and the figure labels.
    _ = fig.legend(handles=patches, fontsize=fontsize, loc="upper right",
                   bbox_to_anchor=(1, 1), ncol=3)
    _ = fig.supxlabel(x_label[x], fontsize=fontsize)
    _ = fig.supylabel(ylabel, fontsize=fontsize)

    close_plot(path=path, show=show, format=format, filename=filename, fig=fig,
               ax=None, tight_layout=False)
