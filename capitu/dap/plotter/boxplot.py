#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Plot and save boxplots.
"""

import seaborn as sns
from . import prepare_plot, close_plot


def boxplot(data, path=None, show=False, x="hour", y="e_kj_per_m2", order=None,
            hue=None, hue_order=None, scale_factor=1, ylim=None,
            absolute=False, whis=50, prefix="", ax_height=12, ax_width=16,
            format="pgf", fontsize=8, ylabel="auto", legend="auto"):
    """
    Plot and save boxplots.

    :param data: Time-indexed data frame containing the data to be plotted.
    :type data: pandas.core.frame.DataFrame
    :param path: Address to the directory where the plot will be saved. If
        None, the plot will not be saved.
    :type path: str or None, default: None
    :param show: If True, the plot will be presented on the screen.
    :type show: bool, default: False
    :param x: Determines how the data will be split. Options: None (no split),
        "hour", "month", "month/year".
    :type x: str, default: "hour"
    :raise ValueError: If x is not None, "hour", "month", nor "month/year".
    :param y: The name of the column containing the data to be plotted.
    :type y: str, default: "e_kj_per_m2"
    :param order: Order to plot the categorical levels in, otherwise the levels
        are inferred from the data objects.
    :type order: list, default: None
    :param hue: Optional categorical variables in the data to use for splitting
        the data into color subsets.
    :type hue: str or None, default: None
    :param hue_order: Order for the levels of the hue variable in the plot.
    :type hue_order: list, default: None
    :param scale_factor: Scale factor by which the data will be divided.
    :type scale_factor: float, default: 1.0
    :param ylim: matplotlib.pyplot ylim parameter.'
    :type ylim: float or None, default: None
    :param absolute: If True the absolute value of data will be plotted.
    :type absolute: bool, default: False
    :param whis: The whiskers factor of the boxplot.
    :type whis: int, default: 50
    :param prefix: String to be prepended to the filename of the saved plot.
    :type prefix: str, default: ""
    :param ax_height: The height of each subplot.
    :type ax_height: int or float, default: 12
    :param ax_width: The width of each subplot.
    :type ax_width: int or float, default: 16
    :param format: Specifies the file format for saving the plot.
    :type format: str, default: "pgf"
    :param fontsize: The font size of the axis labels and title.
    :type fontsize: int, default: 8
    :param ylabel: The label text to attribute to the y-axis. If "auto",
        will use the column name.
    :type ylabel: str or "auto", default: "auto"
    :param legend: If "auto", will decide automatically if the legend should
        be displayed. If "full", always display legend. If "brief", only
        display legend if hue is used.
    :type legend: str, default: "auto"

    :example:
        >>> import os
        >>> import pandas as pd
        >>> import numpy as np
        >>> from PIL import Image, ImageChops
        >>> from capitu import dap
        >>> from capitu.dap.plotter.boxplot import boxplot
        >>> np.random.seed(42)
        >>> dap.set_language("english")
        >>> data = pd.DataFrame({
        ...    'temperature_celsius': np.random.uniform(-10, 30, 100),
        ...    'season': np.random.choice(
        ...        ['Spring', 'Summer', 'Autumn', 'Winter'], 100)},
        ...    index=pd.date_range(start='2022-01-01', periods=100, freq='D'))
        >>> boxplot(data, path="./", show=False, x="month/year",
        ...         y="temperature_celsius", hue="season",
        ...         hue_order=["Summer", "Autumn", "Winter", "Spring"],
        ...         scale_factor=1.0, ylim=None, absolute=False, whis=50,
        ...         prefix="", ax_height=10, ax_width=15, format="png",
        ...         fontsize=10, ylabel="Random Temperature (°C)")
        >>> target = Image.open('./box_temperature_celsius.png')
        >>> template = Image.open(
        ...     '../capitu/datastore/doctest/box_temperature_celsius.png')
        >>> diff = ImageChops.difference(target, template)
        >>> diff.getbbox() is None
        True
        >>> os.remove('box_temperature_celsius.png')
    """

    # Prepare the plot setting a nice theme, scaling the data.
    data, x_values, colors = prepare_plot(data, y, x, scale_factor, absolute,
                                          hue)

    # Boxplotting.
    if hue is not None:
        ax = sns.boxplot(
            data=data, x=x_values, y=y, palette=colors, fliersize=1, whis=whis,
            hue=hue, hue_order=hue_order, legend=legend)
    else:
        ax = sns.boxplot(
            data=data, x=x_values, y=y, color=colors[0], fliersize=1,
            whis=whis, order=order, hue_order=hue_order, legend=legend)

    # Closing.
    close_plot(x, y, ylim, None, None, path, show, format,
               filename=prefix+"box_"+y, ax=ax, fontsize=fontsize,
               ylabel=ylabel, ax_height=ax_height, ax_width=ax_width)
