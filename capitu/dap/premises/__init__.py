#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
The premises module loads important information utilized in the models, such
as data from equipment datasheets and energy market data.

These premises consist of JSON files or Python modules containing the
necessary information."
"""

name = "premises"
__author__ = "Daniel Campos Pompermayer [gitlab.com/daniel_pompermayer]"
