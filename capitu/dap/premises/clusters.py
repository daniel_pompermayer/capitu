#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
The clusters module provides functions and variables for clustering consumption
and generation data according to preconfigured clusters.

    :variables:
        off_peak_season_clusters (List[str]): list containing names of
        season clusters occurring in off-peak time band.

    :example:
        >>> from capitu.dap.premises.clusters import (
        ...     off_peak_season_clusters)
        >>> off_peak_season_clusters
        ['vacation', 'holidays']
"""


import logging
import pandas as pd
from pathlib import Path
from .holidays import holidays, vacation
from ..parser import datetime_to_season
from ..plotter.labels import table_labels

off_peak_season_clusters = ["vacation", "holidays"]


def get_season_clusters(year=2023, variable="both", path=None):
    """
    Get the season clusters for a given year and variable.

    :param year: Year for which season clusters are obtained.
    :type year: int, default 2023
    :param variable: Variable to mark clusters for. If "both", mark clusters
        for consumption and generation. If "consumption", mark clusters for
        consumption. If "generation", mark clusters for generation.
    :type variable: str, default "both"
    :param path: address to the directory where the table of clusters will be
        saved. If None, it will not be saved.
    :type path: str or None, default: None
    :return: Series having each season cluster along with its size.
    :rtype: pandas.Series

    :example:
        >>> import os
        >>> import filecmp
        >>> import pandas as pd
        >>> from capitu.dap import set_language
        >>> from capitu.dap.premises.clusters import get_season_clusters
        >>> set_language("english")
        >>> get_season_clusters(year=2023, variable="both", path="./")
        cluster
        not_summer/holidays       88
        not_summer/not_winter    118
        not_summer/vacation       18
        not_summer/winter         52
        summer/holidays           31
        summer/not_winter         15
        summer/vacation           43
        Name: date, dtype: int64
        >>> filecmp.cmp('./clusters.tex',
        ...             '../capitu/datastore/doctest/clusters.tex')
        True
        >>> os.remove('clusters.tex')
    """

    # Mark consumption clusters of the days of year.
    days = pd.DataFrame({"date": pd.date_range(
        start="01-01-" + str(year), end="12-31-" + str(year), freq="1D")})

    # Label the clusters of the year.
    days["cluster"] = mark_season_clusters(days.date.dt, variable)

    # Size of each cluster.
    clusters = days.groupby("cluster").count().date

    # Check whether user wants to save the table of clusters.
    if path is not None:

        # Get the clusters as columns and not as index.
        formated_clusters = clusters.reset_index()

        # Break the cluster column into generation and consumption columns.
        formated_clusters[["generation", "consumption"]] = (
            formated_clusters.cluster.str.split("/", expand=True))

        # Get rid of the original cluster colum.
        formated_clusters = formated_clusters[[
            "generation", "consumption", "date"]].rename(
                {"date": "number_of_days"}, axis=1)

        # Format the column names and values.
        formated_clusters = formated_clusters.rename(columns=table_labels)
        formated_clusters = formated_clusters.replace(table_labels)

        # Save it.
        formated_clusters.to_latex(
            Path(path)/"clusters.tex",
            index=False, column_format="ccc")

    return clusters


def mark_season_clusters(dates, variable="both"):

    """
    Mark clusters based on the seasons and variable (consumption or
    generation).

    :param dates: Dates to mark clusters for.
    :type dates: pandas.DatetimeIndex
    :param variable: Variable to mark clusters for. If "both", mark clusters
        for consumption and generation. If "consumption", mark clusters for
        consumption. If "generation", mark clusters for generation.
    :type variable: str, default "both"
    :return: Array containing marked clusters.
    :rtype: numpy.ndarray

    :example:
        >>> import pandas as pd
        >>> from capitu.dap.premises.clusters import mark_season_clusters
        >>> dates = pd.date_range(start="2024-01-01", end="2024-12-31",
        ...     periods=10)
        >>> mark_season_clusters(dates, variable="both")
        array(['summer/not_winter', 'summer/holidays', 'not_summer/not_winter',
               'not_summer/not_winter', 'not_summer/not_winter',
               'not_summer/holidays', 'not_summer/holidays',
               'not_summer/not_winter', 'not_summer/not_winter',
               'summer/not_winter'], dtype=object)
    """

    logging.info("Starting process.")

    days = pd.DataFrame({"date": dates.strftime("%Y-%m-%d"),
                         "weekday": dates.weekday,
                         "season": datetime_to_season(dates)})

    if variable == "both" or variable == "consumption":
        days["consumption"] = ""
        days.loc[days.season == "winter", "consumption"] = "winter"
        days.loc[days.season != "winter", "consumption"] = "not_winter"
        days.loc[days.date.isin(vacation), "consumption"] = "vacation"
        days.loc[days.weekday >= 5, "consumption"] = "holidays"
        days.loc[days.date.isin(holidays), "consumption"] = "holidays"

    if variable == "both" or variable == "generation":
        days["generation"] = ""
        days.loc[days.season == "summer", "generation"] = "summer"
        days.loc[days.season != "summer", "generation"] = "not_summer"

    if variable == "both":
        days["cluster"] = (days.generation + "/" + days.consumption)
    else:
        days["cluster"] = days[variable]

    logging.info("Done.")

    return days.cluster.values
