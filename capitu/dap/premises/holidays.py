#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
The holidays contains two variables, 'holidays' and 'vacation', which store
lists of date strings representing holidays and vacation periods, respectively.

    :variables:
        holidays ([str]): A list of date strings representing holidays.

        vacation ([str]): A list of date strings representing vacation periods.

    :example:
        >>> from capitu.dap.premises.holidays import holidays, vacation
        >>> print(holidays[-3:])
        ['2023-11-03', '2023-11-15', '2023-12-25']
        >>> print(vacation[:3])
        ['2015-01-01', '2015-01-02', '2015-01-03']
"""

import pandas as pd


holidays = ["2015-01-01", "2015-02-16", "2015-02-17", "2015-02-18",
            "2015-04-03", "2015-04-13", "2015-04-20", "2015-04-21",
            "2015-05-01", "2015-06-04", "2015-06-05", "2015-09-07",
            "2015-09-08", "2015-10-12", "2015-10-30", "2015-11-02",
            "2015-12-25", "2016-01-01", "2016-02-08", "2016-02-09",
            "2016-02-10", "2016-03-25", "2015-03-26", "2016-04-04",
            "2016-04-21", "2016-04-22", "2016-04-23", "2016-09-07",
            "2016-09-08", "2016-09-09", "2016-09-10", "2016-10-12",
            "2016-10-28", "2016-10-29", "2016-11-02", "2016-11-14",
            "2016-11-15", "2016-12-25", "2017-02-27", "2017-02-28",
            "2017-03-01", "2017-04-14", "2017-04-15", "2017-04-21",
            "2017-04-22", "2017-04-24", "2017-05-01", "2017-06-15",
            "2017-06-16", "2017-06-17", "2017-09-07", "2017-09-08",
            "2017-09-09", "2017-10-12", "2017-10-13", "2017-10-13",
            "2017-10-28", "2017-11-02", "2017-11-03", "2017-11-03",
            "2017-11-04", "2017-11-15", "2017-12-25", "2023-01-01",
            "2023-02-20", "2023-02-21", "2023-02-22", "2023-04-07",
            "2023-04-17", "2023-04-21", "2023-05-01", "2023-06-08",
            "2023-09-07", "2023-10-12", "2023-10-28", "2023-11-02",
            "2023-11-03", "2023-11-15", "2023-12-25"]

vacation = (pd.date_range(start="01-01-2015", end="03-02-2015",
                          freq="1D").strftime("%Y-%m-%d").to_list()
            + pd.date_range(start="07-07-2015", end="08-02-2015",
                            freq="1D").strftime("%Y-%m-%d").to_list()
            + pd.date_range(start="12-04-2015", end="02-29-2016",
                            freq="1D").strftime("%Y-%m-%d").to_list()
            + pd.date_range(start="07-05-2016", end="07-31-2016",
                            freq="1D").strftime("%Y-%m-%d").to_list()
            + pd.date_range(start="12-08-2016", end="01-22-2017",
                            freq="1D").strftime("%Y-%m-%d").to_list()
            + pd.date_range(start="03-11-2017", end="03-27-2017",
                            freq="1D").strftime("%Y-%m-%d").to_list()
            + pd.date_range(start="07-31-2017", end="08-23-2017",
                            freq="1D").strftime("%Y-%m-%d").to_list()
            + pd.date_range(start="12-30-2017", end="12-31-2017",
                            freq="1D").strftime("%Y-%m-%d").to_list()
            + pd.date_range(start="01-01-2023", end="01-22-2023",
                            freq="1D").strftime("%Y-%m-%d").to_list()
            + pd.date_range(start="02-11-2023", end="03-19-2023",
                            freq="1D").strftime("%Y-%m-%d").to_list()
            + pd.date_range(start="07-22-2023", end="08-13-2023",
                            freq="1D").strftime("%Y-%m-%d").to_list()
            + pd.date_range(start="12-17-2023", end="12-31-2023",
                            freq="1D").strftime("%Y-%m-%d").to_list())
