"""
The panel information module handles premises related to the information of
the employed PV panels. It loads this information from a JSON file.

    :variables:
        panels (pandas.core.frame.DataFrame): A DataFrame containing the
        information of the employed PV panels loaded from a JSON file.

    :example:
        >>> from capitu.dap.premises.panels import panels
        >>> panels.index
        Index(['efficiency', 'power_kw', 'area_m2', 'ptc',
               'operation_temperature_celsius', 'NOCT_celsius',
               'std_temperature_celsius'],
              dtype='object')
        >>> panels.columns.values
        array(['JA Solar JAM60S01-315/PR', 'JA Solar JAM60S01-310/PR'],
              dtype=object)
        >>> panels["JA Solar JAM60S01-315/PR"]
        efficiency                        0.19300
        power_kw                          0.31500
        area_m2                           1.48678
        ptc                               0.38000
        operation_temperature_celsius    48.40000
        NOCT_celsius                     45.00000
        std_temperature_celsius          25.00000
        Name: JA Solar JAM60S01-315/PR, dtype: float64
        >>> panels["JA Solar JAM60S01-310/PR"]
        efficiency                        0.19000
        power_kw                          0.31000
        area_m2                           1.48678
        ptc                               0.38000
        operation_temperature_celsius    48.40000
        NOCT_celsius                     45.00000
        std_temperature_celsius          25.00000
        Name: JA Solar JAM60S01-310/PR, dtype: float64
"""

import pandas as pd
from pathlib import Path


panels = pd.read_json(Path(__file__).parent / "panels.json")
