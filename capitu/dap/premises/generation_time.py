"""
The generation time information module handles premises related to the working
hours of the PV generators.

    :variables:
        generation_time (dict): A dictionary containing the working hours of
        the PV generators.

    :example:
        >>> import pandas as pd
        >>> import numpy as np
        >>> from capitu.dap.premises.generation_time import generation_time
        >>> generation_time
        {'start_time': '06:00', 'end_time': '19:00', 'inclusive': 'right'}
        >>> index = pd.date_range(start='2022-01-01', end='2022-01-05',
        ...                       freq='H')
        >>> np.random.seed(42)
        >>> data = pd.DataFrame({'value': np.random.randn(len(index))},
        ...                     index=index)
        >>> data
                                value
        2022-01-01 00:00:00  0.496714
        2022-01-01 01:00:00 -0.138264
        2022-01-01 02:00:00  0.647689
        2022-01-01 03:00:00  1.523030
        2022-01-01 04:00:00 -0.234153
        ...                       ...
        2022-01-04 20:00:00 -0.702053
        2022-01-04 21:00:00 -0.327662
        2022-01-04 22:00:00 -0.392108
        2022-01-04 23:00:00 -1.463515
        2022-01-05 00:00:00  0.296120
        <BLANKLINE>
        [97 rows x 1 columns]
        >>> processed_data = data.between_time(**generation_time)
        >>> processed_data.head()
                                value
        2022-01-01 07:00:00  0.767435
        2022-01-01 08:00:00 -0.469474
        2022-01-01 09:00:00  0.542560
        2022-01-01 10:00:00 -0.463418
        2022-01-01 11:00:00 -0.465730
        >>> processed_data.tail()
                                value
        2022-01-04 15:00:00  0.328751
        2022-01-04 16:00:00 -0.529760
        2022-01-04 17:00:00  0.513267
        2022-01-04 18:00:00  0.097078
        2022-01-04 19:00:00  0.968645
"""

generation_time = {"start_time": "06:00",
                   "end_time": "19:00",
                   "inclusive": "right"}
