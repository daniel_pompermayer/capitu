#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
The market module brings together important information of the electrical
market, which are key for various calculations and analyses within the
application. The information of the electrical market are either loaded from
a json file (the applied fees) or hardcoded (the ideal power factor value,
the peak hours, among others).

    :variables:
        brazilian_fees (pandas.core.frame.DataFrame): DataFrame containing the
            applied fees in the Brazilian electrical market.

        brazilian_ideal_pf (float): The ideal power factor value for the
            Brazilian electrical market.

        peak_hours ([int]): A list of peak hours in the Brazilian electrical
            market.

        night (range): Range representing nighttime hours in the Brazilian
            electrical market.

        day (range): Range representing daytime hours in the Brazilian
            electrical market.

    :example:
        >>> from capitu.dap.premises.market import (
        ...     brazilian_fees, brazilian_ideal_pf, peak_hours, night, day)
        >>> brazilian_fees
                                      erex     peak  off_peak
        2014-08-07 00:00:00-03:00  0.19841  1.14954   0.22040
        2015-03-02 00:00:00-03:00  0.23044  1.24966   0.30331
        2015-08-07 00:00:00-03:00  0.24481  1.27149   0.31785
        2016-08-07 00:00:00-03:00  0.23361  1.49866   0.29581
        2017-08-07 00:00:00-03:00  0.26467  1.60560   0.30827
        2018-08-07 00:00:00-03:00  0.30331  1.76160   0.37029
        2019-08-07 00:00:00-03:00  0.25141  1.40252   0.32115
        2020-08-07 00:00:00-03:00  0.26292  1.48417   0.34051
        2021-08-07 00:00:00-03:00  0.27926  1.73795   0.34886
        2022-08-07 00:00:00-03:00  0.27703  1.66260   0.39824
        2023-08-07 00:00:00-03:00  0.30560  1.72586   0.41158
        >>> brazilian_ideal_pf
        0.92
        >>> peak_hours
        [19, 20, 21]
        >>> night
        range(0, 6)
        >>> day
        range(6, 24)
"""

import pandas as pd
from pathlib import Path


# https://www.aneel.gov.br/resultado-dos-processos-tarifarios-de-distribuicao
brazilian_fees = pd.read_json(Path(__file__).parent / "brazilian_fees.json").T
brazilian_fees = brazilian_fees.tz_localize("America/Sao_Paulo")

brazilian_ideal_pf = 0.92

peak_hours = [19, 20, 21]
night = range(0, 6)
day = range(6, 24)
