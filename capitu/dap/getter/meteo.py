#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
The meteo module offers functions to import and preprocess meteorological data.
"""

import pytz
import numpy as np
import pandas as pd
import datetime as dt
from pathlib import Path
from ..parser import meteo_date
from ..premises.generation_time import generation_time


def get(start_date, end_date, filepath=None, api=None, station=None,
        data_tz="Etc/UTC", target_tz="America/Sao_Paulo", localize_tz=False,
        generation_time_only=False):
    """
    Get meteorological data either from an automatic meteorological station
    of the Brazilian National Institute of Meteorology (INMET) or from
    stored data.

    .. warning::
        Get data using api might not work because of the INMET API policy.

    :param start_date: date of the begin of the interval of data.
    :type start_date: str, format: %Y-%m-%d
    :param end_date: date of the end of the interval of data.
    :type end_date: str, format: %Y-%m-%d
    :param filepath: String having the path and the name of the file having the
                     meteorological data. If filepath is not None, api will not
                     be used.
    :type filepath: str, default None
    :param api: URL of the INMET's API.
    :type api: str, default None
    :param station: name of the INMET automatic meteorological station from
        which the data will be got.
    :type station: str, default None
    :param data_tz: name of the pytz timezone where the station is located.
    :type data_tz: str, default "Etc/UTC".
    :param target_tz: name of the target timezone to which the data will be
                      converted.
    :type target_tz: str, default "America/Sao_Paulo".
    :param localize_tz: If True, timezone is localized. If False, timezone is
        not localized.
    :type localize_tz: bool, default False.
    :param generation_time_only: If True, only data registered in the
        generators working hours will be kept.
    :type generation_time_only: bool, default: True
    :return: time indexed pandas DataFrame with the meteorological data.
    :rtype: pandas.core.frame.DataFrame

    :example:
        >>> from capitu.dap.getter.meteo import get as get_meteo
        >>> start_date = '2015-01-01'
        >>> end_date = '2016-12-31'
        >>> filepath = '../capitu/datastore/meteo.csv'
        >>> meteo = get_meteo(start_date, end_date, filepath,
        ...                   data_tz="Etc/UTC", target_tz="America/Sao_Paulo",
        ...                   localize_tz=True)
        >>> type(meteo)
        <class 'pandas.core.frame.DataFrame'>
        >>> meteo.shape
        (16924, 2)
        >>> meteo.temperature_celsius.mean()
        24.952133065469155
        >>> meteo.e_kj_per_m2.mean()
        769.2944410895769
        >>> meteo.index.to_list()[0]
        Timestamp('2015-01-01 00:00:00-0200', tz='America/Sao_Paulo')
        >>> meteo.index.to_list()[-1]
        Timestamp('2016-12-31 23:00:00-0200', tz='America/Sao_Paulo')
    """

    # Get data in datetime format. It will be usefull further.
    start_time = pd.to_datetime(start_date, format="%Y-%m-%d")
    end_time = pd.to_datetime(end_date + " 23:59", format="%Y-%m-%d %H:%M")

    # Localize timezones.
    data_tz = pytz.timezone(data_tz)
    target_tz = pytz.timezone(target_tz)
    start_time = target_tz.localize(start_time)
    end_time = target_tz.localize(end_time)

    # Convert timezones.
    start_time = start_time.astimezone(data_tz)
    end_time = end_time.astimezone(data_tz)

    # Make times timezone-naive to avoid slicing troubles.
    start_time = start_time.replace(tzinfo=None)
    end_time = end_time.replace(tzinfo=None)

    # Check wether user wants to read data from a stored file or using the
    # INMET API.
    if filepath is not None:
        meteo = get_stored_data(start_time, end_time, filepath)

    else:
        meteo = get_inmet_data(start_time, end_time, station, api)

    # If user wants so, convert station timezone to target timezone.
    if localize_tz:
        meteo = meteo.tz_localize(data_tz)
        meteo = meteo.tz_convert(target_tz)

    # Negative radiations are not a valid number, replace by zero.
    meteo.loc[meteo.e_kj_per_m2 < 0, "e_kj_per_m2"] = 0

    # Check whether the user wants to keep only data from the time interval
    # when solar generation is expected.
    if generation_time_only:
        meteo = meteo.between_time(**generation_time)

    return meteo


def get_stored_data(start_date, end_date, filepath):
    """
    Get meteorological data from stored data.

    :param start_date: date of the begin of the interval of data.
    :type start_date: str, format: %Y-%m-%d
    :param end_date: date of the end of the interval of data.
    :type end_date: str, format: %Y-%m-%d
    :param filepath: String having the path and the name of the file having the
                     meteorological data.
    :type filepath: str, default None
    :return: time indexed pandas DataFrame with the meteorological data.
    :rtype: pandas.core.frame.DataFrame

    :example:
        >>> from capitu.dap.getter.meteo import get_stored_data
        >>> start_date = '2015-01-01'
        >>> end_date = '2016-12-31'
        >>> filepath = '../capitu/datastore/meteo.csv'
        >>> meteo = get_stored_data(start_date, end_date, filepath)
        >>> type(meteo)
        <class 'pandas.core.frame.DataFrame'>
        >>> meteo.shape
        (16924, 2)
        >>> meteo.temperature_celsius.mean()
        24.952038525171353
        >>> meteo.e_kj_per_m2.mean()
        767.9159955093357
        >>> meteo.index.to_list()[0]
        Timestamp('2015-01-01 00:00:00')
        >>> meteo.index.to_list()[-1]
        Timestamp('2016-12-31 23:00:00')
    """

    # Using Path avoid OS related issues.
    meteo_file = Path(filepath)

    # Read data.
    meteo = pd.read_csv(meteo_file, sep=";", decimal=",", header=0,
                        names=["year", "month", "day", "hour",
                               "temperature_celsius", "e_kj_per_m2"])

    # Convert date columns.
    meteo["date"] = pd.to_datetime(meteo[["year", "month", "day", "hour"]])
    meteo = meteo.set_index("date")

    # Drop duplicated entries.
    meteo = meteo[~meteo.index.duplicated(keep="first")]

    # Filter data to the selected interval.
    meteo = meteo.sort_index()[start_date:end_date]

    return meteo[["temperature_celsius", "e_kj_per_m2"]]


def get_inmet_data(start_date, end_date, station="A612",
                   api="https://apitempo.inmet.gov.br/estacao/"):
    """
    Read data from the automatic meteorological stations of the Brazilian
    National Institute of Meteorology (INMET).

    .. warning::
        This function might not work because of the INMET API policy.

    :param start_date: date of the begin of the interval of data.
    :type start_date: str, format: %Y-%m-%d
    :param end_date: date of the end of the interval of data.
    :type end_date: str, format: %Y-%m-%d
    :param station: name of the INMET automatic meteorological station from
                    which the data will be got.
    :type station: str, default "A612"
    :param api: URL of the INMET's API.
    :type api: str, default "https://apitempo.inmet.gov.br/estacao/"
    :return: time indexed pandas DataFrame with the meteorological data.
    :rtype: pandas.core.frame.DataFrame

    :example:
        .. code-block:: python

            start_date = '2023-01-01'
            end_date = '2023-01-10'
            station = 'A612'
            api = 'https://apitempo.inmet.gov.br/estacao/'
            get_inmet_data(start_date, end_date, station, api)
    """

    raise RuntimeWarning("This function might not work because of the INMET"
                         + " API policy.")

    # INMET's API doesn't allow querying data from a date range greater than a
    # year. So, I'll need to create an array of annual intervals.
    dates = pd.date_range(start=start_date, end=end_date, freq="360D",
                          inclusive="left").to_pydatetime()

    # Pandas date_range doesn't includes the end date.
    end_date = pd.to_datetime(end_date, format="%Y-%m-%d")
    dates = np.append(dates, end_date) if end_date not in dates else dates

    # Zip the dates array with itself shifted, so I'll get the boundaries of
    # the annual intervals.
    intervals = zip(dates, dates[1:])

    # Empty pandas data frame for storing the meteorological data.
    meteo = pd.DataFrame()

    for interval in intervals:

        # Convert time to string in the API required format.
        start_interval = dt.datetime.strftime(interval[0], "%Y-%m-%d")
        end_interval = dt.datetime.strftime(interval[-1], "%Y-%m-%d")

        # Compose API entry.
        api_entry = api + start_interval + "/" + end_interval + "/" + station

        # Get data.
        meteo = pd.concat([meteo, pd.read_json(api_entry)])

    # Create datetime column from the columns of date and hour.
    hour = (meteo.HR_MEDICAO/100).astype(int).astype(str)
    meteo["date"] = (meteo.DT_MEDICAO + " " + hour).apply(meteo_date)
    meteo = meteo.set_index("date")

    # Keep only usefull columns and rename it.
    meteo = meteo[["RAD_GLO", "TEM_INS"]]
    meteo = meteo.rename(
        {"RAD_GLO": "e_kj_per_m2", "TEM_INS": "temperature_celsius"}, axis=1)

    # Drop duplicated entries.
    meteo = meteo[~meteo.index.duplicated(keep="first")]

    return meteo
