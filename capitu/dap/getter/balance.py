#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
The balance module offers functions to import and preprocess together the
consumption and the generation data.
"""

import logging
import pandas as pd
from .meteo import get as get_meteo
from .inverters import get as get_inverters
from .consumption import get as get_consumption
from ..estimator.radiation_method import estimate_generation
from ..premises.generation_time import generation_time
from ..plotter.boxplot import boxplot
from ..power_factor.compute import compute as compute_pf


def get(start_date, end_date, consumption_file, inverters_file, meteo_file,
        extra_kvar_c=0, extra_kvar_i=0, api=None, station=None, campus="G",
        inverters_date=None, void_sn="ignore", compute_temperature=False,
        uplimit="nominal", coherent=False, consumption_agg=None,
        hour_offset=True, generation_time_only=False, data_tz="Etc/UTC",
        target_tz="America/Sao_Paulo", localize_tz=True, show=False,
        image_export_path=None, ax_height=12, ax_width=16, fontsize=8,
        format="pgf"):
    """
    Read a file having the available consumption data of Ufes' campuses and get
    meteorological data either from an automatic meteorological station of the
    Brazilian National Institute of Meteorology (INMET) or from stored data.

    .. warning::
        Get data using api might not work because of the INMET API policy.

    :param start_date: date of the begin of the interval of data.
    :type start_date: str, format: %Y-%m-%d
    :param end_date: date of the end of the interval of data.
    :type end_date: str, format: %Y-%m-%d
    :param consumption_file: Path to the consumption file.
    :type consumption_file: str
    :param inverters_file: Path to the inverters file.
    :type inverters_file: str
    :param meteo_file: String having the path and the name of the file having
        the meteorological data. If filepath is not None, api will not be used.
    :type meteo_file: str
    :param extra_kvar_c: Additional capacitive reactive energy.
    :type extra_kvar_c: float, default: 0
    :param extra_kvar_i: Additional inductive reactive energy.
    :type extra_kvar_i: float, default: 0
    :param api: URL of the INMET's API.
    :type api: str, default None
    :param station: name of the INMET automatic meteorological station from
        which the data will be got.
    :type station: str, default None
    :param campus: First letter of the name of the Ufes campus where the
        desired inverters are installed.
    :type campus: str, default: "G"
    :param inverters_date: If specified, only inverters registered and not
        expired by this date will be considered.
    :type inverters_date: str, format: "%Y-%m-%d", default: None
    :param void_sn: Specifies the action for inverters with no serial number.
        If "drop", inverters without serial numbers are dropped. Otherwise,
        no action is taken.
    :type void_sn: str, default: "ignore"
    :param compute_temperature: If True, compute the operation temperature; if
        False, just use a prefixed operation temperature.
    :type compute_temperature: bool, default: False
    :param uplimit: If “nominal”, uplimit the generated energy with the nominal
        power value; if None, do not uplimit the generated energy.
    :type uplimit: str or None, default: "nominal"
    :raise KeyError: If uplimit is not None neither “nominal”.
    :param coherent: if True, estimate generation only for registered and not
       expirated inverters; if False do not care about it.
    :type coherent: bool, default: False
    :param consumption_agg: If None, no aggregation is performed. If "hour",
        the consumption file is aggregated by hour. If "month", the consumption
        file is aggregated by month.
    :type consumption_agg: str, default: None
    :param hour_offset: If True and aggregation is set to "hour",  1 will be
        sumed up to the hour value of the registers, so the registered energy
        is related to the past hour interval. If aggregation is not "hour",
        this parameter doesn't take any effect.
    :type hour_offset: bool, default: True
    :param generation_time_only: If True, only data registered in the
        generators working hours will be kept.
    :type generation_time_only: bool, default: True
    :raise KeyError: If consumption_agg is not None, "hour", or "month".
    :param data_tz: name of the pytz timezone where the meteorological station
        is located.
    :type data_tz: str, default "Etc/UTC".
    :param target_tz: Name of the target pytz timezone.
    :type target_tz: str, default "America/Sao_Paulo".
    :param localize_tz: If True, timezone is localized. If False, timezone is
        not localized.
    :type localize_tz: bool, default True.
    :param show: If True, a boxplot of the meteorological data and of the
        consumption data  will be presented on the screen.
    :type show: bool, default: False
    :param image_export_path: Address to the directory where the plots will be
        saved. If None, the plot will not be saved.
    :type image_export_path: str or None, default: None
    :param ax_height: The height of each subplot.
    :type ax_height: int or float, default: 12
    :param ax_width: The width of each subplot.
    :type ax_width: int or float, default: 16
    :param fontsize: The font size of the axis labels and title.
    :type fontsize: int, default: 8
    :param format: Specifies the file format for saving the plot.
    :type format: str, default: "pgf"
    :return: time indexed pandas DataFrame with the consumption data.
    :rtype: pandas.core.frame.DataFrame

    :example:
        >>> import os
        >>> import pandas as pd
        >>> from PIL import Image, ImageChops
        >>> from capitu import dap
        >>> from capitu.dap.getter.balance import get as get_balance
        >>> start_date = '2015-03-01'
        >>> end_date = '2015-05-31'
        >>> meteo_file = '../capitu/datastore/meteo.csv'
        >>> consumption_file = '../capitu/datastore/consumption.csv'
        >>> inverters_file = '../capitu/datastore/inverters_v1.2.0.csv'
        >>> dap.set_language("english")
        >>> balance = get_balance(
        ...     start_date, end_date, consumption_file, inverters_file,
        ...     meteo_file, extra_kvar_c=300, extra_kvar_i=0, campus="G",
        ...     inverters_date="2023-01-01", void_sn="drop",
        ...     consumption_agg="hour", data_tz="Etc/UTC",
        ...     target_tz="America/Sao_Paulo", localize_tz=True, show=False,
        ...     image_export_path="./", format="png")
        >>> balance[["gen_energy_kwh", "time_band",
        ...          "pf"]] # doctest: +NORMALIZE_WHITESPACE
                                   gen_energy_kwh time_band        pf
        2015-03-01 00:00:00-03:00        0.000000  off_peak  0.942339
        2015-03-01 01:00:00-03:00        0.000000  off_peak  0.944372
        2015-03-01 02:00:00-03:00        0.000000  off_peak  0.938039
        2015-03-01 03:00:00-03:00        0.000000  off_peak  0.940558
        2015-03-01 04:00:00-03:00        0.000000  off_peak  0.952493
        ...                                   ...       ...       ...
        2015-05-24 17:00:00-03:00      145.081014  off_peak  0.976306
        2015-05-24 18:00:00-03:00        0.000000  off_peak  0.965078
        2015-05-24 19:00:00-03:00        0.000000  off_peak  0.953755
        2015-05-24 20:00:00-03:00        0.000000  off_peak  0.959086
        2015-05-24 21:00:00-03:00        0.000000  off_peak  0.961629
        <BLANKLINE>
        [2038 rows x 3 columns]
        >>> balance[balance.weekend].pf.mean()
        0.9497072246573303
        >>> balance[balance.vacation].load_kwh.mean()
        1366.2833333333335
        >>> balance[balance.holidays].load_kvarh.mean()
        283.6590277777778
        >>> balance[balance.business].gen_energy_kwh.mean()
        762.117298013315
        >>> target = Image.open('./box_e_kj_per_m2.png')
        >>> template = Image.open(
        ...     '../capitu/datastore/doctest/box_e_kj_per_m2.png')
        >>> diff = ImageChops.difference(target, template)
        >>> diff.getbbox() is None
        True
        >>> os.remove('./box_e_kj_per_m2.png')
        >>> target = Image.open('./box_gen_energy_kwh.png')
        >>> template = Image.open(
        ...     '../capitu/datastore/doctest/box_gen_energy_kwh.png')
        >>> diff = ImageChops.difference(target, template)
        >>> diff.getbbox() is None
        True
        >>> os.remove('./box_gen_energy_kwh.png')
        >>> target = Image.open('./box_load_kwh.png')
        >>> template = Image.open(
        ...     '../capitu/datastore/doctest/box_load_kwh.png')
        >>> diff = ImageChops.difference(target, template)
        >>> diff.getbbox() is None
        True
        >>> os.remove('./box_load_kwh.png')
        >>> target = Image.open('./box_load_kvarh.png')
        >>> template = Image.open(
        ...     '../capitu/datastore/doctest/box_load_kvarh.png')
        >>> diff = ImageChops.difference(target, template)
        >>> diff.getbbox() is None
        True
        >>> os.remove('./box_load_kvarh.png')
    """

    logging.info("Starting process.")

    # Get meteorological data.
    meteo = get_meteo(start_date, end_date, meteo_file, api, station, data_tz,
                      target_tz, localize_tz, generation_time_only)

    # Plot meteorological data.
    if show or (image_export_path is not None):
        boxplot(meteo, path=image_export_path, show=show, x="hour",
                y="e_kj_per_m2", scale_factor=1000, ylim=None, absolute=False,
                whis=1.5, ax_height=ax_height, ax_width=ax_width,
                format=format, fontsize=fontsize)

        if compute_temperature:
            boxplot(meteo, path=image_export_path, show=show, x="hour",
                    y="temperature_celsius", scale_factor=1, ylim=None,
                    absolute=False, whis=1.5, ax_height=ax_height,
                    ax_width=ax_width, format=format, fontsize=fontsize)

    logging.info("Got meteorological data.")

    # Get inverters data frame.
    inverters = get_inverters(inverters_file, campus, inverters_date, void_sn)

    # Estimate the generation.
    generation = estimate_generation(meteo, inverters, compute_temperature,
                                     uplimit, coherent)
    generation = generation.groupby(
        pd.Grouper(level="date", freq="h")).sum(numeric_only=True)

    # Check whether I shoud keep only the data registered in generation time.
    if generation_time_only:
        generation = generation.between_time(**generation_time)

    # Plot generation data.
    # In the original article, I mishandled daylight saving time. Because of
    # that, the boxplot will differ slightly.
    if show or (image_export_path is not None):
        boxplot(generation, path=image_export_path, show=show, x="hour",
                y="gen_energy_kwh", scale_factor=1000, ylim=None,
                absolute=False, whis=1.5, ax_height=ax_height,
                ax_width=ax_width, format=format, fontsize=fontsize)

    logging.info("Generation estimated.")

    # Get the registered consumption.
    consumption = get_consumption(
        consumption_file, consumption_agg, hour_offset, start_date, end_date,
        target_tz, localize_tz, extra_kvar_i, extra_kvar_c,
        generation_time_only)

    # Compute the balance between the consumption and the generation.
    balance = generation.merge(consumption, left_index=True, right_index=True,
                               how="inner", suffixes=("_g", "_c"))
    balance.dropna(inplace=True)

    # Compute the new reactive power demand.
    balance["load_kvarh"] = balance.load_kvarh_i - balance.load_kvarh_c

    # Plot balance data.
    if show or (image_export_path is not None):
        boxplot(balance, path=image_export_path, show=show, x="hour",
                y="load_kwh", scale_factor=1000, ylim=None, absolute=False,
                whis=1.5, ax_height=ax_height, ax_width=ax_width,
                format=format, fontsize=fontsize)
        boxplot(balance, path=image_export_path, show=show, x="hour",
                y="load_kvarh", scale_factor=1000, ylim=None, absolute=False,
                whis=1.5, ax_height=ax_height, ax_width=ax_width,
                format=format, fontsize=fontsize)

    # Compute the load power factor.
    balance["pf"] = compute_pf(
        balance, active="load_kwh", reactive="load_kvarh")

    logging.info("Got the estimated balance.")

    return balance
