import logging
import pytz
import pandas as pd
import numpy as np
from pathlib import Path
from ..parser import gen_data_columns, gen_float_columns, gen_columns_agg
from ..parser import generation_date as parse_date
from ..premises.generation_time import generation_time
from ..premises.holidays import holidays
from ..premises.market import peak_hours


def get(filepath, start_date=None, end_date=None, data_tz=None, target_tz=None,
        localize_tz=False, columns=None, aggregation=None, hour_offset=True,
        generation_time_only=False):
    """
    Get generation data from stored data.

    :param start_date: date of the begin of the interval of data.
    :type start_date: str, format: %Y-%m-%d, default None
    :param end_date: date of the end of the interval of data.
    :type end_date: str, format: %Y-%m-%d, default None
    :param filepath: String having the path and the name of the file having the
                     generation data.
    :type filepath: str, default None
    :param data_tz: name of the pytz timezone where is localized the generation
                    data.
    :type data_tz: str, default None.
    :param target_tz: name of the target pytz timezone.
    :type target_tz: str, default None.
    :param localize_tz: If True, timezone is localized. If False, timezone is
        not localized.
    :type localize_tz: bool, default False.
    :param columns: List of the columns of the generation file that will be
                    returned. If None, all of the columns will be returned.
    :type columns: list, default None.
    :param aggregation: if None, no aggregation is performed; if "hour", the
        consumption file is aggregated by hour; if "month", the consumption
        file is aggregated by month.
    :type aggregation: string, default None.
    :param hour_offset: If True and aggregation is set to "hour",  1 will be
        sumed up to the hour value of the registers, so the registered energy
        is related to the past hour interval. If aggregation is not "hour",
        this parameter doesn't take any effect.
    :type hour_offset: bool, default: True
    :param generation_time_only: If True, only data registered in the
        generators working hours will be kept.
    :type generation_time_only: bool, default: True
    :return: time indexed pandas DataFrame with the generation data.
    :rtype: pandas.core.frame.DataFrame

    >>> from capitu.dap.getter.generation import get as get_generation
    >>> generation = get_generation(
    ...     "../capitu/datastore/test_generation.ftr", start_date="2022-10-3",
    ...     end_date="2022-10-10", data_tz=None, target_tz="America/Sao_Paulo",
    ...     localize_tz=True, columns=["Time", "serial_number", "Power(W)"],
    ...     aggregation="hour")
    >>> generation[["gen_energy_kwh", "p_w"]]  # doctest: +NORMALIZE_WHITESPACE
                               gen_energy_kwh           p_w
    2022-10-03 06:00:00-03:00        0.128630    167.913043
    2022-10-03 07:00:00-03:00        1.796770   1797.250000
    2022-10-03 08:00:00-03:00        2.966710   2981.383333
    2022-10-03 09:00:00-03:00        3.319655   3333.550000
    2022-10-03 10:00:00-03:00        4.392658   4386.183333
    ...                                   ...           ...
    2022-10-10 14:00:00-03:00       19.256550  19251.683333
    2022-10-10 15:00:00-03:00       14.937067  14914.983333
    2022-10-10 16:00:00-03:00        8.862737   8852.883333
    2022-10-10 17:00:00-03:00        3.245317   3240.800000
    2022-10-10 18:00:00-03:00        0.148319    159.303571
    <BLANKLINE>
    [196 rows x 2 columns]
    >>> list(generation.columns)
    ['gen_energy_kwh', 'time_band', 'serial_number', 'p_w', 'int_time']
    """

    logging.info("Starting process.")

    # Get data in datetime format. It will be usefull further.
    if start_date is None:
        start_time = None
    else:
        start_time = pd.to_datetime(start_date, format="%Y-%m-%d")

    if end_date is None:
        end_time = None
    else:
        end_time = pd.to_datetime(end_date + " 23:59", format="%Y-%m-%d %H:%M")

    # Localize and convert timezones.
    if target_tz is not None:
        target_tz = pytz.timezone(target_tz)
        if start_time is not None:
            start_time = target_tz.localize(start_time)
        if end_time is not None:
            end_time = target_tz.localize(end_time)

    if data_tz is not None:
        data_tz = pytz.timezone(data_tz)
        if start_time is not None:
            start_time = start_time.astimezone(data_tz)
        if end_time is not None:
            end_time = end_time.astimezone(data_tz)

    # Make times timezone-naive to avoid slicing troubles.
    if start_time is not None:
        start_time = start_time.replace(tzinfo=None)
    if end_time is not None:
        end_time = end_time.replace(tzinfo=None)

    logging.info("Got start and end time.")

    # Using Path avoid OS related issues.
    generation_file = Path(filepath)

    # Assure the power column is in columns list.
    if columns is not None:
        if "Power(W)" not in columns:
            columns.append("Power(W)")

    # Read data.
    generation = pd.read_feather(generation_file, columns=columns)

    logging.info("Data read.")

    # Rename the columns to appropriated names.
    generation = generation.rename(gen_data_columns, axis=1)

    logging.info("Columns renamed.")

    # Convert the float columns to float.
    float_columns = [column for column in gen_float_columns
                     if column in generation.columns]
    generation[float_columns] = generation[float_columns].astype(float)

    logging.info("Float columns converted.")

    # Convert date columns.
    generation["date"] = parse_date(generation.date)

    logging.info("Date column parsed.")

    # Drop duplicated entries.
    generation = generation[
        ~generation.duplicated(["date", "serial_number"], keep="first")]

    # Set the date column as the index of the data frame.
    generation = generation.set_index("date")

    # Filter data to the selected interval.
    if start_time is not None:
        generation = generation.sort_index()[start_time:]
    if end_time is not None:
        generation = generation.sort_index()[:end_time]

    logging.info("Got data.")

    # Compute the generated energy. For doing so, I'll need to group the data
    # using serial number and date so I'm not integrating different inverters
    # together or different days together.
    generation["int_time"] = generation.groupby(
        [generation.index.date, "serial_number"]).apply(
            lambda group: -group.reset_index().date.diff(-1)).values
    generation["int_time"] = generation.int_time.ffill()
    generation["int_time"] = (
        generation.int_time.dt.total_seconds().values / 3600)

    # Integrate data.
    generation["gen_energy_kwh"] = generation.p_w * generation.int_time / 1000

    logging.info("Got the generated energy.")

    # Set the time band (peak or off peak).
    generation["time_band"] = "off_peak"
    is_holiday = generation.index.strftime("%Y-%m-%d").isin(holidays)
    peak = np.logical_and(generation.index.hour.isin(peak_hours),
                          np.logical_not(is_holiday))
    generation.loc[peak, "time_band"] = "peak"

    logging.info("Time band set.")

    # If user wants, aggregate data.
    if aggregation is None:
        pass
    elif aggregation == "hour":

        # # I'm only implementing aggregation to the power column.
        # generation = generation[["serial_number", "p_w"]]

        # If I must use the hourly time interval upper limit as the time index,
        # then I must first move data to the lower limit, get them together and
        # then move then to upper limit again.
        if hour_offset:
            generation.index = generation.index - pd.to_timedelta(
                generation.int_time, unit="h")

        # Filter the agg dictionary to keep only the existing columns.
        filtered_agg = {"gen_energy_kwh": "sum", "time_band": "first"}
        for column in generation.columns.drop(["gen_energy_kwh", "time_band"]):
            filtered_agg[column] = gen_columns_agg[column]

        # Group data using serial number and date so I'm not integrating
        # different inverters together or different days together.
        aggregated = pd.DataFrame([])
        groups = generation.groupby([generation.index.date, "serial_number"])
        for index, group in groups:

            # Integrate data.
            integrated = group.groupby(group.index.hour).agg(filtered_agg)

            # Recompose the index as datetime.
            integrated.index = pd.to_datetime(
                str(index[0]) + " " + integrated.index.astype(str) + ":00:00",
                format="%Y-%m-%d %H:%M:%S")

            # The index hour must be toghether with the energy integrated
            # during the previous hour. When I've grouped the data, the index
            # hour goes toghether with the energy integrated in that same hour.
            # So, I must sum 1.
            if hour_offset:
                integrated.index = integrated.index + pd.Timedelta(hours=1)

            # Concat integrated data to the aggregated data frame.
            aggregated = pd.concat([aggregated, integrated])

        # Save the aggregated data frame.
        generation = aggregated

    elif aggregation == "time_band":

        # Filter the agg dictionary to keep only the existing columns.
        filtered_agg = {"gen_energy_kwh": "sum", "time_band": "first"}
        for column in generation.columns.drop(["gen_energy_kwh", "time_band"]):
            filtered_agg[column] = gen_columns_agg[column]

        # Group data using serial number and date so I'm not integrating
        # different inverters together or different days together.
        aggregated = pd.DataFrame([])
        groups = generation.groupby([generation.index.date, "serial_number"])
        for index, group in groups:

            # Integrate time band.
            integrated = group.groupby("time_band").agg(filtered_agg)

            # Set the date as the index.
            integrated["date"] = pd.to_datetime(
                index[0], format="%Y-%m-%d %H:%M:%S")
            integrated = integrated.set_index("date")

            # Concat integrated data to the aggregated data frame.
            aggregated = pd.concat([aggregated, integrated])

        # Save the aggregated data frame.
        generation = aggregated
    else:
        raise KeyError("Sorry. I don't know the chosen aggregation method.")

    # Check whether the user wants to keep only data from the time interval
    # when solar generation is expected.
    if generation_time_only:
        generation = generation.between_time(**generation_time)

        logging.info("Only generation time is kept.")

    # If user wants so, convert station timezone to target timezone.
    if localize_tz:
        if data_tz is not None:
            generation = generation.tz_localize(data_tz)
            generation = generation.tz_convert(target_tz)
        else:
            generation = generation.tz_localize(target_tz)

    return generation
