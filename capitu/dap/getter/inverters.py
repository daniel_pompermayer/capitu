#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
The inverters module offer a function to import and preprocess the Ufes'
inverters information.
"""


import pandas as pd
from pathlib import Path


def get(filepath, campus=None, inverters_date=None, void_sn="drop"):
    """Read a file containing information about Ufes' inverters.

    :param filepath: Path to the inverters file.
    :type filepath: str
    :param campus: First letter of the name of the Ufes campus where the
        desired inverters are installed.
    :type campus: str, default: None
    :param inverters_date: If specified, only inverters registered and not
        expired by this date will be considered.
    :type inverters_date: str, format: "%Y-%m-%d"
    :param void_sn: Specifies the action for inverters with no serial number.
        If "drop", inverters without serial numbers are dropped. Otherwise,
        no action is taken.
    :type void_sn: str, default: "drop"
    :return: time indexed pandas DataFrame with the inverters data.
    :rtype: pandas.core.frame.DataFrame

    :example:
        >>> import pandas as pd
        >>> from capitu.dap.getter.inverters import get as get_inverters
        >>> inverters = get_inverters(
        ...     "../capitu/datastore/inverters_v1.2.0.csv", campus="G",
        ...     inverters_date="2024-01-01", void_sn="drop")
        >>> isinstance(inverters, pd.DataFrame)
        True
        >>> inverters.shape
        (125, 9)
        >>> inverters.nominal_power_kw.head().values
        array([36, 36, 36, 36, 36])
        >>> inverters.serial_number.loc[50:51].values
        array(['9035KMTL197R0022', '9035KMTL197R0011'], dtype=object)
    """

    # Read inverters data.
    inverters_file = Path(filepath)
    inverters = pd.read_csv(inverters_file, sep=";", encoding="latin-1",
                            header=0)

    # Avoid the inverters whose serial number is null.
    if void_sn == "drop":
        inverters = inverters[
            inverters.serial_number == inverters.serial_number]
        inverters = inverters[inverters.serial_number != "-"]

    # Convert date columns to datetime.
    inverters.loc[inverters.register_date == "-", "register_date"] = pd.NaT
    inverters.loc[inverters.expiration_date == "-", "expiration_date"] = pd.NaT
    inverters.loc[:, "register_date"] = pd.to_datetime(
        inverters.register_date, format="%m.%d.%Y")
    inverters.loc[:, "expiration_date"] = pd.to_datetime(
        inverters.expiration_date, format="%m.%d.%Y")

    # Localize date columns.
    inverters.loc[:, "register_date"] = [date.tz_localize(
        "America/Sao_Paulo") for date in inverters.register_date]
    inverters.loc[:, "expiration_date"] = [date.tz_localize(
        "America/Sao_Paulo") for date in inverters.expiration_date]

    # Check whether user wants to filter the inverters data frame.
    if campus is not None:
        inverters = inverters[inverters.inverter.str.startswith(campus)]
    if inverters_date is not None:
        inverters_date = pd.to_datetime(inverters_date, format="%Y-%m-%d")
        inverters_date = inverters_date.tz_localize("America/Sao_Paulo")
        inverters = inverters[~(inverters.register_date >= inverters_date)]
        inverters = inverters[~(inverters.expiration_date < inverters_date)]

    return inverters
