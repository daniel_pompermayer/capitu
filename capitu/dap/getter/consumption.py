#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
The consumption module offers functions to import and preprocess consumption
data.
"""

import logging
import pandas as pd
from ..parser import consumption_date
from ..premises.holidays import holidays, vacation
from ..premises.generation_time import generation_time
from pathlib import Path


def get(filepath, aggregation=None, hour_offset=True, start_date=None,
        end_date=None, target_tz="America/Sao_Paulo", localize_tz=False,
        extra_kvar_i=0, extra_kvar_c=0, generation_time_only=False):
    """
    Read a file having the available consumption data of Ufes' campuses.

    :param filepath: Path to the consumption file.
    :type filepath: str
    :param aggregation: If None, no aggregation is performed. If "hour", the
        consumption file is aggregated by hour. If "month", the consumption
        file is aggregated by month.
    :type aggregation: str, optional
    :param hour_offset: If True and aggregation is set to "hour",  1 will be
        sumed up to the hour value of the registers, so the registered energy
        is related to the past hour interval. If aggregation is not "hour",
        this parameter doesn't take any effect.
    :type hour_offset: bool, default: True
    :raise KeyError: If aggregation is not None, "hour", or "month".
    :param start_date: Start date of the data interval.
    :type start_date: str, format: "%Y-%m-%d"
    :param end_date: End date of the data interval.
    :type end_date: str, format: "%Y-%m-%d"
    :param target_tz: Name of the target pytz timezone.
    :type target_tz: str, default "America/Sao_Paulo".
    :param localize_tz: If True, timezone is localized. If False, timezone is
        not localized.
    :type localize_tz: bool, default False.
    :param extra_kvar_i: Additional inductive reactive energy.
    :type extra_kvar_i: float, default: 0
    :param extra_kvar_c: Additional capacitive reactive energy.
    :type extra_kvar_c: float, default: 0
    :param generation_time_only: If True, only data registered in the
        generators working hours will be kept.
    :type generation_time_only: bool, default: True
    :return: time indexed pandas DataFrame with the consumption data.
    :rtype: pandas.core.frame.DataFrame

    :example:
        >>> import pandas as pd
        >>> from capitu.dap.getter.consumption import (
        ...     get as get_consumption)
        >>> consumption = get_consumption(
        ...     "../capitu/datastore/consumption.csv", aggregation="hour",
        ...     start_date="2016-01-01", end_date="2016-12-31",
        ...     target_tz="America/Sao_Paulo", localize_tz=False,
        ...     extra_kvar_i=0, extra_kvar_c=0)
        >>> consumption[["load_kwh", "load_kvarh_c", "load_kvarh_i"]]
                             load_kwh  load_kvarh_c  load_kvarh_i
        2016-01-01 00:00:00    856.10           0.0        591.50
        2016-01-01 01:00:00    836.50           0.0        560.70
        2016-01-01 02:00:00    826.70           0.0        550.20
        2016-01-01 03:00:00    819.00           0.0        541.80
        2016-01-01 04:00:00    813.40           0.0        549.50
        ...                       ...           ...           ...
        2016-12-31 19:00:00    705.60           0.0        432.00
        2016-12-31 20:00:00    816.00           0.0        482.88
        2016-12-31 21:00:00    813.12           0.0        489.60
        2016-12-31 22:00:00    798.72           0.0        477.12
        2016-12-31 23:00:00    792.00           0.0        474.24
        <BLANKLINE>
        [8016 rows x 3 columns]
        >>> len(consumption[consumption.weekend])
        2327
        >>> len(consumption[consumption.vacation])
        2497
        >>> len(consumption[consumption.holidays])
        456
        >>> len(consumption[consumption.business])
        3672
    """

    logging.info("Starting process.")

    # Read consumption data.
    consumption_file = Path(filepath)
    consumption = pd.read_csv(
        consumption_file, sep=";", decimal=",", encoding="latin_1", header=0,
        quotechar="'", usecols=[0, 2, 3, 4, 5], index_col=0, thousands=".",
        parse_dates=[0], date_format=consumption_date,
        names=["date", "time_band", "load_kwh", "load_kvarh_i",
               "load_kvarh_c"])

    # Translate the time band stamps.
    consumption.loc[:, "time_band"] = consumption.time_band.replace(
        {"Fora Ponta": "off_peak", "Ponta": "peak"})

    logging.info("Data read.")

    # Compute the integration time. For doing so, I'll need to group the data
    # using the date so I'm not integrating different different days together.
    consumption["int_time"] = consumption.groupby(
        consumption.index.date).apply(
            lambda group: -group.reset_index().date.diff(-1)).values
    consumption["int_time"] = consumption.int_time.ffill()
    consumption["int_time"] = (
        consumption.int_time.dt.total_seconds().values / 3600)

    # Add extra kvarh.
    consumption["load_kvarh_c"] = (
        consumption.int_time * extra_kvar_c + consumption.load_kvarh_c)
    consumption["load_kvarh_i"] = (
        consumption.int_time * extra_kvar_i + consumption.load_kvarh_i)

    # Localize timestamp.
    if localize_tz:
        consumption = consumption.tz_localize(
            target_tz, nonexistent="shift_forward", ambiguous=False)

    # Check whether user wants some kind of aggregation.
    if aggregation is None:
        pass
    elif aggregation == "hour":
        if hour_offset:
            consumption.index = consumption.index - pd.to_timedelta(
                consumption.int_time, unit="h")
        consumption = consumption.groupby(
            consumption.index.strftime("%Y %m %d %H")).agg({
                "time_band": "first", "load_kwh": "sum", "load_kvarh_i": "sum",
                "load_kvarh_c": "sum", "int_time": "sum"})
        consumption.index = pd.to_datetime(consumption.index)
        if hour_offset:
            consumption.index = consumption.index + pd.Timedelta(hours=1)
    elif aggregation == "month":
        consumption = consumption.groupby(
            consumption.index.strftime("%Y %m")).agg({
                "time_band": "first", "load_kwh": "sum", "load_kvarh_i": "sum",
                "load_kvarh_c": "sum", "int_time": "sum"})
        consumption.index = pd.to_datetime(consumption.index)
    elif aggregation == "time_band":
        consumption["day"] = consumption.index.date
        consumption = consumption.groupby(["day", "time_band"]).agg({
                "load_kwh": "sum", "load_kvarh_i": "sum",
                "load_kvarh_c": "sum", "int_time": "sum"})
        consumption = consumption.reset_index().set_index("day")
        consumption.index = pd.to_datetime(consumption.index)
    else:
        raise KeyError("Sorry. I don't know the chosen aggregation method.")

    logging.info("Data grouped.")

    # Check whether the user wants to keep only data from the time interval
    # when solar generation is expected.
    if generation_time_only:
        consumption = consumption.between_time(**generation_time)

        logging.info("Only generation time is kept.")

    # Localize timestamp again, since aggregation may have flushed that
    # information.
    if localize_tz:
        consumption = consumption.tz_localize(
            target_tz, nonexistent="shift_forward", ambiguous=False)

    # Check whether user wants an specific interval.
    consumption = consumption[start_date:end_date]

    logging.info("Data sliced.")

    # Mark vacation, holidays, weekends and business days.
    consumption["weekend"] = consumption.index.dayofweek.isin([5, 6])
    consumption["vacation"] = consumption.index.strftime("%Y-%m-%d").isin(
        vacation)
    consumption["holidays"] = consumption.index.strftime("%Y-%m-%d").isin(
        holidays)
    consumption["business"] = ~(consumption.weekend | consumption.vacation
                                | consumption.holidays)

    logging.info("Done.")

    return consumption
