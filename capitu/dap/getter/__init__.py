#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
The getter module offers functions to import and preprocess meteorological,
consumption, and inverter data.
"""
name = "getter"
__author__ = "Daniel Campos Pompermayer [gitlab.com/daniel_pompermayer]"
