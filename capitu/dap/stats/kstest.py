#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
The kstest module provides a function for testing the fit of a random variable
using the Kolmogorov-Smirnov Test.
"""

import logging
import numpy as np
import scipy.stats


def kstest(observations, rv, params, alpha=0.05):
    """
    Perform the Kolmogorov-Smirnov (KS) Test to assess the fit of a random
    variable to observations.

    :param observations: The observed data for testing the fit of the random
        variable.
    :type observations: array_like
    :param rv: A random variable object representing the theoretical
        distribution.
    :type rv: scipy.stats._distn_infrastructure.rv_continuous
    :param params: The parameters of the theoretical distribution.
    :type params: array_like
    :param alpha: The significance level for the test, which determines the
        critical value.
    :type alpha: float, default: 0.05.
    :return: True if the null hypothesis that the distribution of the
        observations matches the theoretical distribution cannot be rejected at
        the specified significance level; False otherwise.
    :rtype: bool

    :notes: This function performs the Kolmogorov-Smirnov (KS) Test to evaluate
        whether the distribution of the observed data (`observations`) matches
        the distribution described by the random variable (`rv`) with the given
        parameters (`params`). The KS statistic is computed as the maximum
        absolute difference between the empirical cumulative distribution
        function (ECDF) of the observed data and the cumulative distribution
        function (CDF) of the theoretical distribution.
        The null hypothesis is that the two distributions are identical.
        The significance level `alpha` determines the critical value for the
        test. If the computed KS statistic is less than the critical value, the
        null hypothesis cannot be rejected, indicating a good fit. Otherwise,
        if the KS statistic exceeds the critical value, the null hypothesis can
        be rejected at the specified significance level, indicating a possible
        poor fit.

    :references: Massey, F. J. Jr. (1951). The Kolmogorov-Smirnov Test for
        Goodness of Fit. Journal of the American Statistical Association,
        46(253), 68-78.

    :examples:
        >>> import numpy as np
        >>> from scipy.stats import norm, beta
        >>> from capitu.dap.stats.kstest import kstest
        >>> np.random.seed(42)
        >>> data = np.random.normal(loc=0, scale=1, size=100)
        >>> kstest(data, norm, (0, 1))
        True
        >>> kstest(data, beta, (7, 5, -4, 7))
        False

    """
    logging.info("Starting process.")

    # Get the ecdf of the observations.
    ecdf_obj = scipy.stats.ecdf(observations)
    quantiles = ecdf_obj.cdf.quantiles
    ecdf = ecdf_obj.cdf.probabilities

    logging.info("Got theoretical ecdf and quantiles from the observations.")

    # Compute the KS statistic.
    ks_stats = np.nanmax(np.abs(rv.cdf(quantiles, *params) - ecdf))

    # Get the number of observations.
    n_obs = len(observations)

    # Compute the critical value.
    k_alpha = scipy.stats.kstwo.isf(alpha, n_obs)

    logging.info("Got the critical value from %s." % (rv.name))

    logging.info("Done.")

    return ks_stats < k_alpha
