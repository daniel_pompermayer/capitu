#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
The stats module offers functions for building statistical models with the
data.
"""
name = "stats"
__author__ = "Daniel Campos Pompermayer [gitlab.com/daniel_pompermayer]"
