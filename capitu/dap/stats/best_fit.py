#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
The best_fit module provides a function for finding a random variable
distribution that best fits the data.
"""

import logging
import scipy.stats
import pandas as pd
from .aic import aic
from .kstest import kstest


def best_fit(data, fparams={}, candidates=None, alpha=0.05, error="raise",
             info=""):
    """
    Finds the random variable distribution that best fits the data.

    :param data: The observed data for testing the fit of the random variable.
    :type data: array_like
    :param fparams: Fixed parameters for the distribution.
    :type fparams: dict, default: {}
    :param candidates: List of candidate distributions to consider. If None,
        all available scipy distributions are considered, except for some
        hardcoded exceptions.
    :type candidates: list, default: None
    :param alpha: The significance level for the Kolmogorov-Smirnov test.
    :type alpha: float, default: 0.05
    :param error: Determines the behavior on error. "raise" to raise
        exceptions, "ignore" to proceed silently.
    :type error: str, default: "raise"
    :param info: Additional information to be displayed in logs.
    :type info: str, default: ""
    :return: A tuple containing the name of the fitted distribution, its
        shapes, fitted parameters, AIC value, and KS test result.
    :rtype: tuple

    :examples:
        >>> import numpy as np
        >>> from capitu.dap.stats.best_fit import best_fit
        >>> np.random.seed(42)
        >>> data = np.random.normal(loc=0, scale=1, size=1000)
        >>> rv, shapes, params, aic, ks_approved = best_fit(
        ...     data, candidates=["norm", "beta", "dweibull", "triang"])
        >>> rv
        'norm'
        >>> shapes
        []
        >>> params
        (0.01933205582232549, 0.9787262077473543)
        >>> aic
        2798.870384834241
        >>> ks_approved
        True
    """

    logging.info("Starting process.")

    # Check whether user has not given a list of candidates.
    if candidates is None:

        # Load all scipy available distributions.
        candidates = scipy.stats._continuous_distns._distn_names.copy()

        # Remove the some distributions that have been taking too long to
        # fit.
        candidates.remove("studentized_range")
        candidates.remove("gausshyper")
        candidates.remove("rel_breitwigner")

    # I'm storing candidates as a dataframe so I can sort later according to
    # the AIC.
    candidates = pd.DataFrame({"name": candidates})

    # Search the random variables in scipy.stats.
    candidates["rv"] = candidates.name.apply(
        lambda rv_name: getattr(scipy.stats, rv_name))

    logging.info("Got the candidate random variables.")

    # Compute the AIC of each candidate.
    candidates[["aic", "params"]] = candidates.apply(
        lambda candidate: aic(data, candidate.rv, fparams, None, error),
        result_type="expand", axis=1)

    # Sort the candidates according to it's AIC.
    candidates = candidates.sort_values("aic")

    logging.info("%sGot candidates' AIC.", (info,))

    # I'll iterate over the data frame of the candidates to find the first one
    # approved in the Kolmogorov-Smirnov test. If none of them is approved,
    # the one having the smaller aic is to be chosen.
    rv = candidates.rv.iloc[0]
    params = candidates.params.iloc[0]
    rv_aic = candidates.aic.iloc[0]
    ks_approved = False
    for index, candidate in candidates.iterrows():
        logging.info("%sKS Test: %s" % (info, candidate.rv.name))
        try:
            ks_result = kstest(data, candidate.rv, candidate.params, alpha)
        except Exception as e:
            logging.error("Ops! %s" % (e.args[0]))
            if error == "raise":
                raise e
            else:
                ks_result = False
        if ks_result:
            rv = candidate.rv
            params = candidate.params
            rv_aic = candidate.aic
            ks_approved = True
            logging.info("KS Test: %s approved." % (candidate.rv.name))
            break
        else:
            logging.info("KS Test: %s failed." % (candidate.rv.name))

    # Check whether the random variable has shapes.
    if rv.shapes is not None:
        shapes = rv.shapes.split(", ")
    else:
        shapes = []

        logging.info("%sDone.", (info,))

    return rv.name, shapes, params, rv_aic, ks_approved
