#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
The find_rvs module offers a function to group data acording to its x-axis
values and some categories and fit random variables using data from each group.
"""

import re
import logging
import scipy.stats
import numpy as np
import pandas as pd
from pathlib import Path
from .group_fit import group_fit
from .single_fit import single_fit
from ..plotter.labels import table_labels


def find_rvs(data, y, approach="best", x=None, categories=None, fparams={},
             candidates=None, alpha=0.05, bounds=False, pdfs_path=None,
             tables_path=None, filename="pdfs"):
    """
    Group data acording to its x-axis values and some categories and fit random
    variables using data from each group. It can try to find the best of the
    scipy.stats available distribution using a Kolmogorov-Smirnov test.

    :param data: data frame having the data we're modeling.
    :type data: pandas.core.frame.DataFrame
    :param y: string or list of strings having the name of the column having
              the y-axis of the data to be modeled.
    :type y: str or List[str]
    :param approach: approach to use, either "best" to find the best
        distribution or "single" to fit each variable separately.
    :type approach: str, default: "best"
    :param x: string having the name of the column having the x-axis of the
              data to be modeled.
    :type x: str, default: None
    :param categories: name of the column having the categorical information
        that will split be used to split data in different groups.
    :type categories: str, default: None
    :param fparams: f0…fn - hold respective shape parameters fixed. For
        example, floc holds the location parameter fixed to the specified
        value.
    :type fparams: dictionary or list of dictionaries, default: {}
    :param candidates: List of candidate distributions to consider. If None,
        all available scipy distributions are considered, except for some
        hardcoded exceptions
    :type candidates: List[str], default: None
    :param alpha: significance level for the Kolmogorov-Smirnov test.
    :type alpha: float, default: 0.05
    :param bounds: If True, constrain the found density parameters to the
        inferred bounds.
    :type bounds: bool, default: False
    :param pdfs_path: address to the directory where the parameters of the
        fitted random variable will be saved. If None, parameters will not
        be saved.
    :type pdfs_path: str or None, default: None
    :param tables_path: address to the directory where the parameters of the
        fitted random variable will be saved in a table format. If None,
        parameters will not be saved as table.
    :type tables_path: str or None, default: None
    :param filename: name of the file to be saved.
    :type filename: str, default: "pdfs"
    :return: If approach is "best", returns a pandas DataFrame containing the
        parameters of the fitted random variable for each group. If approach is
        "single", returns three pandas DataFrames: single_params, best_params,
        and comparisons.
    :rtype: pandas.DataFrame or tuple(pandas.DataFrame, pandas.DataFrame,
        pandas.DataFrame)

    :examples:
        >>> import os
        >>> import filecmp
        >>> import numpy as np
        >>> import pandas as pd
        >>> from scipy.stats import norm, triang, beta
        >>> from capitu.dap import set_language
        >>> from capitu.dap.stats.find_rvs import find_rvs
        >>> set_language("english")
        >>> nine_am = pd.date_range(
        ...     start='2023-01-01 09:00:00', end='2023-12-31 09:00:00',
        ...     freq='D')
        >>> data_size = len(nine_am)
        >>> np.random.seed(42)
        >>> temp_9am = norm.rvs(loc=26, scale=3, size=data_size)
        >>> np.random.seed(42)
        >>> solar_rad_9am = norm.rvs(loc=1345, scale=575, size=data_size)
        >>> eleven_am = pd.date_range(
        ...     start='2023-01-01 11:00:00', end='2023-12-31 11:00:00',
        ...     freq='D')
        >>> np.random.seed(42)
        >>> temp_11am = norm.rvs(loc=28, scale=3, size=data_size)
        >>> np.random.seed(42)
        >>> solar_rad_11am = norm.rvs(loc=2300, scale=850, size=data_size)
        >>> one_pm = pd.date_range(
        ...     start='2023-01-01 13:00:00', end='2023-12-31 13:00:00',
        ...     freq='D')
        >>> np.random.seed(42)
        >>> temp_1pm = triang.rvs(c=0.6, loc=19, scale=17, size=data_size)
        >>> np.random.seed(42)
        >>> solar_rad_1pm = triang.rvs(c=1, loc=-30, scale=4100,
        ...                            size=data_size)
        >>> three_pm = pd.date_range(
        ...     start='2023-01-01 15:00:00', end='2023-12-31 15:00:00',
        ...     freq='D')
        >>> np.random.seed(42)
        >>> temp_3pm = beta.rvs(a=5, b=3, loc=16, scale=19, size=data_size)
        >>> np.random.seed(42)
        >>> solar_rad_3pm = beta.rvs(a=1, b=1, loc=66, scale=3560,
        ...                          size=data_size)
        >>> data = pd.DataFrame({
        ...     "temperature_celsius": np.concatenate([
        ...         temp_9am, temp_11am, temp_1pm, temp_3pm]),
        ...     "e_kj_per_m2": np.concatenate([
        ...         solar_rad_9am, solar_rad_11am, solar_rad_1pm,
        ...         solar_rad_3pm])},
        ...         index=np.concatenate([nine_am, eleven_am, one_pm,
        ...                               three_pm]))
        >>> data["hour"] = data.index.hour
        >>> single_params, best_params, comparison = find_rvs(
        ...     data, y=["temperature_celsius", "e_kj_per_m2"],
        ...     approach="single", categories="hour", pdfs_path="./",
        ...     tables_path="./", filename="pdfs",
        ...     candidates = ["norm", "triang", "uniform", "beta"])
        >>> filecmp.cmp('./best_pdfs.csv',
        ...             '../capitu/datastore/doctest/best_pdfs.csv')
        True
        >>> os.remove('./best_pdfs.csv')
        >>> filecmp.cmp('./best_pdfs.tex',
        ...             '../capitu/datastore/doctest/best_pdfs.tex')
        True
        >>> os.remove('./best_pdfs.tex')
        >>> filecmp.cmp('./single_pdfs.csv',
        ...             '../capitu/datastore/doctest/single_pdfs.csv')
        True
        >>> os.remove('./single_pdfs.csv')
        >>> filecmp.cmp('./single_pdfs.tex',
        ...             '../capitu/datastore/doctest/single_pdfs.tex')
        True
        >>> os.remove('./single_pdfs.tex')
        >>> filecmp.cmp('./comparison_pdfs.csv',
        ...             '../capitu/datastore/doctest/comparison_pdfs.csv')
        True
        >>> os.remove('./comparison_pdfs.csv')
        >>> filecmp.cmp('./comparison_pdfs.tex',
        ...             '../capitu/datastore/doctest/comparison_pdfs.tex')
        True
        >>> os.remove('./comparison_pdfs.tex')
    """

    logging.info("Starting process.")

    # Do not bother external data.
    data = data.copy()

    logging.info("Protected data.")

    # I need y to be a list of column names. So I'll check whether y is not a
    # list of column names and adjust it.
    if not isinstance(y, list):
        y = [y]

    # Check whether fparams and candidates are lists single directives, then
    # relate them to each variable.
    if isinstance(fparams, list):
        fparams = dict(zip(y, fparams))
    else:
        fparams = dict(zip(y, len(y) * [fparams]))

    logging.info("Got dictionaries of fixed parameters.")

    # I'm using x and category to group the data, but only if they're not None.
    grouper = [column for column in [x, categories] if column is not None]

    # I'm melting data so I can calculate each variable separately.
    data = data.melt(id_vars=grouper, value_vars=y, var_name="variable",
                     value_name="value", ignore_index=False)

    # Check which approach to use.
    if approach == "best":

        # Iterate over the variables.
        rvs_params = pd.DataFrame([])
        for variable in y:
            rv_params = group_fit(
                data[data.variable == variable], "best", grouper,
                fparams[variable], alpha, candidates, info=variable+" - ")
            rv_params["variable"] = variable
            rvs_params = pd.concat([rvs_params, rv_params], ignore_index=True)

        # Check whether user wants to save the fitted parameters.
        if pdfs_path is not None:

            # Convert string of the save directory to path.
            save_path = Path(pdfs_path) / (filename + ".csv")

            # Save it.
            rvs_params.to_csv(save_path, sep=";", decimal=".")

        # Check whether user wants to save the table of parameters.
        if tables_path is not None:

            # Convert string of the save directory to path.
            tex_path = Path(tables_path) / (filename + ".tex")

            # Format the column names and values.
            formated_rvs_params = rvs_params.rename(columns=table_labels)
            formated_rvs_params = formated_rvs_params.replace(table_labels)

            # Save it.
            formated_rvs_params.to_latex(
                tex_path, index=False, column_format=rvs_params.shape[1] * "c",
                na_rep="-", escape=True, longtable=True)

            # I'm gonna need to manually change the break table message.
            with open(tex_path, "r") as filp:
                content = filp.read()
                content = re.sub(
                    r"Continued on next page",
                    r"%s" % table_labels["continue"],
                    content)

            with open(tex_path, "w") as filp:
                filp.write(content)

            logging.info("Tables exported.")

        logging.info("Done.")

        return rvs_params

    elif approach == "single":

        # Iterate over the variables.
        singles_params = pd.DataFrame([])
        bests_params = pd.DataFrame([])
        comparisons = pd.DataFrame([])

        for variable in y:
            single_params, best_params, comparison = single_fit(
                data[data.variable == variable], grouper, "value",
                fparams[variable], candidates, alpha, error="ignore",
                return_best=True,
                info=variable + " - ")

            # Check whether I must constrain the found parameters to inferred
            # bounds.
            density = getattr(
                    scipy.stats, single_params.iloc[0].density)

            if bounds and density.shapes is not None:

                # Get the name of the parameters.
                param_names = density.shapes.split(", ") + ["loc", "scale"]

                # Get the quartiles.
                indices = single_params[param_names].quantile([0.9, 0.1]).T

                # Compute a interquantile range.
                indices["iqr"] = indices[0.9] - indices[0.1]

                # Compute the external limits.
                indices["max_value"] = indices[0.9] + indices["iqr"]
                indices["min_value"] = indices[0.1] - indices["iqr"]

                # Infer the bounds.
                param_bounds = {index: (row['min_value'], row['max_value'])
                                for index, row in indices.iterrows()}

                # Group fit it with bounded parameters.
                bounded_single_params = group_fit(
                    data[data.variable == variable], density.name, grouper,
                    "value", fparams[variable], alpha, param_bounds,
                    candidates, info=variable+" - ")

                # Get the out of bounds registers.
                out_of_bounds = [False] * len(single_params)
                for param in param_names:
                    min_value, max_value = param_bounds[param]
                    out_of_bounds |= np.logical_or(
                        single_params[param] < min_value,
                        single_params[param] > max_value).values

                # Get the columns to be substituted:
                target_columns = param_names + ["aic", "ks_approved"]

                # Compare the two sets.
                better_aic = (bounded_single_params.aic.values
                              < single_params.aic.values)

                # I'll benefit from the bounded parameters and substitute
                # some rows.
                substitute = np.logical_or(np.logical_or(

                    # The AIC is not better, but the original fit was not
                    # approved in the KS Test while the new was.
                    np.logical_and(np.logical_and(
                        np.logical_not(better_aic),
                        np.logical_not(single_params.ks_approved.values)),
                                   bounded_single_params.ks_approved.values),

                    # The AIC si better and either the original fit was not
                    # approved in the KS Test or the new was.
                    np.logical_and(
                        better_aic,
                        np.logical_or(
                            np.logical_not(single_params.ks_approved.values),
                            bounded_single_params.ks_approved.values))),

                    # The parameters is out of bounds.
                    out_of_bounds)

                # Substitute it.
                single_params.loc[substitute, target_columns] = (
                    bounded_single_params.loc[substitute,
                                              target_columns]).values

            single_params["variable"] = variable
            best_params["variable"] = variable
            comparison["variable"] = variable
            singles_params = pd.concat([singles_params, single_params],
                                       ignore_index=True)
            bests_params = pd.concat([bests_params, best_params],
                                     ignore_index=True)
            comparisons = pd.concat([comparisons, comparison],
                                    ignore_index=True)

        # Check whether user wants to save the fitted parameters.
        if pdfs_path is not None:

            # List of prefixes and data frame parameters.
            prefixes = ["single_", "best_", "comparison_"]
            df_params = [singles_params, bests_params, comparisons]

            # Itereate over the prefixes and data frame parameters.
            for prefix, df in zip(prefixes, df_params):

                # Convert string of the save directory to path.
                csv_path = Path(pdfs_path) / (prefix + filename + ".csv")

                # Save it.
                df.to_csv(csv_path, sep=";", decimal=".")

            logging.info("Pdfs exported.")

        # Check whether user wants to save the table of parameters.
        if tables_path is not None:

            # List of prefixes and data frame parameters.
            prefixes = ["single_", "best_", "comparison_"]
            df_params = [singles_params, bests_params, comparisons]

            # Itereate over the prefixes and data frame parameters.
            for prefix, df in zip(prefixes, df_params):

                # Convert string of the save directory to path.
                tex_path = Path(tables_path) / (prefix + filename + ".tex")

                # Format the column names and values.
                formated_df = df.rename(columns=table_labels)
                formated_df = formated_df.replace(table_labels)

                # Save it.
                formated_df.to_latex(
                    tex_path, index=False, column_format=df.shape[1] * "c",
                    na_rep="-", escape=True, longtable=True)

                # I'm gonna need to manually change the break table message.
                with open(tex_path, "r") as filp:
                    content = filp.read()
                    content = re.sub(
                        r"Continued on next page",
                        r"%s" % table_labels["continue"],
                        content)

                with open(tex_path, "w") as filp:
                    filp.write(content)

            logging.info("Tables exported.")

        logging.info("Done.")

        return singles_params, bests_params, comparisons

    else:
        raise AttributeError("Sorry, I don't know that approach.")
