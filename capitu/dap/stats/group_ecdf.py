#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
The group_ecdf module offers a function to compute empirical cumulative
distribution functions (ECDFs) for groups of data based on specified y-axis
values and an optional x-axis variable.
"""

import logging
import scipy.stats


def group_ecdf(data, y, x="hour", info=""):

    """
    Computes empirical cumulative distribution functions (ECDFs) for groups of
    data based on specified y-axis values and an optional x-axis variable.

    :param data: The data for which ECDFs will be calculated.
    :type data: pandas.DataFrame
    :param y: The column name(s) representing the y-axis values.
    :type y: str or list[str]
    :param x: The column name representing the x-axis variable.
    :type x: str, default: "hour"
    :param info: Additional information to be displayed in logs.
    :type info: str, default: ""
    :return: A pandas DataFrame containing the ECDFs for each group.
    :rtype: pandas.DataFrame

    :examples:
        >>> import numpy as np
        >>> import pandas as pd
        >>> from scipy.stats import norm
        >>> from capitu.dap.stats.group_ecdf import group_ecdf
        >>> dates = pd.date_range(start='2023-01-01 09:00:00',
        ...                       end='2023-12-31', freq='2H')
        >>> dates = dates[np.logical_and(dates.hour >= 9, dates.hour <= 15)]
        >>> np.random.seed(42)
        >>> temp = norm.rvs(loc=26, scale=3, size=len(dates))
        >>> data = pd.DataFrame({"temperature_celsius": temp}, index=dates)
        >>> data["hour"] = data.index.hour
        >>> ecdfs = group_ecdf(data, y="temperature_celsius", x="hour")
        >>> ecdfs
        variable  hour                                temperature_celsius
        0            9  ECDFResult(cdf=EmpiricalDistributionFunction(q...
        1           11  ECDFResult(cdf=EmpiricalDistributionFunction(q...
        2           13  ECDFResult(cdf=EmpiricalDistributionFunction(q...
        3           15  ECDFResult(cdf=EmpiricalDistributionFunction(q...
        >>> ecdfs[ecdfs.hour==9].temperature_celsius.iloc[0].cdf.evaluate(27)
        array(0.64010989)
        >>> ecdfs[ecdfs.hour==11].temperature_celsius.iloc[0].cdf.evaluate(27)
        array(0.62087912)
        >>> ecdfs[ecdfs.hour==13].temperature_celsius.iloc[0].cdf.evaluate(27)
        array(0.6456044)
        >>> ecdfs[ecdfs.hour==15].temperature_celsius.iloc[0].cdf.evaluate(27)
        array(0.58516484)
    """

    logging.info("Starting process.")

    data = data.copy()

    logging.info("Protected data.")

    # I need y to be a list of column names. So I'll check whether y is a
    # single column name or a list of column names and adjust it.
    if isinstance(y, str):
        y = [y]

    data = data[[x] + y].melt(id_vars=x)

    groups = data.groupby([x, "variable"])

    logging.info("Got list of groups.")

    ecdfs = groups.value.apply(scipy.stats.ecdf)

    ecdfs = ecdfs.reset_index().pivot_table(
        values="value", index="hour", columns="variable", aggfunc="first")

    return ecdfs.reset_index()
