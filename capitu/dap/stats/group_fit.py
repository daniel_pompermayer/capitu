#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
The group_fit module provides a function for fitting a random variable
distribution to goups of data.
"""

import logging
import pandas as pd
from .best_fit import best_fit
from .dist_fit import dist_fit


def group_fit(data, density, grouper, y="value", fparams={}, alpha=0.05,
              bounds=None, candidates=None, error="raise", info=""):

    """
    Fits a random variable distribution to groups of data.

    :param data: The data to be grouped and fitted.
    :type data: pandas.DataFrame or pandas.Series
    :param density: The name of the random variable distribution to fit or
        "best" to find the best fit.
    :type density: str
    :param grouper: List of column names with which to group the data.
    :type grouper: [str]
    :param y: string having the name of the column having the y-axis of the
        data to be modeled.
    :type y: str, default: "value"
    :param fparams: Fixed parameters for the distribution.
    :type fparams: dict, default: {}
    :param alpha: The significance level for the Kolmogorov-Smirnov test.
    :type alpha: float, default: 0.05
    :param bounds: Dictionary whose keys are names of the parameters of the
        distribution and whose values are tuples containing the lower and upper
        bound on that parameter. Not used when density is "best".
    :type bounds: dict, default: None
    :param candidates: List of candidate distributions to consider. If None,
        all available scipy distributions are considered, except for some
        hardcoded exceptions.
    :type candidates: [str], default: None
    :param error: Determines the behavior on error. "raise" to raise
        exceptions, "ignore" to proceed silently.
    :type error: str, default: "raise"
    :param info: Additional information to be displayed in logs.
    :type info: str, default: ""
    :return: A pandas DataFrame containing the parameters of the fitted random
        variable distributions for each group.
    :rtype: pandas.DataFrame

    :examples:
        >>> import numpy as np
        >>> import pandas as pd
        >>> from scipy.stats import norm, triang
        >>> from capitu.dap.stats.group_fit import group_fit
        >>> nine_am = pd.date_range(
        ...     start='2023-01-01 09:00:00', end='2023-12-31 09:00:00',
        ...     freq='D')
        >>> data_size = len(nine_am)
        >>> np.random.seed(42)
        >>> temp_9am = norm.rvs(loc=26, scale=3, size=data_size)
        >>> one_pm = pd.date_range(
        ...     start='2023-01-01 13:00:00', end='2023-12-31 13:00:00',
        ...     freq='D')
        >>> np.random.seed(42)
        >>> temp_1pm = triang.rvs(c=0.6, loc=19, scale=17, size=data_size)
        >>> data = pd.DataFrame({
        ...    "temperature_celsius": np.concatenate([temp_9am, temp_1pm])},
        ...        index=np.concatenate([nine_am, one_pm]))
        >>> data["hour"] = data.index.hour
        >>> params = group_fit(data, "norm", ["hour"], y="temperature_celsius")
        >>> params.loc[0]
        hour                     9
        loc              26.029839
        scale             2.840254
        density               norm
        aic            1801.867501
        ks_approved           True
        Name: 0, dtype: object
        >>> params.loc[1]
        hour                    13
        loc              27.935379
        scale             3.521975
        density               norm
        aic            1958.911228
        ks_approved           True
        Name: 1, dtype: object
        >>> best_params = group_fit(data, "best", ["hour"],
        ...                         y="temperature_celsius",
        ...                         candidates=["norm", "beta", "triang"])
        >>> best_params.loc[0]
        hour                     9
        loc              26.029839
        scale             2.840254
        density               norm
        aic            1801.867501
        ks_approved           True
        c                      NaN
        Name: 0, dtype: object
        >>> best_params.loc[1]
        hour                    13
        loc              19.064596
        scale            16.359921
        density             triang
        aic            1925.875644
        ks_approved           True
        c                  0.63726
        Name: 1, dtype: object
    """

    # Split data in hours and clusters.
    groups = data.groupby(grouper)

    logging.info("Grouped data.")

    # Create a data frame to store the parameters of the fitted rvs.
    rvs_params = pd.DataFrame([])

    # I'm creating a dictionary to store my parameters.
    for i, (index, group) in enumerate(groups):

        logging.info("%sFitting group %s with %s."
                     % (info, str(index), density))
        infos = "%s%s[%d/%d] - " % (info, str(index), i + 1, len(groups))

        try:

            # Check whether user wants to find the best distribution.
            if density == "best":

                # Search for the best fit.
                rv_density, shapes, params, rv_aic, ks_approved = best_fit(
                    group[y].values, fparams, candidates, alpha, "ignore",
                    info=infos)

            # Or whether he parsed the name of the distribution he wants to fit
            else:
                rv_density, shapes, params, rv_aic, ks_approved = dist_fit(
                    group[y].values, density, fparams, alpha, bounds, "raise",
                    info=infos)

        except Exception as e:
            logging.error("Error fitting %s for group %s: %s" % (
                density, str(index), e.args[0]))
            if error == "raise":
                raise type(e)("Error fitting %s for group %s: %s" % (
                    density, str(index), e.args[0]))
            else:
                ks_approved = False

        logging.info("Distribution fitted.")

        # Get the names of the columns of the parameters data frame.
        names = (grouper + shapes + ["loc", "scale"]
                 + ["density", "aic", "ks_approved"])

        # Get the values of the parameters data frame.
        values = index + params + (rv_density, rv_aic, ks_approved)

        # Add parameters to the parameters data frame.
        rvs_params = pd.concat([
            rvs_params, pd.DataFrame(dict(zip(names, values)), index=[i])])

        logging.info("%sDone.", info)

    return rvs_params
