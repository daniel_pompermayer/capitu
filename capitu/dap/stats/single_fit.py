#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
The single_fit module offers a function to identify the optimal random
variable distribution that fits an entire dataset and contrasts it with the
best distribution for each subgroup.
"""


import logging
import scipy.stats
import pandas as pd
from .group_fit import group_fit


def single_fit(data, grouper, y="value", fparams={}, candidates=None,
               alpha=0.05, error="raise", return_best=False, info=""):

    """
    Identify the optimal random variable distribution that fits an entire
    dataset and contrasts it with the best distribution for each subgroup.

    :param data: The data to be grouped and fitted.
    :type data: pandas.DataFrame or pandas.Series
    :param grouper: List of column names with which to group the data.
    :type grouper: List[str]
    :param y: string having the name of the column having the y-axis of the
        data to be modeled.
    :type y: str, default: "value"
    :param fparams: Fixed parameters for the distribution.
    :type fparams: dict, default: {}
    :param candidates: List of candidate distributions to consider. If None,
        all available scipy distributions are considered, except for some
        hardcoded exceptions.
    :type candidates: List[str] or None, default: None
    :param alpha: The significance level for the Kolmogorov-Smirnov test.
    :type alpha: float, default: 0.05
    :param error: Determines the behavior on error. "raise" to raise
        exceptions, "ignore" to proceed silently.
    :type error: str, default: "raise"
    :param return_best: Whether to return parameters for the best fit for each
        group.
    :type return_best: bool, default: False
    :param info: Additional information to be displayed in logs.
    :type info: str, default: ""
    :return: If return_best is False, returns parameters for the single best
        fit for the entire dataset. If return_best is True, returns parameters
        for the single best fit for the entire dataset, parameters for the best
        fit for each group, and a comparison between the single best fit and
        the best fit for each group.
    :rtype: pandas.DataFrame or tuple(pandas.DataFrame, pandas.DataFrame,
        pandas.DataFrame)

    :examples:
        >>> import numpy as np
        >>> import pandas as pd
        >>> from scipy.stats import norm, triang, beta
        >>> from capitu.dap.stats.single_fit import single_fit
        >>> nine_am = pd.date_range(
        ...     start='2023-01-01 09:00:00', end='2023-12-31 09:00:00',
        ...     freq='D')
        >>> data_size = len(nine_am)
        >>> np.random.seed(42)
        >>> temp_9am = norm.rvs(loc=26, scale=3, size=data_size)
        >>> eleven_am = pd.date_range(
        ...     start='2023-01-01 11:00:00', end='2023-12-31 11:00:00',
        ...     freq='D')
        >>> np.random.seed(42)
        >>> temp_11am = norm.rvs(loc=28, scale=3, size=data_size)
        >>> one_pm = pd.date_range(
        ...     start='2023-01-01 13:00:00', end='2023-12-31 13:00:00',
        ...     freq='D')
        >>> np.random.seed(42)
        >>> temp_1pm = triang.rvs(c=0.6, loc=19, scale=17, size=data_size)
        >>> three_pm = pd.date_range(
        ...     start='2023-01-01 15:00:00', end='2023-12-31 15:00:00',
        ...     freq='D')
        >>> np.random.seed(42)
        >>> temp_3pm = beta.rvs(a=5, b=3, loc=16, scale=19, size=data_size)
        >>> data = pd.DataFrame({
        ...     "temperature_celsius": np.concatenate([
        ...         temp_9am, temp_11am, temp_1pm, temp_3pm])},
        ...         index=np.concatenate([nine_am, eleven_am, one_pm,
        ...                               three_pm]))
        >>> data["hour"] = data.index.hour
        >>> single_params, best_params, comparison = single_fit(
        ...     data, grouper=["hour"], y="temperature_celsius",
        ...     return_best=True,
        ...     candidates = ["norm", "triang", "uniform", "beta"])
        >>> comparison # doctest: +NORMALIZE_WHITESPACE
                          aic              density        ks_approved
        approach         best       single    best single        best single
        hour
        9         1801.867501  1804.814647    norm   beta        True   True
        11        1801.867501  1804.781995    norm   beta        True   True
        13        1925.875644  1929.619499  triang   beta        True   True
        15        1793.554834  1799.420100  triang   beta        True   True
    """

    logging.info("Starting process.")

    # Check whether user has not given a list of candidates.
    if candidates is None:

        # Load all scipy available distributions.
        candidates = scipy.stats._continuous_distns._distn_names.copy()

        # Remove the some distributions that have been taking too long to
        # fit.
        candidates.remove("studentized_range")
        candidates.remove("gausshyper")
        candidates.remove("rel_breitwigner")
        candidates.remove("genhyperbolic")

    # I'm storing candidates as a dataframe so I can sort later according to
    # the AIC.
    candidates = pd.DataFrame({"name": candidates})

    logging.info("Got the list of candidates.")

    # Fit data using each candidate and compute the mean aic and the number of
    # KS approved.
    rvs_params = pd.DataFrame([])
    for i, candidate in candidates.iterrows():
        try:
            infos = "%s%s [%d/%d] - " % (
                info, candidate["name"], i + 1, len(candidates))
            rv_params = group_fit(data, candidate["name"], grouper, y,
                                  fparams, alpha, info=infos)
            candidates.loc[candidates["name"] == candidate["name"],
                           "mean_aic"] = rv_params.aic.mean()
            candidates.loc[
                candidates["name"] == candidate["name"],
                "number_of_ks_approved"] = rv_params.ks_approved.sum()
            rvs_params = pd.concat([rvs_params, rv_params], ignore_index=True)
        except Exception as e:
            logging.error("Ops! %s" % (e.args[0]))
            if error == "raise":
                raise e

    logging.info("Got the mean AIC and the number of ks approved.")

    # Sort the candidates according to the number of ks approved and mean AIC.
    candidates = candidates.sort_values(["number_of_ks_approved", "mean_aic"],
                                        ascending=[False, True])
    logging.info("Got candidates sorted.")

    # Find the best single candidate.
    single_candidate = candidates.name.iloc[0]
    single_params = rvs_params[rvs_params.density == single_candidate].dropna(
        axis=1, how="all")

    if not return_best:
        return single_params

    # Get the best candidate of each group.
    best_params = rvs_params.sort_values(
        grouper + ["ks_approved", "aic"], ascending=len(grouper) * [True]
        + [False, True]).drop_duplicates(
            grouper, keep="first").reset_index(drop=True)

    # Get the comparison between the single candidate and the best candidate.
    single_summary = single_params[grouper + ["density", "aic", "ks_approved"]]
    single_summary["approach"] = "single"
    best_summary = best_params[grouper + ["density", "aic", "ks_approved"]]
    best_summary["approach"] = "best"
    comparison = pd.concat([single_summary, best_summary]).pivot_table(
        index=grouper, columns="approach", aggfunc="first")

    logging.info("Done!")

    return single_params, best_params, comparison
