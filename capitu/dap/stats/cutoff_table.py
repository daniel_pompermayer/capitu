#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
The get_cutoff_table module offers a function to compute cutoff values from
empirical cumulative distribution functions (ECDFs) based on specified cutoffs
mode.
"""

import numpy as np


def get_cutoff_table(ecdfs, cutoff=0.9, x="hour", cutoff_mode="probability",
                     format=None):

    """
    Computes cutoff values from empirical cumulative distribution functions
    (ECDFs) based on specified cutoff mode.

    :param ecdfs: The empirical cumulative distribution functions for which
        cutoff values will be calculated.
    :type ecdfs: pandas.DataFrame
    :param cutoff: The cutoff value used to determine the threshold.
    :type cutoff: float, default: 0.9
    :param x: The column name representing the x-axis variable.
    :type x: str, default: "hour"
    :param cutoff_mode: The mode for determining the cutoff value. Options
        are "probability" and "quantile".
    :type cutoff_mode: str, default: "probability"
    :raise KeyError: If aggregation is not "probability", or "quantile".
    :return: A pandas DataFrame containing the cutoff values for each hour and
        variable combination.
    :rtype: pandas.DataFrame

    :examples:
        >>> import numpy as np
        >>> import pandas as pd
        >>> from scipy.stats import norm
        >>> from capitu.dap.stats.group_ecdf import group_ecdf
        >>> from capitu.dap.stats.cutoff_table import get_cutoff_table
        >>> dates = pd.date_range(start='2023-01-01 09:00:00',
        ...                       end='2023-12-31', freq='2H')
        >>> dates = dates[np.logical_and(dates.hour >= 9, dates.hour <= 15)]
        >>> np.random.seed(42)
        >>> temp = norm.rvs(loc=26, scale=3, size=len(dates))
        >>> data = pd.DataFrame({"temperature_celsius": temp}, index=dates)
        >>> data["hour"] = data.index.hour
        >>> ecdfs = group_ecdf(data, y="temperature_celsius", x="hour")
        >>> cutoff_table = get_cutoff_table(
        ...     ecdfs, cutoff=0.9, x="hour", cutoff_mode="probability")
        >>> cutoff_table # doctest: +NORMALIZE_WHITESPACE
        variable temperature_celsius
        hour
        9                  29.603642
        11                 30.140274
        13                 29.984582
        15                 30.066914
        >>> cutoff_table = get_cutoff_table(
        ...     ecdfs, cutoff=30, x="hour", cutoff_mode="quantile")
        >>> cutoff_table # doctest: +NORMALIZE_WHITESPACE
        variable temperature_celsius
        hour
        9                   0.909341
        11                  0.884615
        13                  0.901099
        15                  0.895604
    """

    if cutoff_mode == "probability":
        cutoff_table = ecdfs.melt(id_vars=x)
        cutoff_table.loc[:, "value"] = cutoff_table.value.apply(
            lambda ecdf: ecdf.cdf.quantiles[np.abs(
                ecdf.cdf.probabilities - cutoff).argmin()])

    elif cutoff_mode == "quantile":
        cutoff_table = ecdfs.melt(id_vars=x)
        cutoff_table.loc[:, "value"] = cutoff_table.value.apply(
            lambda ecdf: ecdf.cdf.evaluate(cutoff))

    else:
        raise KeyError("Sorry. I don't know the chosen cutoff mode.")

    cutoff_table = cutoff_table.pivot_table(
        index="hour", columns="variable").droplevel(0, axis=1)

    if format is not None:
        cutoff_table = cutoff_table.map(lambda x: format.format(x))

    return cutoff_table
