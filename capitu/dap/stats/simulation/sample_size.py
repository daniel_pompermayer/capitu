#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
The sample_size module offers a function for determining the
maximum sample size the computer is able to use to conducting a Monte Carlo
simulation process using statistical models of generation and consumption.
"""

import logging
import numpy as np
import pandas as pd
from pathlib import Path
from . import simulation


def sample_size_determination(generation_pdfs, consumption_pdfs, clusters,
                              num_of_inverters, target_tz, fee_date,
                              path=None, start=1, stop=5, num=29):

    """
    Determine the maximum sample size the computer is able to use to conducting
    a Monte Carlo simulation process using statistical models of generation and
    consumption.

    :param generation_pdfs: Probability density functions for generation data.
    :type generation_pdfs: pandas.DataFrame
    :param consumption_pdfs: Probability density functions for consumption
        data.
    :type consumption_pdfs: pandas.DataFrame
    :param clusters: Dictionary containing cluster names as keys and their
        corresponding number of days as values.
    :type clusters: dict
    :param num_of_inverters: Number of photovoltaic inverters in the
        simulation.
    :type num_of_inverters: int
    :param target_tz: Target time zone.
    :type target_tz: str
    :param fee_date: Date of the applied energy fee in the format "%Y-%m-%d".
    :type fee_date: str, default: None
    :param path: Path to save the convergence table.
    :type path: str or None, default: None
    :param start: 10 ** start is the starting value of the sequence of tested
        sample size.
    :type start: int, default: 1
    :param stop: 10 ** stop is the final value of the sequence of tested
        sample size.
    :type stop: int, default: 5
    :param num: Number of samples size to test.
    :type num: int, default: 29
    :return: Tuple having the power factor ecdf and the cutoff table, the
        excess reactive energy cost ecdf and cutoff table and the simulated
        realizations. If energy_savings is True, it will be returned also
        right after the excess reactive energy cutoff table.
    :rtype: tuple

    .. seealso::
        Module :py:mod:`capitu.src.dap.stats.simulation`
           Documentation of the simulation module used for conducting a Monte
           Carlo simulation process using statistical models of generation and
           consumption.

    :example:
        >>> import os
        >>> from capitu.dap.stats.modeling.consumption import (
        ...     consumption as model_consumption)
        >>> from capitu.dap.stats.modeling.generation import (
        ...     generation as model_generation)
        >>> from capitu.dap.stats.simulation.sample_size import (
        ...     sample_size_determination)
        >>> consumption_pdfs, _, _ = model_consumption(
        ...     start_date="2015-02-22", end_date="2017-03-24",
        ...     consumption_file="../capitu/datastore/consumption.csv",
        ...     target_tz="America/Sao_Paulo", localize_tz=True,
        ...     extra_kvar_i=0, extra_kvar_c=300, fparams={},
        ...     candidates=["johnsonsu", "dweibull", "genlogistic"],
        ...     alpha=0.05, generation_time_only=True, show=False,
        ...     pdfs_path=None, image_path=None)
        >>> generation_pdfs, _, _ = model_generation(
        ...     start_date="2022-1-1", end_date="2022-12-31",
        ...     generation_file="../capitu/datastore/test_generation.ftr",
        ...     data_tz=None, target_tz="America/Sao_Paulo",
        ...     localize_tz=True, fparams={},
        ...     candidates=["johnsonsu", "dweibull", "genlogistic"],
        ...     alpha=0.05, generation_time_only=True, show=False,
        ...     pdfs_path=None, image_path=None)
        >>> num_of_inverters = 125
        >>> clusters = {'not_summer/holidays': 88,
        ...             'not_summer/not_winter': 118,
        ...             'not_summer/vacation': 18,
        ...             'not_summer/winter': 52,
        ...             'summer/holidays': 31,
        ...             'summer/not_winter': 15,
        ...             'summer/vacation': 43}
        >>> (pf_ecdfs, q_cost_ecdfs, pf_table, q_cost_table,
        ...  convergence) = sample_size_determination(
        ...     generation_pdfs, consumption_pdfs, clusters, num_of_inverters,
        ...     target_tz="America/Sao_Paulo", fee_date="2023-07-17",
        ...     path="./", start=1, stop=2, num=5)
        >>> pf_table # doctest: +NORMALIZE_WHITESPACE
        variable    new_pf    old_pf
        hour
        7         0.081205  0.001014
        8         0.225726  0.002137
        9         0.368877   0.00211
        10        0.429671  0.001425
        11        0.523452  0.001096
        12        0.568493  0.001178
        13        0.578795  0.000904
        14        0.507315  0.000849
        15        0.394164  0.000767
        16        0.382932  0.000712
        17        0.202164  0.000849
        18        0.018411  0.000548
        19        0.000247  0.000247
        >>> q_cost_table # doctest: +NORMALIZE_WHITESPACE
        variable new_q_cost old_q_cost
        hour
        7               0.0        0.0
        8          4.056537        0.0
        9         29.215223        0.0
        10         59.89735        0.0
        11        76.064216        0.0
        12        82.514675        0.0
        13        78.157555        0.0
        14        79.267095        0.0
        15        35.441897        0.0
        16        29.400376        0.0
        17         3.015382        0.0
        18              0.0        0.0
        19              0.0        0.0
        >>> convergence
              size        pf    q_cost
        0     10.0      None      None
        17    17.0  0.009234  1.173691
        31    31.0   0.00562   0.68178
        56    56.0  0.002863  1.068017
        100  100.0  0.002943  0.668277
    """

    logging.info("Starting process.")

    sizes = np.logspace(start, stop, num, dtype=int)

    logging.info("Got the sizes too be tested.")

    for i, size in enumerate(sizes):

        logging.info("Testing a sample size of %d." % (size))

        pf_ecdfs, pf_table, q_cost_ecdfs, q_cost_table, _ = simulation(
            generation_pdfs, consumption_pdfs, clusters, num_of_inverters,
            target_tz, fee_date, sample_size=size)

        logging.info("Got the cutoff tables.")

        if i == 0:
            convergence = pd.DataFrame({
                "size": [size], "pf": [None], "q_cost": [None]})
            last_pf_table = pf_table
            last_q_cost_table = q_cost_table

        else:
            convergence.loc[size, "size"] = size
            convergence.loc[size, "pf"] = np.abs(
                pf_table - last_pf_table).max().max()
            convergence.loc[size, "q_cost"] = np.abs(
                q_cost_table - last_q_cost_table).max().max()
            last_pf_table = pf_table
            last_q_cost_table = q_cost_table

        if path is not None:
            convergence.reset_index().to_feather(
                Path(path) / "convergence.ftr")

        logging.info("Got the difference to the last tested size.")

    logging.info("Done.")

    return (pf_ecdfs, q_cost_ecdfs, pf_table, q_cost_table, convergence)
