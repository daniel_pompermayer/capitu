#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
The monte_carlo_simulation module offers a function for performing a Monte
Carlo simulation to generate synthetic electrical data for clusters based on
provided generation and consumption probability density functions (PDFs) and
cluster parameters.
"""

import logging
import numpy as np
import pandas as pd
import scipy.stats
from ...premises.market import peak_hours
from ...premises.clusters import off_peak_season_clusters


def monte_carlo(generation_pdfs, consumption_pdfs, num_of_inverters, clusters,
                sample_size, seed, time_interval="hour"):

    """
    Perform Monte Carlo simulation to generate synthetic electrical data for
    clusters based on provided generation and consumption probability density
    functions (PDFs) and cluster parameters.

    :param generation_pdfs: Probability density functions for generation data.
    :type generation_pdfs: pandas.DataFrame
    :param consumption_pdfs: Probability density functions for consumption
        data.
    :type consumption_pdfs: pandas.DataFrame
    :param num_of_inverters: Number of photovoltaic inverters in the
        simulation.
    :type num_of_inverters: int
    :param clusters: Dictionary containing cluster names as keys and their
        corresponding number of days as values.
    :type clusters: dict
    :param sample_size: Number of samples to generate for each cluster.
    :type sample_size: int
    :param seed: Seed for random number generation.
    :type seed: int
    :param time_interval: Time interval for data simulation.
    :type time_interval: str, default: "hour".
    :return: Simulated electrical data for all clusters and time intervals.
    :rtype: pandas.DataFrame

    .. seealso::

       Module :py:mod:`capitu.src.dap.stats.modeling`
          Documentation of the modeling module used to fit and test the
          statistical models to consumption and generation data we're using
          in this function.

       Module :py:mod:`capitu.src.dap.stats.simulation`
          Documentation of the simulation module used for conducting a Monte
          Carlo simulation process using statistical models of generation and
          consumption.

    :example:
        >>> from capitu.dap.stats.modeling.consumption import (
        ...     consumption as model_consumption)
        >>> from capitu.dap.stats.modeling.generation import (
        ...     generation as model_generation)
        >>> from capitu.dap.stats.simulation.monte_carlo import monte_carlo
        >>> consumption_pdfs, _, _ = model_consumption(
        ...     start_date="2015-02-22", end_date="2017-03-24",
        ...     consumption_file="../capitu/datastore/consumption.csv",
        ...     target_tz="America/Sao_Paulo", localize_tz=True,
        ...     extra_kvar_i=0, extra_kvar_c=300, fparams={},
        ...     candidates=["johnsonsu", "dweibull", "genlogistic"],
        ...     alpha=0.05, generation_time_only=True, show=False,
        ...     pdfs_path=None, image_path=None)
        >>> generation_pdfs, _, _ = model_generation(
        ...     start_date="2022-1-1", end_date="2022-12-31",
        ...     generation_file="../capitu/datastore/test_generation.ftr",
        ...     data_tz=None, target_tz="America/Sao_Paulo",
        ...     localize_tz=True, fparams={},
        ...     candidates=["johnsonsu", "dweibull", "genlogistic"],
        ...     alpha=0.05, generation_time_only=True, show=False,
        ...     pdfs_path=None, image_path=None)
        >>> num_of_inverters = 125
        >>> clusters = {'not_summer/holidays': 88,
        ...             'not_summer/not_winter': 118,
        ...             'not_summer/vacation': 18,
        ...             'not_summer/winter': 52,
        ...             'summer/holidays': 31,
        ...             'summer/not_winter': 15,
        ...             'summer/vacation': 43}
        >>> realizations = monte_carlo(
        ...     generation_pdfs, consumption_pdfs, num_of_inverters, clusters,
        ...     sample_size=1000, seed=42, time_interval="hour")
        >>> realizations.index.size == (1000 * 365 * 13)
        True
        >>> realizations.mean(numeric_only=True)
        hour                    12.000000
        metered_energy_kwh   -2327.315480
        load_kwh              1636.825789
        load_kvarh             333.403080
        gen_energy_kwh        3964.141269
        old_pf                   0.975963
        new_pf                   0.872711
        dtype: float64
    """

    logging.info("Starting process.")

    logging.info("Getting parameters of the clusters.")
    pdfs = pd.DataFrame([])
    for cluster, count in clusters.items():
        for interval in consumption_pdfs[time_interval].drop_duplicates():

            logging.info("Cluster: %s. %s: %d - Starting."
                         % (cluster, time_interval, interval))

            generation_pdf = generation_pdfs[np.logical_and(
                generation_pdfs.cluster == cluster.split("/")[0],
                generation_pdfs[time_interval] == interval)]

            consumption_pdf = consumption_pdfs[np.logical_and(
                consumption_pdfs.cluster == cluster.split("/")[1],
                consumption_pdfs[time_interval] == interval)]

            pdf = pd.concat([generation_pdf, consumption_pdf])
            pdf["cluster"] = cluster
            pdf = pdf.pivot_table(
                index=[time_interval, "cluster"], aggfunc='first',
                columns='variable').swaplevel(0, 1, axis=1)
            pdf["count"] = count
            pdfs = pd.concat([pdfs, pdf])

            logging.info("Cluster: %s. %s: %d - Done."
                         % (cluster, time_interval, interval))

    logging.info("Getting parameters of the clusters.")

    results = pd.DataFrame([])

    for (interval, cluster_name), cluster in pdfs.iterrows():

        res = pd.DataFrame([])

        for variable in cluster.loc[:, "density"].index:

            logging.info(
                "Variable: %s. Cluster: %s. %s: %d- Starting."
                % (variable, cluster_name, time_interval, interval))

            # Get rid of the void attributes.
            params = cluster[variable][cluster[variable] == cluster[variable]]

            # Get the density.
            density = params.density

            # Get rid of the cluster identifications.
            params = params.drop(["density", "ks_approved", "aic"])

            rv = getattr(scipy.stats, density).freeze(**params.to_dict())

            np.random.seed(seed)
            res[variable] = rv.rvs(size=sample_size * cluster["count"])

            logging.info(
                "Variable: %s. Cluster: %s. %s: %d - Got random values."
                % (variable, cluster_name, time_interval, interval))

        logging.info(
            "Computing electrical features of cluster %s and %s %d."
            % (cluster_name, time_interval, interval))

        res["gen_energy_kwh"] = num_of_inverters * res.gen_energy_kwh

        res["metered_energy_kwh"] = res.load_kwh - res.gen_energy_kwh

        res["new_pf"] = np.abs(res.metered_energy_kwh) / np.sqrt(
            (res[["metered_energy_kwh", "load_kvarh"]] ** 2).sum(axis=1))

        res["old_pf"] = np.abs(res.load_kwh) / np.sqrt(
            (res[["load_kwh", "load_kvarh"]] ** 2).sum(axis=1))

        res[time_interval] = interval

        if time_interval == "hour":
            res["time_band"] = "off_peak"
            if cluster_name.split("/")[1] not in off_peak_season_clusters:
                res.loc[np.isin(res[time_interval], peak_hours),
                        "time_band"] = "peak"

        res["cluster"] = cluster_name

        useful_columns = [time_interval, "cluster", "metered_energy_kwh",
                          "load_kwh", "load_kvarh", "gen_energy_kwh",
                          "old_pf", "new_pf"]

        if "time_band" not in useful_columns:
            useful_columns.append("time_band")

        results = pd.concat([results, res[useful_columns]], ignore_index=True)

        logging.info("Cluster %s and %s %d - Done."
                     % (cluster_name, time_interval, interval))

    results["cluster"] = results.cluster.astype("category")

    logging.info("Done.")

    return results
