#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
The simulation module offers functions for conducting a Monte Carlo simulation
process using statistical models of generation and consumption. It generates
synthetic data for consumption and generation, and performs subsequent
calculations based on the simulated data.
"""

import logging
from pathlib import Path
from .monte_carlo import monte_carlo
from ..group_ecdf import group_ecdf
from ..cutoff_table import get_cutoff_table
from ...financial.energy_cost import bill as energy_bill
from ...financial.reactive_cost import bill as q_cost_bill
from ...plotter.ecdfplot import ecdfplot
from ...premises.market import brazilian_ideal_pf


def simulation(generation_pdfs, consumption_pdfs, clusters, num_of_inverters,
               target_tz, fee_date=None, sample_size=1000,
               energy_savings=False, plotting=False, show=False,
               image_path=None, table_path=None, seed=42, value_format=None,
               format="pgf"):

    """
    Conducts a Monte Carlo simulation process using statistical models of
    generation and consumption. It generates synthetic data for consumption
    and generation, and performs subsequent calculations based on the simulated
    data.

    :param generation_pdfs: Probability density functions for generation data.
    :type generation_pdfs: pandas.DataFrame
    :param consumption_pdfs: Probability density functions for consumption
        data.
    :type consumption_pdfs: pandas.DataFrame
    :param clusters: Dictionary containing cluster names as keys and their
        corresponding number of days as values.
    :type clusters: dict
    :param num_of_inverters: Number of photovoltaic inverters in the
        simulation.
    :type num_of_inverters: int
    :param target_tz: Target time zone.
    :type target_tz: str
    :param fee_date: Date of the applied energy fee in the format "%Y-%m-%d".
    :type fee_date: str, default: None
    :param sample_size: Number of samples to generate for each cluster.
    :type sample_size: int, optional
    :param energy_savings: Whether to calculate energy savings.
    :type energy_savings: bool, optional
    :param plotting: Whether to plot results.
    :type plotting: bool, optional
    :param show: Whether to display plots.
    :type show: bool, optional
    :param image_path: Path to save figures.
    :type image_path: str or None, default: None
    :param table_path: Path to save tables.
    :type table_path: str or None, default: None
    :param seed: Seed for random number generation.
    :type seed: int, default: 42
    :param value_format: Object to define how values are displayed.
    :type value_format: str, default: None
    :param format: Specifies the file format for saving the plot.
    :type format: str, default: "pgf"
    :return: Tuple having the power factor ecdf and the cutoff table, the
        excess reactive energy cost ecdf and cutoff table and the simulated
        realizations. If energy_savings is True, it will be returned also
        right after the excess reactive energy cutoff table.
    :rtype: tuple

    .. seealso::

        Module :py:mod:`capitu.src.dap.stats.modeling`
            Documentation of the modeling module used to fit and test the
            statistical models to consumption and generation data we're using
            in this function.

        Module :py:mod:`capitu.src.dap.stats.simulation.monte_carlo`
            Documentation of the monte_carlo_simulation module used to
            performing a Monte Carlo simulation to generate synthetic
            electrical data for clusters based on provided generation and
            consumption probability density functions (PDFs) and cluster
            parameters.

    :example:
        >>> import os
        >>> import pandas as pd
        >>> from PIL import Image, ImageChops
        >>> from capitu import dap
        >>> from capitu.dap.stats.modeling.consumption import (
        ...     consumption as model_consumption)
        >>> from capitu.dap.stats.modeling.generation import (
        ...     generation as model_generation)
        >>> from capitu.dap.stats.simulation import simulation
        >>> dap.set_language("english")
        >>> consumption_pdfs, _, _ = model_consumption(
        ...     start_date="2015-02-22", end_date="2017-03-24",
        ...     consumption_file="../capitu/datastore/consumption.csv",
        ...     target_tz="America/Sao_Paulo", localize_tz=True,
        ...     extra_kvar_i=0, extra_kvar_c=300, fparams={},
        ...     candidates=["johnsonsu", "dweibull", "genlogistic"],
        ...     alpha=0.05, generation_time_only=True, show=False,
        ...     pdfs_path=None, image_path=None)
        >>> generation_pdfs, _, _ = model_generation(
        ...     start_date="2022-1-1", end_date="2022-12-31",
        ...     generation_file="../capitu/datastore/test_generation.ftr",
        ...     data_tz=None, target_tz="America/Sao_Paulo",
        ...     localize_tz=True, fparams={},
        ...     candidates=["johnsonsu", "dweibull", "genlogistic"],
        ...     alpha=0.05, generation_time_only=True, show=False,
        ...     pdfs_path=None, image_path=None)
        >>> num_of_inverters = 125
        >>> clusters = {'not_summer/holidays': 88,
        ...             'not_summer/not_winter': 118,
        ...             'not_summer/vacation': 18,
        ...             'not_summer/winter': 52,
        ...             'summer/holidays': 31,
        ...             'summer/not_winter': 15,
        ...             'summer/vacation': 43}
        >>> fee_date = "2023-07-17"
        >>> (pf_ecdfs, pf_table, q_cost_ecdfs, q_cost_table,
        ...  avg_energy_savings, mc_realizations) = simulation(
        ...     generation_pdfs, consumption_pdfs, clusters, num_of_inverters,
        ...     target_tz="America/Sao_Paulo", fee_date="2023-07-17",
        ...     sample_size=1000, energy_savings=True, plotting=True,
        ...     show=False, image_path="./", table_path="./", seed=42,
        ...     value_format=None, format="png")
        >>> pf_table_expected = pd.read_feather(
        ...     '../capitu/datastore/doctest/pf_table.ftr')
        >>> pf_table_expected.compare(pf_table).empty
        True
        >>> q_cost_expected = pd.read_feather(
        ...     '../capitu/datastore/doctest/q_cost.ftr')
        >>> q_cost_expected.compare(q_cost_table).empty
        True
        >>> target = Image.open('./hist_pf.png')
        >>> template = Image.open('../capitu/datastore/doctest/hist_pf.png')
        >>> diff = ImageChops.difference(target, template)
        >>> diff.getbbox() is None
        True
        >>> os.remove('./hist_pf.png')
        >>> target = Image.open('./hist_q_cost.png')
        >>> template = Image.open(
        ...     '../capitu/datastore/doctest/hist_q_cost.png')
        >>> diff = ImageChops.difference(target, template)
        >>> diff.getbbox() is None
        True
        >>> os.remove('./hist_q_cost.png')
        >>> mc_realizations.index.size == (1000 * 365 * 13)
        True
        >>> mc_realizations.mean(numeric_only=True)
        hour                    13.000000
        metered_energy_kwh     629.498809
        load_kwh              1650.021135
        load_kvarh             348.451644
        gen_energy_kwh        1020.522326
        old_pf                   0.978739
        new_pf                   0.867733
        old_q_cost               0.000002
        new_q_cost              10.225957
        energy_savings         406.412818
        dtype: float64
        >>> mc_realizations.groupby(
        ...    "hour").gen_energy_kwh.mean(numeric_only=True)
        hour
        7      186.577207
        8      505.173893
        9     1007.637078
        10    1517.454524
        11    1841.211624
        12    1958.231252
        13    1934.493334
        14    1675.569129
        15    1358.642777
        16     857.517712
        17     361.674774
        18      62.606203
        19       0.000730
        Name: gen_energy_kwh, dtype: float64
        >>> avg_energy_savings # doctest: +NORMALIZE_WHITESPACE
              energy_savings
        hour
        7          74.302507
        8         201.180451
        9         401.281390
        10        604.311090
        11        733.244117
        12        779.846014
        13        770.392625
        14        667.278650
        15        541.065900
        16        341.497854
        17        144.033362
        18         24.932294
        19          0.000380
    """

    logging.info("Starting process.")

    # Use Monte Carlo method to get a set of power factor values from the
    # adjusted pdfs.
    mc_realizations = monte_carlo(
        generation_pdfs, consumption_pdfs, num_of_inverters, clusters,
        sample_size, seed)

    logging.info("Got the realizations.")

    mc_realizations["old_q_cost"] = q_cost_bill(
        mc_realizations, fee_date, active="load_kwh", reactive="load_kvarh",
        pf="old_pf", target_tz=target_tz, hour="hour")

    logging.info("Got the old q_cost.")

    mc_realizations["new_q_cost"] = q_cost_bill(
        mc_realizations, fee_date, active="metered_energy_kwh",
        reactive="load_kvarh", pf="new_pf", target_tz=target_tz, hour="hour")

    logging.info("Got the new q_cost.")

    logging.info("Computing ecdfs.")

    pf_ecdfs = group_ecdf(
        mc_realizations, y=["old_pf", "new_pf"],
        info="pf: Simulating a sample size of %d." % (sample_size))

    logging.info("Got the pf ecdf.")

    pf_table = get_cutoff_table(pf_ecdfs, cutoff=brazilian_ideal_pf,
                                cutoff_mode="quantile", format=value_format)

    if table_path is not None:
        pf_table.to_feather(Path(table_path)/"pf_table.ftr")

    logging.info("Got and saved the pf table.")

    q_cost_ecdfs = group_ecdf(
        mc_realizations, y=["old_q_cost", "new_q_cost"],
        info="q_cost: Simulating a sample size of %d." % (sample_size))

    logging.info("Got the q_cost ecdf.")

    q_cost_table = get_cutoff_table(q_cost_ecdfs, cutoff=0.9,
                                    cutoff_mode="probability",
                                    format=value_format)

    if table_path is not None:
        q_cost_table.to_feather(Path(table_path)/"q_cost.ftr")

    logging.info("Got and saved the q_cost table.")

    if plotting:

        # Plot the ecdf of the power factor and the q cost in each hour and
        # season.
        ecdfplot(pf_ecdfs, y=["old_pf", "new_pf"], x="hour",
                 path=image_path, cutoff=brazilian_ideal_pf,
                 cutoff_mode="quantile",
                 cutoff_label="Regulatory Power Factor", show=show,
                 filename="hist_pf", format=format)
        logging.info("Plotted the power factor ecdfplot.")

        ecdfplot(q_cost_ecdfs, y=["old_q_cost", "new_q_cost"], x="hour",
                 path=image_path, cutoff=0.9, cutoff_mode="probability",
                 cutoff_label="ppf(90%)", show=show, filename="hist_q_cost",
                 ylim=(0, 90), format=format)
        logging.info("Plotted the q_cost ecdfplot.")

    if energy_savings:
        mc_realizations["energy_savings"] = energy_bill(
            mc_realizations, fee_date, active="gen_energy_kwh")

        logging.info("Got the energy savings.")

        avg_energy_savings = mc_realizations.groupby(
            ["hour"]).energy_savings.mean().to_frame()

        if value_format is not None:
            avg_energy_savings = avg_energy_savings.map(
                lambda x: value_format.format(x))

        if table_path is not None:
            avg_energy_savings.to_feather(
                Path(table_path)/"avg_energy_savings.ftr")

        logging.info("Done.")

        return (pf_ecdfs, pf_table, q_cost_ecdfs, q_cost_table,
                avg_energy_savings, mc_realizations)

    logging.info("Done.")

    return pf_ecdfs, pf_table, q_cost_ecdfs, q_cost_table, mc_realizations
