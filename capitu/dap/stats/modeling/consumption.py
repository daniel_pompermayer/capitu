#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
The consumption module offers a function for fitting statistical models to the
consumption data.
"""

import logging
from .data_processing import pre_processing
from ..find_rvs import find_rvs
from ...getter.consumption import get as get_consumption
from ...plotter.pdfplot import pdfplot
from ...plotter.variation_plot import variation_plot
from ...premises.clusters import mark_season_clusters


def consumption(start_date, end_date, consumption_file, target_tz,
                localize_tz=True, extra_kvar_i=0, extra_kvar_c=0,
                fparams={}, candidates=None, alpha=0.05, bounds=False,
                generation_time_only=True, show=False, pdfs_path=None,
                tables_path=None, pdfs_label="Single Fit",
                best_label="Best Fit", image_path=None, fig_height=12,
                fig_width=17, fontsize=10, density_peak=1.1,
                format="pgf"):
    """
    Model statistical models for the consumption data read from a file.

    :param start_date: Start date of the data interval.
    :type start_date: str, format: "%Y-%m-%d"
    :param end_date: End date of the data interval.
    :type end_date: str, format: "%Y-%m-%d"
    :param consumption_file: Path to the consumption file.
    :type consumption_file: str
    :param target_tz: Name of the target pytz timezone.
    :type target_tz: str, default "America/Sao_Paulo".
    :param localize_tz: If True, timezone is localized. If False, timezone is
        not localized.
    :type localize_tz: bool, default False.
    :param extra_kvar_i: Additional inductive reactive energy.
    :type extra_kvar_i: float, default: 0
    :param extra_kvar_c: Additional capacitive reactive energy.
    :type extra_kvar_c: float, default: 0
    :param fparams: Fixed parameters for the distribution.
    :type fparams: dict, default: {}
    :param candidates: List of candidate distributions to consider. If None,
        all available scipy distributions are considered, except for some
        hardcoded exceptions.
    :type candidates: List[str] or None, default: None
    :param alpha: The significance level for the Kolmogorov-Smirnov test.
    :type alpha: float, default: 0.05
    :param bounds: If True, constrain the found density parameters to the
        inferred bounds.
    :type bounds: bool, default: False
    :param generation_time_only: If True, only data registered between 6 am
        and 7 pm will be kept.
    :type generation_time_only: bool, default: True
    :param show: If True, the plot will be presented on the screen.
    :type show: bool, default: False
    :param pdfs_path: Path to save the fitted models. If None, the models will
        not be saved.
    :type pdfs_path: str or None, default: None
    :param tables_path: Path to save the fitted models as table. If None, the
        models will not be saved as table.
    :type tables_path: str or None, default: None
    :param pdfs_label: Label for the density plot represented by the `pdfs`
        parameter.
    :type pdfs_label: str or None, default: "Single Fit"
    :param best_label: Label for the density plot represented by the
        `best_pdfs` parameter.
    :type best_label: str or None, default: "Best Fit"
    :param image_path: Path to save the plots. If None, the plot will not be
        saved.
    :type image_path: str or None, default: None
    :param fig_height: Height of the figure that will be plotted.
    :type fig_height: int, default: 12
    :param fig_width: Width of the figure that will be plotted.
    :type fig_width: int, default: 17
    :param fontsize: Fontsize of the texts of the plot.
    :type fontsize: int, default: 10
    :param density_peak: Maximum ratio between the biggest histogram bar and
        the density plot size. If the density plot size is bigger than the
        biggest histogram bar, it will be cropped in density_peak times the
        size of biggest bar.
    :type density_peak: float, default: 1.1
    :param format: Specifies the file format for saving the plot.
    :type format: str, default: "pgf"
    :return: parameters for the single best fit for the entire dataset,
        parameters for the best fit for each group, and a comparison between
        the single best fit and the best fit for each group.
    :rtype: pandas.DataFrame or tuple(pandas.DataFrame, pandas.DataFrame,
        pandas.DataFrame)

    :example:
        >>> import os
        >>> import filecmp
        >>> from PIL import Image, ImageChops
        >>> from capitu import dap
        >>> from capitu.dap.stats.modeling.consumption import (
        ...     consumption as model_consumption)
        >>> dap.set_language("english")
        >>> single_pdfs, best_pdfs, comparison = model_consumption(
        ...     start_date="2015-02-22", end_date="2017-03-24",
        ...     consumption_file="../capitu/datastore/consumption.csv",
        ...     target_tz="America/Sao_Paulo", localize_tz=True,
        ...     extra_kvar_i=0, extra_kvar_c=300, fparams={},
        ...     candidates=["johnsonsu", "dweibull", "genlogistic"],
        ...     alpha=0.05, generation_time_only=True, show=False,
        ...     pdfs_path="./", image_path="./", format="png")
        >>> for approach in ["whole", "vacation", "business", "holidays"]:
        ...     for variable in ["load_kwh", "load_kvarh"]:
        ...         target = Image.open(
        ...             './' + approach + '_line_' + variable + '.png')
        ...         template = Image.open(
        ...             '../capitu/datastore/doctest/'
        ...             + approach + '_line_' + variable + '.png')
        ...         diff = ImageChops.difference(target, template)
        ...         print(diff.getbbox() is None)
        ...         os.remove('./' + approach + '_line_' + variable + '.png')
        True
        True
        True
        True
        True
        True
        True
        True
        >>> for approach in ["best", "single", "comparison"]:
        ...     filecmp.cmp('./' + approach + '_consumption_pdfs.csv',
        ...                 '../capitu/datastore/doctest/'
        ...                 + approach + '_consumption_pdfs.csv')
        ...     os.remove('./' + approach + '_consumption_pdfs.csv')
        True
        True
        True
        >>> target = Image.open('./hist_consumption.png')
        >>> template = Image.open(
        ...     '../capitu/datastore/doctest/hist_consumption.png')
        >>> diff = ImageChops.difference(target, template)
        >>> diff.getbbox() is None
        True
        >>> os.remove('./hist_consumption.png')
    """

    logging.info("Starting process.")

    # Get and prepare consumption data.
    consumption = prepare_consumption_for_modeling(
        start_date, end_date, consumption_file, target_tz, localize_tz,
        extra_kvar_i, extra_kvar_c, generation_time_only, show, image_path,
        fig_height, fig_width, format, fontsize)

    # Fit denstity for each group.
    single_pdfs, best_pdfs, comparison = find_rvs(
        data=consumption, y=["load_kwh", "load_kvarh"], approach="single",
        x="hour", categories="cluster", fparams=fparams, candidates=candidates,
        alpha=alpha, bounds=bounds, pdfs_path=pdfs_path,
        tables_path=tables_path, filename="consumption_pdfs")

    logging.info("Densities fitted to groups.")

    # Plot the pdfs of the consumption in each group.
    pdfplot(consumption, single_pdfs, y=["load_kwh", "load_kvarh"],
            best_pdfs=best_pdfs, x="hour", path=image_path, show=show,
            row_category="cluster", filename="hist_consumption",
            pdfs_label=pdfs_label, best_label=best_label,
            density_peak=density_peak, format=format)

    logging.info("Done.")

    return single_pdfs, best_pdfs, comparison


def prepare_consumption_for_modeling(
        start_date, end_date, consumption_file, target_tz, localize_tz=True,
        extra_kvar_i=0, extra_kvar_c=0, generation_time_only=True, show=False,
        image_path=None, fig_height=12, fig_width=17, format="pgf",
        fontsize=10):
    """
    Prepare the consumption data to be statistically modeled.

    :param start_date: Start date of the data interval.
    :type start_date: str, format: "%Y-%m-%d"
    :param end_date: End date of the data interval.
    :type end_date: str, format: "%Y-%m-%d"
    :param consumption_file: Path to the consumption file.
    :type consumption_file: str
    :param target_tz: Name of the target pytz timezone.
    :type target_tz: str, default "America/Sao_Paulo".
    :param localize_tz: If True, timezone is localized. If False, timezone is
        not localized.
    :type localize_tz: bool, default False.
    :param extra_kvar_i: Additional inductive reactive energy.
    :type extra_kvar_i: float, default: 0
    :param extra_kvar_c: Additional capacitive reactive energy.
    :type extra_kvar_c: float, default: 0
    :param generation_time_only: If True, only data registered between 6 am
        and 7 pm will be kept.
    :type generation_time_only: bool, default: True
    :param show: If True, the plot will be presented on the screen.
    :type show: bool, default: False
    :param image_path: Path to save the plots. If None, the plot will not be
        saved.
    :type image_path: str or None, default: None
    :param fig_height: Height of the figure that will be plotted.
    :type fig_height: int, default: 12
    :param fig_width: Width of the figure that will be plotted.
    :type fig_width: int, default: 17
    :param format: Specifies the file format for saving the plot.
    :type format: str, default: "pgf"
    :param fontsize: Fontsize of the texts of the plot.
    :type fontsize: int, default: 10
    :return: Data frame having the consumption preprocessed and with the
        clusters marked.
    :rtype: pandas.DataFrame or tuple(pandas.DataFrame, pandas.DataFrame,
        pandas.DataFrame)

    :example:
        >>> import os
        >>> from PIL import Image, ImageChops
        >>> from capitu import dap
        >>> from capitu.dap.stats.modeling.consumption import (
        ...     prepare_consumption_for_modeling)
        >>> dap.set_language("english")
        >>> consumption = prepare_consumption_for_modeling(
        ...     start_date="2015-02-22", end_date="2017-03-24",
        ...     consumption_file="../capitu/datastore/consumption.csv",
        ...     target_tz="America/Sao_Paulo", localize_tz=True,
        ...     extra_kvar_i=0, extra_kvar_c=300,
        ...     generation_time_only=True, show=False, image_path="./",
        ...     format="png")
        >>> for approach in ["whole", "vacation", "business", "holidays"]:
        ...     for variable in ["load_kwh", "load_kvarh"]:
        ...         target = Image.open(
        ...             './' + approach + '_line_' + variable + '.png')
        ...         template = Image.open(
        ...             '../capitu/datastore/doctest/'
        ...             + approach + '_line_' + variable + '.png')
        ...         diff = ImageChops.difference(target, template)
        ...         print(diff.getbbox() is None)
        ...         os.remove('./' + approach + '_line_' + variable + '.png')
        True
        True
        True
        True
        True
        True
        True
        True
        >>> consumption[["time_band", "load_kwh", "season", "cluster"]]
                                  time_band  load_kwh  season   cluster
        2015-02-22 07:00:00-03:00  off_peak    622.30  summer  holidays
        2015-02-22 08:00:00-03:00  off_peak    660.10  summer  holidays
        2015-02-22 09:00:00-03:00  off_peak    698.60  summer  holidays
        2015-02-22 10:00:00-03:00  off_peak    716.10  summer  holidays
        2015-02-22 11:00:00-03:00  off_peak    733.60  summer  holidays
        ...                             ...       ...     ...       ...
        2017-03-23 15:00:00-03:00  off_peak   2538.24  autumn  vacation
        2017-03-23 16:00:00-03:00  off_peak   2416.32  autumn  vacation
        2017-03-23 17:00:00-03:00  off_peak   2127.36  autumn  vacation
        2017-03-23 18:00:00-03:00  off_peak   1743.36  autumn  vacation
        2017-03-23 19:00:00-03:00      peak   1629.12  autumn  vacation
        <BLANKLINE>
        [9477 rows x 4 columns]
    """

    logging.info("Starting process.")

    # Get consumption data.
    consumption = get_consumption(consumption_file, "hour", True, start_date,
                                  end_date, target_tz, localize_tz,
                                  extra_kvar_i, extra_kvar_c,
                                  generation_time_only)

    # Compute the reactive power demand.
    consumption["load_kvarh"] = (
        consumption.load_kvarh_i - consumption.load_kvarh_c)

    # Process data for getting some useful columns such as hour and season.
    consumption = pre_processing(consumption)

    logging.info("Got the data.")

    # I'm creating a list of tuples where each tuple contains the data to be
    # plotted and the prefix to be used in the saved file names.
    conditions = [
        (consumption, "whole_"),
        (consumption[consumption.business], "business_"),
        (consumption[consumption.vacation], "vacation_"),
        (consumption[consumption.holidays | consumption.weekend], "holidays_")]

    # Plot the consumption data variation by iterating over the conditions.
    for data, prefix in conditions:
        variation_plot(
            data, path=image_path, show=show, classes="hour",
            categories="season", variables=["load_kwh", "load_kvarh"],
            xlim=[7, 19], scale_factor=[1000, 1000], prefix=prefix,
            ax_height=fig_height, ax_width=fig_width,
            format=format, fontsize=fontsize)

    logging.info("Variation plots exported.")

    # Mark clusters.
    consumption["cluster"] = mark_season_clusters(
        consumption.reset_index().iloc[:, 0].dt, variable="consumption")

    logging.info("Data marked. Finished.")

    return consumption
