#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
The generation module offers a function for fitting statistical models to the
generation data.
"""

import logging
from .data_processing import pre_processing
from ..find_rvs import find_rvs
from ...getter.generation import get as get_generation
from ...plotter.pdfplot import pdfplot
from ...plotter.variation_plot import variation_plot
from ...premises.clusters import mark_season_clusters


def generation(start_date, end_date, generation_file, data_tz, target_tz,
               localize_tz=True, fparams={}, candidates=None, alpha=0.05,
               bounds=False, generation_time_only=False, show=False,
               pdfs_path=None, tables_path=None, pdfs_label="Single Fit",
               best_label="Best Fit", image_path=None, fig_height=12,
               fig_width=17, format="pgf", fontsize=10, density_peak=1.1):
    """
    Model statistical models for the generation data read from a file.

    :param start_date: Start date of the data interval.
    :type start_date: str, format: "%Y-%m-%d"
    :param end_date: End date of the data interval.
    :type end_date: str, format: "%Y-%m-%d"
    :param generation_file: Path to the generation file.
    :type generation_file: str
    :param data_tz: name of the pytz timezone where is localized the generation
        data.
    :type data_tz: str, default None.
    :param target_tz: Name of the target pytz timezone.
    :type target_tz: str, default "America/Sao_Paulo".
    :param localize_tz: If True, timezone is localized. If False, timezone is
        not localized.
    :type localize_tz: bool, default False.
    :param fparams: Fixed parameters for the distribution.
    :type fparams: dict, default: {}
    :param candidates: List of candidate distributions to consider. If None,
        all available scipy distributions are considered, except for some
        hardcoded exceptions.
    :type candidates: List[str] or None, default: None
    :param alpha: The significance level for the Kolmogorov-Smirnov test.
    :type alpha: float, default: 0.05
    :param bounds: If True, constrain the found density parameters to the
        inferred bounds.
    :type bounds: bool, default: False
    :param generation_time_only: If True, only data registered in the
        generators working hours will be kept.
    :type generation_time_only: bool, default: True
    :param show: If True, the plot will be presented on the screen.
    :type show: bool, default: False
    :param pdfs_path: Path to save the fitted models. If None, the models will
        not be saved.
    :type pdfs_path: str or None, default: None
    :param tables_path: Path to save the fitted models as table. If None, the
        models will not be saved as table.
    :type tables_path: str or None, default: None
    :param pdfs_label: Label for the density plot represented by the `pdfs`
        parameter.
    :type pdfs_label: str or None, default: "Single Fit"
    :param best_label: Label for the density plot represented by the
        `best_pdfs` parameter.
    :type best_label: str or None, default: "Best Fit"
    :param image_path: Path to save the plots. If None, the plot will not be
        saved.
    :type image_path: str or None, default: None
    :param fig_height: Height of the figure that will be plotted.
    :type fig_height: int, default: 12
    :param fig_width: Width of the figure that will be plotted.
    :type fig_width: int, default: 17
    :param format: Specifies the file format for saving the plot.
    :type format: str, default: "pgf"
    :param fontsize: Fontsize of the texts of the plot.
    :type fontsize: int, default: 10
    :param density_peak: Maximum ratio between the biggest histogram bar and
        the density plot size. If the density plot size is bigger than the
        biggest histogram bar, it will be cropped in density_peak times the
        size of biggest bar.
    :type density_peak: float, default: 1.1
    :return: parameters for the single best fit for the entire dataset,
        parameters for the best fit for each group, and a comparison between
        the single best fit and the best fit for each group.
    :rtype: pandas.DataFrame or tuple(pandas.DataFrame, pandas.DataFrame,
        pandas.DataFrame)

    :example:
        >>> import os
        >>> import filecmp
        >>> from PIL import Image, ImageChops
        >>> from capitu import dap
        >>> from capitu.dap.stats.modeling.generation import (
        ...     generation as model_generation)
        >>> dap.set_language("english")
        >>> single_pdfs, best_pdfs, comparison = model_generation(
        ...     start_date="2022-1-1", end_date="2022-12-31",
        ...     generation_file="../capitu/datastore/test_generation.ftr",
        ...     data_tz=None, target_tz="America/Sao_Paulo",
        ...     localize_tz=True, fparams={},
        ...     candidates=["johnsonsu", "dweibull", "genlogistic"],
        ...     alpha=0.05, generation_time_only=True, show=False,
        ...     pdfs_path="./", image_path="./", format="png")
        >>> target = Image.open('./line_gen_energy_kwh.png')
        >>> template = Image.open(
        ...     '../capitu/datastore/doctest/line_gen_energy_kwh.png')
        >>> diff = ImageChops.difference(target, template)
        >>> diff.getbbox() is None
        True
        >>> os.remove('./line_gen_energy_kwh.png')
        >>> for approach in ["best", "single", "comparison"]:
        ...     filecmp.cmp('./' + approach + '_generation_pdfs.csv',
        ...                 '../capitu/datastore/doctest/'
        ...                 + approach + '_generation_pdfs.csv')
        ...     os.remove('./' + approach + '_generation_pdfs.csv')
        True
        True
        True
        >>> target = Image.open('./hist_generation.png')
        >>> template = Image.open(
        ...     '../capitu/datastore/doctest/hist_generation.png')
        >>> diff = ImageChops.difference(target, template)
        >>> diff.getbbox() is None
        True
        >>> os.remove('./hist_generation.png')
    """

    logging.info("Starting process.")

    # Get and prepare generation data.
    generation = prepare_generation_for_modeling(
        start_date, end_date, generation_file, data_tz, target_tz, localize_tz,
        generation_time_only, show, image_path, fig_height, fig_width,
        format, fontsize)

    # Fit denstity for each group.
    single_pdfs, best_pdfs, comparison = find_rvs(
        data=generation, y="gen_energy_kwh", approach="single",
        x="hour", categories="cluster", fparams=fparams, candidates=candidates,
        alpha=alpha, bounds=bounds, pdfs_path=pdfs_path,
        tables_path=tables_path, filename="generation_pdfs")

    logging.info("Densities fitted to groups.")

    # Plot the pdfs of the generation in each group.
    pdfplot(generation, single_pdfs, y="gen_energy_kwh",
            best_pdfs=best_pdfs, x="hour", path=image_path, show=show,
            row_category="cluster", filename="hist_generation",
            pdfs_label=pdfs_label, best_label=best_label,
            density_peak=density_peak, format=format)

    logging.info("Done.")

    return single_pdfs, best_pdfs, comparison


def prepare_generation_for_modeling(
        start_date, end_date, generation_file, data_tz, target_tz,
        localize_tz=True, generation_time_only=True, show=False,
        image_path=None, fig_height=12, fig_width=17, format="pgf",
        fontsize=10):

    """
    Prepare the generation data to be statistically modeled.

    :param start_date: Start date of the data interval.
    :type start_date: str, format: "%Y-%m-%d"
    :param end_date: End date of the data interval.
    :type end_date: str, format: "%Y-%m-%d"
    :param generation_file: Path to the generation file.
    :type generation_file: str
    :param data_tz: name of the pytz timezone where is localized the generation
        data.
    :type data_tz: str, default None.
    :param target_tz: Name of the target pytz timezone.
    :type target_tz: str, default "America/Sao_Paulo".
    :param localize_tz: If True, timezone is localized. If False, timezone is
        not localized.
    :type localize_tz: bool, default False.
    :param generation_time_only: If True, only data registered between 6 am
        and 7 pm will be kept.
    :type generation_time_only: bool, default: True
    :param show: If True, the plot will be presented on the screen.
    :type show: bool, default: False
    :param image_path: Path to save the plots. If None, the plot will not be
        saved.
    :type image_path: str or None, default: None
    :param fig_height: Height of the figure that will be plotted.
    :type fig_height: int, default: 12
    :param fig_width: Width of the figure that will be plotted.
    :type fig_width: int, default: 17
    :param format: Specifies the file format for saving the plot.
    :type format: str, default: "pgf"
    :param fontsize: Fontsize of the texts of the plot.
    :type fontsize: int, default: 10
    :return: Data frame having the generation preprocessed and with the
        clusters marked.
    :rtype: pandas.DataFrame or tuple(pandas.DataFrame, pandas.DataFrame,
        pandas.DataFrame)

    :example:
        >>> import os
        >>> from PIL import Image, ImageChops
        >>> from capitu import dap
        >>> from capitu.dap.stats.modeling.generation import (
        ...     prepare_generation_for_modeling)
        >>> dap.set_language("english")
        >>> generation = prepare_generation_for_modeling(
        ...     start_date="2022-1-1", end_date="2022-12-31",
        ...     generation_file="../capitu/datastore/test_generation.ftr",
        ...     data_tz=None, target_tz="America/Sao_Paulo",
        ...     localize_tz=True, generation_time_only=True, show=False,
        ...     image_path="./", format="png")
        >>> target = Image.open('./line_gen_energy_kwh.png')
        >>> template = Image.open(
        ...     '../capitu/datastore/doctest/line_gen_energy_kwh.png')
        >>> diff = ImageChops.difference(target, template)
        >>> diff.getbbox() is None
        True
        >>> os.remove('./line_gen_energy_kwh.png')
        >>> generation[[
        ...     "gen_energy_kwh", "cluster"]] # doctest: +NORMALIZE_WHITESPACE
                                   gen_energy_kwh cluster
        2022-01-09 13:00:00-03:00        0.000000  summer
        2022-01-09 14:00:00-03:00        0.000000  summer
        2022-01-09 15:00:00-03:00        0.000000  summer
        2022-01-09 16:00:00-03:00        0.000000  summer
        2022-01-09 17:00:00-03:00        0.000000  summer
        ...                                   ...     ...
        2022-12-30 15:00:00-03:00       19.548245  summer
        2022-12-30 16:00:00-03:00       13.662086  summer
        2022-12-30 17:00:00-03:00        7.178928  summer
        2022-12-30 18:00:00-03:00        1.669420  summer
        2022-12-30 19:00:00-03:00        0.036358  summer
        <BLANKLINE>
        [4362 rows x 2 columns]
    """

    # Get generation data.
    generation = get_generation(generation_file, start_date, end_date,
                                data_tz, target_tz, localize_tz,
                                columns=["Time", "serial_number", "Power(W)"],
                                aggregation="hour",
                                generation_time_only=generation_time_only)
    generation = generation.groupby(generation.index).mean(numeric_only=True)

    # Process data for getting some useful columns such as hour and season.
    generation = pre_processing(generation)

    logging.info("Got the data.")

    # Plot the generation data variation.
    variation_plot(generation, path=image_path, show=show, classes="hour",
                   categories="season", variables=["gen_energy_kwh"],
                   xlim=[7, 19], scale_factor=[1000], prefix="",
                   ax_height=fig_height, ax_width=fig_width, format=format,
                   fontsize=fontsize)

    logging.info("Variation plot exported.")

    # Mark clusters.
    generation["cluster"] = mark_season_clusters(
        generation.reset_index().iloc[:, 0].dt, variable="generation")

    logging.info("Data marked. Finished.")

    return generation
