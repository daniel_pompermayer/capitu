#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
The pre_processing module offers a function for pre-process a given data before
using it to fit statistical models.
"""

from ...filter.iqr_filter import iqr_filter
from ...parser import datetime_to_season
from ...plotter.boxplot import boxplot
from ...premises.generation_time import generation_time


def pre_processing(data):
    """
    Pre-processes the given data before using it to fit statistical models.

    :param data: Input data to be pre-processed.
    :type data: pandas.DataFrame
    :return: Pre-processed data with additional columns for hour and season.
    :rtype: pandas.DataFrame

    :example:
        >>> import pandas as pd
        >>> import numpy as np
        >>> from capitu.dap.stats.modeling.data_processing import (
        ...     pre_processing)
        >>> index = pd.date_range(start='2022-01-01', end='2022-01-05',
        ...                       freq='H')
        >>> np.random.seed(42)
        >>> data = pd.DataFrame({'value': np.random.randn(len(index))},
        ...                     index=index)
        >>> data
                                value
        2022-01-01 00:00:00  0.496714
        2022-01-01 01:00:00 -0.138264
        2022-01-01 02:00:00  0.647689
        2022-01-01 03:00:00  1.523030
        2022-01-01 04:00:00 -0.234153
        ...                       ...
        2022-01-04 20:00:00 -0.702053
        2022-01-04 21:00:00 -0.327662
        2022-01-04 22:00:00 -0.392108
        2022-01-04 23:00:00 -1.463515
        2022-01-05 00:00:00  0.296120
        <BLANKLINE>
        [97 rows x 1 columns]
        >>> processed_data = pre_processing(data)
        >>> processed_data.head()
                                value  hour  season
        2022-01-01 00:00:00  0.496714     0  summer
        2022-01-01 01:00:00 -0.138264     1  summer
        2022-01-01 02:00:00  0.647689     2  summer
        2022-01-01 03:00:00  1.523030     3  summer
        2022-01-01 04:00:00 -0.234153     4  summer
    """

    # Do not bother the external data.
    data = data.copy()

    # I'll use the hours to group data, so it'll be helpful to have a column
    # with that information.
    data["hour"] = data.index.hour

    # Get the season of the data from the date in the index of the data frame.
    data["season"] = datetime_to_season(data.index)

    return data


def filtering(data, path=None, show=False, classes=None, variables=[],
              categories=None, scale_factor=[], whis=1.5, format="pgf"):
    """
    Filters the data by removing outliers based on interquartile range (IQR)
    and optionally plots boxplots before and after filtering.

    :param data: The input DataFrame containing the data to be filtered.
    :type data: pandas.DataFrame
    :param path: The path to save the generated plots. If None, plots are not
                 saved.
    :type path: str, default: None
    :param show: Whether to display the generated plots.
    :type show: bool, default: False
    :param classes: The variable used for grouping the data.
    :type classes: str, default: None
    :param variables: The list of variables to be analyzed and filtered.
    :type variables: List[str], default: []
    :param categories: The categorical variable for grouping the data for
                       outlier removal.
    :type categories: str, default: None
    :param scale_factor: A list of scale factors for each variable used to
                         adjust the width of the boxes in the boxplots.
    :type scale_factor: list[float], defaukt: []
    :param whis: The coefficient determining the reach of the whiskers to the
                 beyond the first and third quartiles.
    :type whis: float, default: 1.5
    :param format: Specifies the file format for saving the plot.
    :type format: str, default: "pgf"

    :return: The filtered DataFrame without outliers.
    :rtype: pandas.DataFrame

    :example:
        >>> import os
        >>> import numpy as np
        >>> import pandas as pd
        >>> from scipy.stats import norm, triang, beta
        >>> from PIL import Image, ImageChops
        >>> from capitu import dap
        >>> from capitu.dap.parser import datetime_to_season
        >>> from capitu.dap.stats.modeling.data_processing import filtering
        >>> dap.set_language("english")
        >>> nine_am = pd.date_range(
        ...     start='2023-01-01 09:00:00', end='2023-12-31 09:00:00',
        ...     freq='D')
        >>> data_size = len(nine_am)
        >>> np.random.seed(42)
        >>> temp_9am = norm.rvs(loc=26, scale=3, size=data_size)
        >>> np.random.seed(42)
        >>> solar_rad_9am = norm.rvs(loc=1345, scale=575, size=data_size)
        >>> eleven_am = pd.date_range(
        ...     start='2023-01-01 11:00:00', end='2023-12-31 11:00:00',
        ...     freq='D')
        >>> np.random.seed(42)
        >>> temp_11am = norm.rvs(loc=28, scale=3, size=data_size)
        >>> np.random.seed(42)
        >>> solar_rad_11am = norm.rvs(loc=2300, scale=850, size=data_size)
        >>> one_pm = pd.date_range(
        ...     start='2023-01-01 13:00:00', end='2023-12-31 13:00:00',
        ...     freq='D')
        >>> np.random.seed(42)
        >>> temp_1pm = triang.rvs(c=0.6, loc=19, scale=17, size=data_size)
        >>> np.random.seed(42)
        >>> solar_rad_1pm = triang.rvs(c=1, loc=-30, scale=4100,
        ...                            size=data_size)
        >>> three_pm = pd.date_range(
        ...     start='2023-01-01 15:00:00', end='2023-12-31 15:00:00',
        ...     freq='D')
        >>> np.random.seed(42)
        >>> temp_3pm = beta.rvs(a=5, b=3, loc=16, scale=19, size=data_size)
        >>> np.random.seed(42)
        >>> solar_rad_3pm = beta.rvs(a=1, b=1, loc=66, scale=3560,
        ...                          size=data_size)
        >>> data = pd.DataFrame({
        ...     "temperature_celsius": np.concatenate([
        ...         temp_9am, temp_11am, temp_1pm, temp_3pm]),
        ...     "e_kj_per_m2": np.concatenate([
        ...         solar_rad_9am, solar_rad_11am, solar_rad_1pm,
        ...         solar_rad_3pm])},
        ...         index=np.concatenate([nine_am, eleven_am, one_pm,
        ...                               three_pm]))
        >>> data.mean()
        temperature_celsius      27.476769
        e_kj_per_m2            2016.471664
        dtype: float64
        >>> num_of_outlines = int(data.index.size * 0.05)
        >>> np.random.seed(42)
        >>> idx_outlines = np.random.randint(
        ...     0, data.index.size, num_of_outlines)
        >>> np.random.seed(42)
        >>> temp_outlines = norm.rvs(loc=100, scale=1, size=num_of_outlines)
        >>> np.random.seed(42)
        >>> rad_outlines = norm.rvs(loc=10000, scale=1, size=num_of_outlines)
        >>> outlines = pd.DataFrame({
        ...     "temperature_celsius": temp_outlines,
        ...     "e_kj_per_m2": rad_outlines},
        ...     index = data.index[idx_outlines])
        >>> data.iloc[idx_outlines, :] = outlines
        >>> data.mean()
        temperature_celsius      30.983086
        e_kj_per_m2            2404.200359
        dtype: float64
        >>> data["hour"] = data.index.hour
        >>> data["season"] = datetime_to_season(data.index)
        >>> data["season"] = data.season.astype("category")
        >>> data = filtering(data, path="./", show=False, classes="hour",
        ...                  variables=["temperature_celsius", "e_kj_per_m2"],
        ...                  categories="season", scale_factor=[1, 1000],
        ...                  whis=1.5, format="png")
        >>> data[["temperature_celsius", "e_kj_per_m2"]].mean()
        temperature_celsius      27.469392
        e_kj_per_m2            2018.846087
        dtype: float64
        >>> target = Image.open('./unfilteredbox_e_kj_per_m2.png')
        >>> template = Image.open(
        ...     '../capitu/datastore/doctest/unfilteredbox_e_kj_per_m2.png')
        >>> diff = ImageChops.difference(target, template)
        >>> diff.getbbox() is None
        True
        >>> os.remove('./unfilteredbox_e_kj_per_m2.png')
        >>> target = Image.open('./unfilteredbox_temperature_celsius.png')
        >>> template = Image.open('../capitu/datastore/doctest/'
        ...                       + 'unfilteredbox_temperature_celsius.png')
        >>> diff = ImageChops.difference(target, template)
        >>> diff.getbbox() is None
        True
        >>> os.remove('./unfilteredbox_temperature_celsius.png')
        >>> target = Image.open('./filteredbox_e_kj_per_m2.png')
        >>> template = Image.open(
        ...     '../capitu/datastore/doctest/filteredbox_e_kj_per_m2.png')
        >>> diff = ImageChops.difference(target, template)
        >>> diff.getbbox() is None
        True
        >>> os.remove('./filteredbox_e_kj_per_m2.png')
        >>> target = Image.open('./filteredbox_temperature_celsius.png')
        >>> template = Image.open('../capitu/datastore/doctest/'
        ...                       + 'filteredbox_temperature_celsius.png')
        >>> diff = ImageChops.difference(target, template)
        >>> diff.getbbox() is None
        True
        >>> os.remove('./filteredbox_temperature_celsius.png')
    """

    # Do not bother external data.
    data = data.copy()

    # Iterate over the variables to plot.
    for n, variable in enumerate(variables):

        # Plot boxplots of the variable showing the outliers.
        boxplot(data, path, show, x=classes, y=variable, hue=categories,
                scale_factor=scale_factor[n], ylim=None, whis=whis,
                prefix="unfiltered", format=format)

    # Iterate over the groups of data.
    for category in data[categories].cat.categories:

        # Nulify the outliers using a IQR filter.
        data.loc[data[categories] == category] = iqr_filter(
            data.loc[data[categories] == category],
            variables, whis=whis, aggregation=classes, agg_index=True)

    # Drop null data.
    data = data.dropna(subset=variables)

    # Iterate over the variables to plot again.
    for n, variable in enumerate(variables):

        # Plot boxplots of the variable without the outliers.
        # I increased the value of the 'whis' parameter because new
        # outliers would be identified in the data after the filter.
        boxplot(data, path, show, x=classes, y=variable, hue=categories,
                scale_factor=scale_factor[n], ylim=None, whis=50,
                prefix="filtered", format=format)

    return data
