#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Fit and test statistical models to consumption and generation data.
"""
name = "modeling"
__author__ = "Daniel Campos Pompermayer [gitlab.com/daniel_pompermayer]"
