#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
The dist_fit module provides a function for fitting a random variable
distribution to data and performs the Kolmogorov-Smirnov test.
"""

import logging
import warnings
import scipy.stats
from .aic import aic
from .kstest import kstest


def dist_fit(data, rv, fparams={}, alpha=0.05, bounds=None, error="raise",
             info=""):
    """
    Fits a random variable distribution to data and performs the
    Kolmogorov-Smirnov test.

    :param observations: The observed data for testing the fit of the random
        variable.
    :type observations: array_like
    :param rv: The name of the random variable distribution to fit.
    :type rv: str
    :param fparams: Fixed parameters for the distribution.
    :type fparams: dict, defaut: {}
    :param alpha: The significance level for the Kolmogorov-Smirnov test.
    :type alpha: float, default: 0.05
    :param bounds: Dictionary whose keys are names of the parameters of the
        distribution and whose values are tuples containing the lower and upper
        bound on that parameter.
    :type bounds: dict, default: None
    :param error: Determines the behavior on error. "raise" to raise
        exceptions, "ignore" to proceed silently.
    :type error: str, default: "raise"
    :param info: Additional information to be displayed in logs.
    :type info: str, default: ""
    :return: A tuple containing the name of the fitted distribution, its
        shapes, fitted parameters, AIC value, and KS test result.
    :rtype: tuple

    :examples:
        >>> import numpy as np
        >>> from capitu.dap.stats.dist_fit import dist_fit
        >>> np.random.seed(42)
        >>> data = np.random.normal(loc=0, scale=1, size=1000)
        >>> rv, shapes, params, aic, ks_approved = dist_fit(data, "norm")
        >>> rv
        'norm'
        >>> shapes
        []
        >>> params
        (0.01933205582232549, 0.9787262077473543)
        >>> aic
        2798.870384834241
        >>> ks_approved
        True
        >>> rv, shapes, params, aic, ks_approved = dist_fit(data, "triang")
        >>> rv
        'triang'
        >>> shapes
        ['c']
        >>> params
        (0.44100235194571513, -3.2531545848270893, 7.116520698586621)
        >>> aic
        3113.660287965658
        >>> ks_approved
        False
    """

    logging.info("Starting process.")

    # Silence the warnings scipy raise.
    warnings.simplefilter("ignore")

    # Search the random variable in scipy.stats.
    rv = getattr(scipy.stats, rv)

    logging.info("Got the candidate random variable.")

    try:
        # Fit the random variable to the data.
        logging.info("%sTrying to fit the %s distribution." % (info, rv.name))
        rv_aic, params = aic(data, rv, fparams, bounds, error)

        # Perform the Kolmogorov-Smirnov test.
        ks_approved = kstest(data, rv, params, alpha)

    except Exception as e:
        logging.error("Error fitting %s: %s" % (rv.name, e.args[0]))
        if error == "raise":
            raise e
        else:
            ks_approved = False

    if ks_approved:
        logging.info("KS Test: %s approved." % (rv.name))
    else:
        logging.info("KS Test: %s failed." % (rv.name))

    # Check whether the random variable has shapes.
    if rv.shapes is not None:
        shapes = rv.shapes.split(", ")
    else:
        shapes = []

    logging.info("%sDone.", info)

    return rv.name, shapes, params, rv_aic, ks_approved
