#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
The aic module offers a function for fitting a probability distribution to a
set of data and computing the Akaike information criterion.
"""

import logging
import warnings
import numpy as np
import scipy.stats


def aic(data, rv, fparams={}, bounds=None, error="ignore"):
    """
    Fit a probability distribution to a set of data and compute the Akaike
    information criterion (AIC).

    :param data: The data to fit the probability distribution.
    :type data: array_like
    :param rv: A random variable object from scipy.stats module representing
        the probability distribution.
    :type rv: scipy.stats._distn_infrastructure.rv_continuous
    :param fparams: Additional parameters to pass to the fit method of the
        random variable object.
    :type fparams: dict, default: {}
    :param bounds: Dictionary whose keys are names of the parameters of the
        distribution and whose values are tuples containing the lower and upper
        bound on that parameter.
    :type bounds: dict, default: None
    :param error: How to handle errors during fitting. 'ignore' to ignore
        errors and return NaN values, 'raise' to raise an exception.
    :type error: {'ignore', 'raise'}, default: "ignore"
    :raises Exception: If an error occurs during fitting and the `error`
        parameter is set to 'raise'.
    :return: A tuple containing the computed AIC value and the parameters of
        the fitted distribution.
    :rtype: tuple

    .. note:: This function fits the given probability distribution (`rv`) to
        the provided data (`data`) and computes the Akaike information
        criterion (AIC) to assess the goodness of fit. The AIC is defined as
        :math:`AIC = 2 * k - 2 * ln(L)`, where `k` is the number of parameters
        in the model and `L` is the likelihood function.

    :references: Akaike, H. (1974). A new look at the statistical model
        identification. IEEE Transactions on Automatic Control, 19(6), 716-723.

    :examples:
        >>> import numpy as np
        >>> from scipy.stats import norm, beta
        >>> from capitu.dap.stats.aic import aic
        >>> np.random.seed(42)
        >>> data = np.random.normal(loc=0, scale=1, size=100)
        >>> norm_aic, norm_params = aic(data, norm)
        >>> beta_aic, beta_params = aic(data, beta)
        >>> norm_aic < beta_aic
        True
    """

    logging.info("Starting process.")

    # Silence the warnings scipy raise.
    warnings.simplefilter("ignore")

    # Fit the random variable to the data and compute the AIC.
    try:
        logging.info("Trying to fit %s." % (rv.name))

        # Check whether I have some bounds.
        if bounds is None:
            params = rv.fit(data, **fparams)
        else:
            if not bool(fparams):
                warnings.warn("fparams and bounds together is not implemented."
                              + " I'm not using fparams.")
            results = scipy.stats.fit(rv, data, bounds)
            params = tuple(results.params._asdict().values())

        loglikelihood = np.sum(rv.logpdf(data, *params))
        aic = 2 * len(params) - 2 * loglikelihood
        logging.info("Got the %s's AIC." % (rv.name))

    # If something didn't go well, check whether to raise an exception or to
    # ignore it.
    except Exception as e:
        logging.error("Ops! %s" % (e.args[0]))
        if error == "raise":
            raise e
        else:
            aic = np.nan
            params = (len(rv.shapes) + 2) * (None, )

    logging.info("Done.")

    return aic, params
