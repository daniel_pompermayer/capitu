#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
The iqr_filter module offers a function for filtering data using the
interquartile range.
"""


def iqr_filter(data, subset, whis=1.5, aggregation=None, agg_index=True):
    """
    Filter data using the interquartile range.

    :param data: Pandas data framte to be filtered.
    :type data: pandas.core.frame.DataFrame
    :param subset: list of columns to be filtered.
    :type subset: list
    :param whis: Proportion of the IQR past the low and high quartiles which
        would extend a boxplot whiskers. Points outside this range will be
        identified as outliers.
    :type whis: float, default: 1.5
    :param aggregation: Name of the attribute to be used to aggregate data. If
        None, data is not aggregated. See agg_index.
    :type aggregation: str or None, default None
    :param agg_index: If True, aggregation is to be getted as attribute of the
        index. If False, it is to be getted as a column of the data frame.
    :type agg_index: bool, default True
    :return: filtered data frame
    :rtype: pandas.core.frame.DataFrame

    :examples:
        >>> import pandas as pd
        >>> import numpy as np
        >>> from capitu.dap.filter.iqr_filter import iqr_filter
        >>> np.random.seed(42)
        >>> data = pd.DataFrame({'A': np.random.normal(0, 1, 1000),
        ...                      'B': np.random.normal(0.15, 1, 1000)})
        >>> np.random.seed(42)
        >>> data.loc[900:, "A"] = np.random.uniform(low=5, high=10, size=100)
        >>> data.mean()
        A    0.751637
        B    0.220836
        dtype: float64
        >>> filtered_data = iqr_filter(data, subset='A')
        >>> filtered_data.mean()
        A    0.017745
        B    0.220836
        dtype: float64
    """

    # Do not bother external data.
    data = data.copy()

    # Iterate over the columns of the data frame for filtering.
    for column in subset:

        # Maybe the user wants to aggregate data.
        if aggregation is not None:

            # Check whether aggregate using aggregation as a column or as a
            # attribute of the index.
            if agg_index:
                grouper = getattr(data[column].index, aggregation)

            else:
                grouper = data[aggregation]

            # Use each group to get the quartiles.
            indices = data[column].groupby(grouper).describe()[["25%", "75%"]]

        # Or maybe he wants to use the whole data.
        else:
            grouper = None
            indices = data[column].describe()[["25%", "75%"]].to_frame().T

        # Compute the interquartile range.
        indices["iqr"] = indices["75%"] - indices["25%"]

        # Compute the external limits.
        indices["max_value"] = indices["75%"] + whis * indices["iqr"]
        indices["min_value"] = indices["25%"] - whis * indices["iqr"]

        # I'll need the series to become a data frame.
        df = data[column].to_frame()

        # If I have no defined grouper, I'll need to create a fake one which
        # will in fact allow every single row of the data frame to be merged
        # to the indices data frame.
        if grouper is None:
            indices.index = [1]
            df.loc[:, "key_0"] = 1
            grouper = "key_0"

        # Merge the data frame with the indices data frame.
        df = df.merge(indices, left_on=grouper, right_index=True)

        # Filter the data.
        data.loc[(df[column] < df.min_value).values, column] = None
        data.loc[(df[column] > df.max_value).values, column] = None

    return data
