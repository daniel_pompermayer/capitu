#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
The filter module offers functions for filtering data.
"""

name = "filter"
__author__ = "Daniel Campos Pompermayer [gitlab.com/daniel_pompermayer]"
