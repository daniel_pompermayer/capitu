#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Set of routines for processing the data used by the application.
"""

from .plotter import labels


name = "dap"
__author__ = "Daniel Campos Pompermayer [gitlab.com/daniel_pompermayer]"

__lang__ = "english"


def set_language(lang="english"):
    """
    This function modifies the language settings for labels used in plots.

    :param lang: The language to use for the labels. It can be "english" or
        "portuguese".
    :type lang: str, default "english"
    :raises ValueError: If the language specified is not supported.
    :return: The language that has been set.
    :rtype: str

    :example:

        >>> from capitu import dap

        To set the language to English:

        >>> dap.set_language("english")
        >>> dap.__lang__
        'english'

        To set the language to Portuguese:

        >>> dap.set_language("portuguese")
        >>> dap.__lang__
        'portuguese'

        Attempting to set an unsupported language will raise a ValueError:

        >>> dap.set_language("french")
        Traceback (most recent call last):
        ...
        ValueError: I don't know how to speak this language yet.
    """

    global __lang__
    __lang__ = labels.set_language(lang)
