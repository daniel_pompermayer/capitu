import time
from pathlib import Path
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from ..clicker import click_on_element
from ..logger import log_message


def export_data(serial_number, from_date, to_date, driver, export_path,
                portal_export_filename, log, timeout=180, repetitions=10):

    # Register the attempt into the log file.
    log_message(log, "Starting proccess for exporting data.", serial_number,
                from_date, to_date)

    for i in range(0, repetitions):
        try:

            log_message(log, "Try no. " + str(i), serial_number, from_date,
                        to_date)

            # Register the amount of files in the folder beforing trying to
            # export.
            path = Path(export_path)
            number_of_files = len(list(path.glob(portal_export_filename)))

            # Sometimes page is oppened rolled down. So try to roll it up
            # before clicking.
            driver.find_element(By.TAG_NAME, 'body').send_keys(
                Keys.CONTROL + Keys.HOME)

            # Click on the export button
            click_on_element(By.CLASS_NAME, "btn-nor.p_btn_export", driver,
                             "Export button", timeout)
            log_message(log, "Clicked on the export button", serial_number,
                        from_date, to_date)

            # Wait for the loading window.
            loading(driver, timeout)
            log_message(log, "Export loaded", serial_number, from_date,
                        to_date)

            # I'll wait for some time for the file to be downloaded.
            downloaded = False
            downloading = False
            start_time = time.time()
            while (downloading or not downloaded):

                # Check whether I've waited for too long.
                if (time.time() - start_time) > timeout:
                    raise TimeoutError("Time runned out without the file being"
                                       + " downloaded.")

                # Check whether I have some .part file.
                if len(list(path.glob("*.part"))) > 0:
                    if not downloading:
                        log_message(log, "Download started", serial_number,
                                    from_date, to_date)
                    downloading = True
                else:
                    if downloading:
                        log_message(log, "Download finished", serial_number,
                                    from_date, to_date)
                    downloading = False

                # Check whether I have some .part file.
                new_number_of_files = len(list(path.glob(
                    portal_export_filename)))
                if (new_number_of_files > number_of_files) and not downloading:
                    log_message(log, "Download success", serial_number,
                                from_date, to_date)
                    downloaded = True
                    return

                # Check if I've found some fail message box. It means that the
                # inverter doesn't have any available data in the chosen dates.
                try:
                    fail_ok_button = driver.find_element(
                        By.CLASS_NAME,
                        "gdw-message-box-button-group.gmbbg-up-line")
                    fail_ok_button.click()
                    raise AttributeError("unavailable data")
                except NoSuchElementException:
                    pass

        except TimeoutError as e:
            if len(e.args) == 0:
                log_message(log, "Try no. " + str(i) + ": TimeoutError",
                            serial_number, from_date, to_date)
            else:
                log_message(log, "Try no. " + str(i) + ": " + e.args[0],
                            serial_number, from_date, to_date)
            if i >= repetitions:
                raise e

        except AttributeError as e:
            if len(e.args) == 0:
                log_message(log, "Try no. " + str(i) + ": AttributeError",
                            serial_number, from_date, to_date)
            else:
                log_message(log, "Try no. " + str(i) + ": " + e.args[0],
                            serial_number, from_date, to_date)
            if e.args[0] == "unavailable data":
                break
            if i >= repetitions:
                raise e

        except RuntimeError as e:
            if len(e.args) == 0:
                log_message(log, "Try no. " + str(i) + ": Runtime Error",
                            serial_number, from_date, to_date)
            else:
                log_message(log, "Try no. " + str(i) + ": " + e.args[0],
                            serial_number, from_date, to_date)
            if i >= repetitions:
                raise e

        except Exception as e:
            if len(e.args) == 0:
                log_message(log, "Try no. " + str(i) + ": Unknown Error",
                            serial_number, from_date, to_date)
            else:
                log_message(log, "Try no. " + str(i) + ": " + e.args[0],
                            serial_number, from_date, to_date)
            if i >= repetitions:
                raise e


def loading(driver, timeout=180):
    try:
        _ = WebDriverWait(driver, 5).until(
            lambda x: x.find_element(By.CLASS_NAME, "common-loading"))
        try:
            _ = WebDriverWait(driver, timeout).until_not(
                lambda x: x.find_element(By.CLASS_NAME, "common-loading"))
        except TimeoutException:
            raise TimeoutError("Loading forever")
    except TimeoutException:
        raise TimeoutError("Loading window didn' show up.")
