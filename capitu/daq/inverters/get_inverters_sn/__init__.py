import numpy as np
import pandas as pd
import datetime as dt
from ..logger import log_message


def get_inverters_sn(inverters_file, log, expired="ignore", sn_null="ignore"):
    """Get list of the inverters' serial numbers."""

    # Read inverters data.
    inverters = pd.read_csv(inverters_file, sep=";", encoding="latin-1",
                            header=0, names=[
                                "inverter", "s_kva", "big_panels",
                                "small_panels", "serial_number", "model",
                                "registration_date", "expiration_date"])

    # Check whether user wants to drop the expired inverters.
    if expired == "drop":

        # Get the inverters that have a registered expiration date.
        expirating_inverters = inverters.expiration_date != "-"

        # Set expiration date as tomorrow for those that doesn't have one.
        inverters.loc[~expirating_inverters, "expiration_date"] = (
            dt.datetime.today() + dt.timedelta(days=1))

        # Convert the expiration date for those who do have one.
        inverters.loc[expirating_inverters, "expiration_date"] = (
            pd.to_datetime(inverters[expirating_inverters].expiration_date))

        # Drop the expirated ones.
        inverters = inverters[inverters.expiration_date > dt.datetime.today()]

        # Unset the expiration date.
        inverters.loc[~expirating_inverters, "expiration_date"] = "-"

    # Check whether user wants to drop the inverters whose serial number is
    # empty.
    if sn_null == "drop":
        inverters = inverters[
            inverters.serial_number == inverters.serial_number]
        inverters = inverters[inverters.serial_number != "-"]

    # Get the serial number list.
    serial_numbers = inverters.serial_number.values

    # Register the success into the log file.
    log_message(log, "Serial number list is loaded.")

    return np.unique(serial_numbers)
