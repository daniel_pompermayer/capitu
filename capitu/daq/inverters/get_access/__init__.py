import pickle
from cryptography.fernet import Fernet


def get_access(key_path, access_path):
    """Decrypt username and password for logging into the inverters portal."""

    with open(key_path, "rb") as c3s4m0_file:
        c3s4m0 = c3s4m0_file.read()

    fernet = Fernet(c3s4m0)

    with open(access_path, "rb") as file:
        access_file = file.read()

    access_file = fernet.decrypt(access_file)

    access = pickle.loads(access_file)

    return access["username"], access["password"]
