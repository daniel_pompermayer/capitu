import datetime as dt
from pathlib import Path


def logger(logpath):
    """Create a log file."""

    logname = dt.datetime.now().strftime("%d-%m-%Y") + ".log"
    log = Path(logpath) / logname

    # Write the header.
    with open(log, "a") as file:
        _ = file.write("======================================" + "\n"
                       + "CAPITU\n"
                       + "Módulo de Inversores\n"
                       + "Universidade Federal do Espírito Santo\n"
                       + str(dt.datetime.now()) + "\n"
                       + "======================================\n\n")

    return log


def log_message(log, message, serial_number="", from_date="", to_date=""):
    """Register some message into the log file."""
    with open(log, "a") as file:
        _ = file.write(str(dt.datetime.now()) + ";" + serial_number + ";"
                       + from_date + ";" + to_date + ";" + message + "\n")


def start_table(log):
    log_message(log, "Message", "Serial Number", "From Date", "To Date")
