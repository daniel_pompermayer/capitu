import time
from selenium.common.exceptions import ElementClickInterceptedException
from selenium.common.exceptions import ElementNotInteractableException
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By


def click_on_element(by, key, driver, element_name, timeout=180):
    """
    Try to click on an element.
    The common cause of not being able to click on a field in the portal
    is the page being rolled down. So, I'll try to roll it a couple of times
    before resigning. It is also possible that when the element has been found,
    it was not yet ready, so I'll wait a moment and try again. It may be
    finally possible that something takes my element from me before clicking.
    """

    try:
        element = WebDriverWait(driver, timeout).until(
            expected_conditions.visibility_of_element_located((by, key)))
        # element = WebDriverWait(driver, timeout).until(
        #     lambda x: x.find_element(by, key))
    except TimeoutException:
        raise TimeoutException(element_name + " hasn't been found")
    
    for i in range(3):
        try:
            element.click()
            break
        except ElementNotInteractableException:
            if i < 2:
                time.sleep(5)
            else:
                raise RuntimeError(element_name + " wasn't interactable")
        except StaleElementReferenceException:
            raise RuntimeError(
                element_name + " has been taken from me before I could click")
        except ElementClickInterceptedException:
            if i < 2:
                driver.find_element(By.TAG_NAME, 'body').send_keys(
                    Keys.CONTROL + Keys.HOME)
            else:
                raise RuntimeError(
                    "Click on " + element_name
                    + " wasn't possible even rolling the page up")
