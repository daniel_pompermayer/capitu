import time
from selenium import webdriver
from selenium.webdriver.firefox.service import Service
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import ElementNotInteractableException
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from ..logger import log_message


def login(username, password, homepage, logged_page, log, driver_path,
          export_path, repetitions=10, timeout=180):
    """Log into the inverters portal."""

    # Set up some firefox preferences.
    firefox_options = webdriver.FirefoxOptions()

    # I don't want a download dialog windows to popup, so never ask and always
    # save to disk the excel files.
    firefox_options.set_preference("browser.helperApps.neverAsk.saveToDisk",
                                   "application/vnd.ms-excel")

    # I also don't want Firefox to use the default "Downloads" (what would be
    # done parsing 1 to the folderList preference). I'd rather want it to use
    # the directory I'm telling it to use (parsing 2).
    firefox_options.set_preference("browser.download.folderList", 2)
    firefox_options.set_preference("browser.download.dir", export_path)

    # Open an instance of Firefox using the setted up preferences.
    driver = webdriver.Firefox(service=Service(driver_path),
                               options=firefox_options)

    # Get the home page.
    driver.get(homepage)

    # Register the success into the log file.
    log_message(log, "Inverters Portal is opened.")

    # If I haven't given enough time, the page may not be fully loaded. If it
    # happens, I'll wait for a little longer and try it again a couple of
    # times. If after that I cannot login, something beyond my reach is going
    # on.
    for i in range(repetitions):
        try:

            # Get and fill the username textbox.
            username_textbox = driver.find_element(By.ID, "username")
            username_textbox.send_keys(username)

            # Get and fill the password textbox.
            password_textbox = driver.find_element(By.ID, "password")
            password_textbox.send_keys(password)

            # Find and uncheck the remember checkbox.
            remember_checkbox = driver.find_element(By.ID, "chkRemember")
            if remember_checkbox.get_property("checked"):
                remember_checkbox.click()

            # Login.
            login_button = driver.find_element(By.ID, "btnLogin")
            login_button.click()

            break

        except ElementNotInteractableException:
            time.sleep(1)
    else:
        log_message(log, "Login page is not being loaded")
        raise ElementNotInteractableException("Login page is not being loaded")

    # Wait for the logged in page to open.
    try:
        _ = WebDriverWait(driver, timeout).until(EC.url_matches(logged_page))
    except TimeoutException:
        log_message(log, "Login seems to be done, but the logged page doesn't"
                    + " showed up")
        raise TimeoutException("Login seems to be done, but the logged page "
                               + "doesn't showed up")

    # Register the success into the log file.
    log_message(log, "Logged into the portal.")

    return driver
