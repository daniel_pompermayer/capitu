import argparse
import datetime as dt
from pathlib import Path


def get_params():
    """Get parameters parsed from terminal."""

    # Instance parser.
    parser = argparse.ArgumentParser()

    # Add arguments related to the decryption of username and password.
    parser.add_argument(
        "--key", type=str,
        help="Path to the decryption key of username and password")
    parser.add_argument(
        "--access", type=str,
        help="Path to the encrypted file containing username and password")

    # Add arguments related to the inverters' portal.
    parser.add_argument(
        "--homepage", type=str,
        help="Webpage address to the inverters' portal")
    parser.add_argument(
        "--logged_page", type=str, default=None,
        help=("Webpage address of the page which is oppened when the login is"
              + " in the inverters' portal is completed"))
    parser.add_argument(
        "--history_page", type=str, default=None,
        help="Webpage address of the inverters' portal history page")

    # Add argument related to the selenium driver location.
    parser.add_argument(
        "--driver", type=str, default="/home/capitu/.geckodriver/geckodriver",
        help="Location of the driver for selenium.")

    # Add arguments related to the time interval.
    parser.add_argument(
        '--interval', type=int, default=1,
        help='Date interval duration in days')
    parser.add_argument(
        '--start', type=str, help='Begin date (%m.%d.%Y")',
        default=(dt.date.today()
                 - dt.timedelta(days=1)).strftime("%m.%d.%Y"))
    parser.add_argument(
        '--end', type=str, help='End date (%m.%d.%Y")',
        default=(dt.date.today()).strftime("%m.%d.%Y"))

    # Add arguments related to the file having the inverters informations.
    parser.add_argument(
        '--inverters_file', type=str,
        help="Path to the file having the inverters informations",
        default=(Path(__file__).parent.parent / "inverters_v1.2.0_stable.csv"))

    # Add arguments related to the exportation.
    parser.add_argument(
        '--export', type=str,
        help="Path where the files must be exported to",
        default="/home/capitu/Downloads")
    parser.add_argument(
        '--portal_export_filename', type=str,
        help=("Regular expression with the format of the names of exported "
              + "files"),
        default="Historical Data Export-*.xls")
    parser.add_argument(
        '--export_filename', type=str,
        help=("String with the names of exported feather file"),
        default="Historical Data Export")

    # Add arguments related to the number of attempts in case of error and
    # timeout.
    parser.add_argument(
        '--timeout', type=str,
        help=("Amount of time Capitu will wait for the proccesses to finish"),
        default=10)
    parser.add_argument(
        '--repetitions', type=str,
        help=("Number of attempts in case of error"),
        default=10)

    # Add logpath argument.
    parser.add_argument(
        '--logpath', type=str,
        help=("Path to the logger file"),
        default=Path(__file__).parent.parent)

    # Get the parameters.
    params = parser.parse_args()
    key_path = params.key
    access_path = params.access
    homepage = params.homepage
    logged_page = params.logged_page
    history_page = params.history_page
    driver_path = params.driver
    inverters_file = params.inverters_file
    export_path = params.export
    portal_export_filename = params.portal_export_filename
    export_filename = params.export_filename
    interval_duration = params.interval
    start_date = params.start
    end_date = params.end
    timeout = params.timeout
    repetitions = params.repetitions
    logpath = params.logpath

    # Check whether logged_page and history_page need to be adjusted.
    if logged_page is None:
        logged_page = homepage + "/PowerStation/powerstatus"

    if history_page is None:
        history_page = homepage + "/PowerStation/historydata"

    return (key_path, access_path, homepage, logged_page, history_page,
            driver_path, inverters_file, export_path, portal_export_filename,
            export_filename, interval_duration, start_date, end_date,
            timeout, repetitions, logpath)
