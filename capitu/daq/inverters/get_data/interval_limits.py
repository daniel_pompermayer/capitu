import pandas as pd
import datetime as dt


def interval_limits(start_date, end_date, interval_duration,
                    input_format="%m.%d.%Y",
                    output_format="%m.%d.%Y %H:%M:%S"):
    """
    Compute the date interval limits to access the inverters' portal a
    number of times covering all the date range with the given interval's
    duration.
    """

    # Write the interval duration as a string.
    interval_duration = str(interval_duration) + "D"

    # I need to create a sliding window of dates here.
    from_date = list(pd.date_range(
        dt.datetime.strptime(start_date, input_format),
        dt.datetime.strptime(end_date, input_format),
        freq=interval_duration, inclusive="left").strftime(output_format))
    to_date = list(pd.date_range(
        dt.datetime.strptime(start_date, input_format),
        dt.datetime.strptime(end_date, input_format),
        freq=interval_duration, inclusive="right").strftime(output_format))

    # Check whether the end date ended up being left out.
    if end_date not in to_date:
        to_date.append(dt.datetime.strptime(end_date, input_format).strftime(
            output_format))

    return zip(from_date, to_date)
