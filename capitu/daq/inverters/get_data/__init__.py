from .interval_limits import interval_limits
from .generate_inverter_data import generate_inverter_data
from ..export_data import export_data
from ..logger import log_message, start_table


def get_data(log, driver, serial_numbers, start_date, end_date,
             interval_duration, history_page, export_path, export_filename,
             timeout=180, repetitions=10):
    """
    Access the portal a number of times to cover all the date range with the
    given interval's duration.
    """
    start_table(log)
    for from_date, to_date in interval_limits(start_date, end_date,
                                              interval_duration):
        for serial_number in serial_numbers:
            try:
                generate_inverter_data(serial_number, from_date, to_date,
                                       driver, history_page, log, timeout,
                                       repetitions)
                export_data(serial_number, from_date, to_date, driver,
                            export_path, export_filename, log, timeout,
                            repetitions)
            except Exception as e:
                log_message(log, "Error: " + str(e.args), serial_number,
                            from_date, to_date)
