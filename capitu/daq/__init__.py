#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Set of routines for acquiring the data used by the application.
"""
name = "daq"
__author__ = "Daniel Campos Pompermayer [gitlab.com/daniel_pompermayer]"
