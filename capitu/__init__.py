#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This module contains the core functionality of the Capitu application.
It includes submodules with many fuctions for data acquisition and processing.
"""

name = "capitu"
__author__ = "Daniel Campos Pompermayer [gitlab.com/daniel_pompermayer]"
